/*
 * Copyright (c) 2017-2020, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "macros.h"
#include "rb79.h"

/* Try and acquire all lock files, in order */
static int
try_get_all_lock_files(size_t num_boards)
{
        for (size_t j = 0; j < num_boards; ++j) {
                if (lock_acquire(j) < 0) {
                        return -1;
                }
        }

        return 0;
}

/* Try and release all lock files, in order */
static int
try_drop_all_lock_files(size_t num_boards)
{
        for (size_t j = 0; j < num_boards; ++j) {
                lock_release(j);
        }

        return 0;
}

/* Make sure we can write to the directory where all the www files will be */
static int
try_write_work_dir(const char *work_dir)
{
        int ret = -1;

        /* First, the directories should be there */
        if (mkdir(work_dir, 0755) < 0) {
                if (errno != EEXIST) {
                        PERROR_MESSAGE("mkdir");
                        ERROR_MESSAGE("Cannot create directory %s", work_dir);
                        goto done;
                }
        }

        ret = 0;
done:

        return ret;
}

/* Make sure we can write to the directory where all the www files will be */
static int
try_write_www_dir(const struct board *boards, size_t boards_num, const
                  char *static_www_folder)
{
        int ret = -1;
        size_t len = 0;
        char *path = 0;
        FILE *f = 0;

        /* First, the directories should be there */
        for (size_t j = 0; j < boards_num; ++j) {
                len = snprintf(0, 0, "%s/%s", static_www_folder,
                               boards[j].name);

                if (len + 1 < len) {
                        ERROR_MESSAGE("overflow");
                        goto done;
                }

                if (!(path = malloc(len + 1))) {
                        PERROR_MESSAGE("malloc");
                        goto done;
                }

                sprintf(path, "%s/%s", static_www_folder, boards[j].name);

                if (mkdir(path, 0755) < 0) {
                        if (errno != EEXIST) {
                                PERROR_MESSAGE("mkdir");
                                ERROR_MESSAGE("Cannot create directory %s",
                                              path);
                                goto done;
                        }
                }

                free(path);
                path = 0;
        }

        /* The /recent page */
        len = snprintf(0, 0, "%s/recent", static_www_folder);

        if (len + 1 < len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (!(path = malloc(len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        sprintf(path, "%s/recent", static_www_folder);

        if (mkdir(path, 0755) < 0) {
                if (errno != EEXIST) {
                        PERROR_MESSAGE("mkdir");
                        ERROR_MESSAGE("Cannot create directory %s", path);
                        goto done;
                }
        }

        free(path);
        path = 0;

        /* Now, the src/ and res/ dirs should be there */
        for (size_t j = 0; j < boards_num; ++j) {
                len = snprintf(0, 0, "%s/%s/src", static_www_folder,
                               boards[j].name);

                if (len + 1 < len) {
                        ERROR_MESSAGE("overflow");
                        goto done;
                }

                if (!(path = malloc(len + 1))) {
                        PERROR_MESSAGE("malloc");
                        goto done;
                }

                sprintf(path, "%s/%s/src", static_www_folder, boards[j].name);

                if (mkdir(path, 0755) < 0) {
                        if (errno != EEXIST) {
                                PERROR_MESSAGE("mkdir");
                                ERROR_MESSAGE("Cannot create directory %s",
                                              path);
                                goto done;
                        }
                }

                sprintf(path, "%s/%s/res", static_www_folder, boards[j].name);

                if (mkdir(path, 0755) < 0) {
                        if (errno != EEXIST) {
                                PERROR_MESSAGE("mkdir");
                                ERROR_MESSAGE("Cannot create directory %s",
                                              path);
                                goto done;
                        }
                }

                free(path);
                path = 0;
        }

        /* Now, we need to be able to write there */
        for (size_t j = 0; j < boards_num; ++j) {
                f = 0;
                len = snprintf(0, 0, "%s/%s/src/a", static_www_folder,
                               boards[j].name);

                if (len + 1 < len) {
                        ERROR_MESSAGE("overflow");
                        goto done;
                }

                if (!(path = malloc(len + 1))) {
                        PERROR_MESSAGE("malloc");
                        goto done;
                }

                sprintf(path, "%s/%s/src/a", static_www_folder, boards[j].name);

                if (!(f = fopen(path, "a"))) {
                        PERROR_MESSAGE("fopen");
                        ERROR_MESSAGE("Cannot create dummy file %s", path);
                        goto done;
                }

                if (fclose(f)) {
                        PERROR_MESSAGE("fclose");
                        ERROR_MESSAGE("Cannot close dummy file %s", path);
                        goto done;
                }

                unlink(path);
                sprintf(path, "%s/%s/res/a", static_www_folder, boards[j].name);

                if (!(f = fopen(path, "a"))) {
                        PERROR_MESSAGE("fopen");
                        ERROR_MESSAGE("Cannot create dummy file %s", path);
                        goto done;
                }

                if (fclose(f)) {
                        PERROR_MESSAGE("fclose");
                        ERROR_MESSAGE("Cannot close dummy file %s", path);
                        goto done;
                }

                unlink(path);
                free(path);
                path = 0;
        }

        len = snprintf(0, 0, "%s/recent/a", static_www_folder);

        if (len + 1 < len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (!(path = malloc(len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        sprintf(path, "%s/recent/a", static_www_folder);

        if (!(f = fopen(path, "a"))) {
                PERROR_MESSAGE("fopen");
                ERROR_MESSAGE("Cannot create dummy file %s", path);
                goto done;
        }

        if (fclose(f)) {
                PERROR_MESSAGE("fclose");
                ERROR_MESSAGE("Cannot close dummy file %s", path);
                goto done;
        }

        unlink(path);
        ret = 0;
done:
        free(path);

        return ret;
}

/*
 * Returns < 0 in case something isn't set up right
 *
 * Preconditions:
 *
 *  - conf is correctly set up.
 *
 * Postconditions (success):
 *
 *  - The ZZZ_setup() functions in other files have all been called,
 *    and all returned successfully.
 */
int
preconditions_check(const struct configuration *conf)
{
        int ret = -1;
        size_t j = 0;
        uintmax_t max_size = (size_t) -1;

        for (j = 0; j < conf->filetypes_num; ++j) {
                const struct filetype *f = &conf->filetypes[j];

                if (f->thumb_creation_command &&
                    f->static_thumbnail) {
                        ERROR_MESSAGE("Filetype for %s cannot have both "
                                      "thumb_creation_command and static_thumbnail",
                                      f->mime_type);
                        goto done;
                }

                if (!f->thumb_creation_command &&
                    !f->static_thumbnail) {
                        ERROR_MESSAGE("Filetype for %s must have one of "
                                      "thumb_creation_command or static_thumbnail",
                                      f->mime_type);
                        goto done;
                }
        }

        for (j = 0; j < conf->boards_num; ++j) {
                const struct board *b = &conf->boards[j];

                if (!b->num_pages) {
                        ERROR_MESSAGE("Board /%s/'s num_pages must be positive",
                                      b->name);
                        goto done;
                }

                if (!b->threads_per_page) {
                        ERROR_MESSAGE("Board /%s/'s threads_per_page must be "
                                      "positive", b->name);
                        goto done;
                }

                if (b->num_pages * b->threads_per_page > max_size) {
                        ERROR_MESSAGE("Board /%s/'s threads cannot be "
                                      "fit in memory", b->name);
                        goto done;
                }

                if (!strcmp(b->name, "recent")) {
                        ERROR_MESSAGE("The board name /recent/ is reserved");
                        goto done;
                }
        }

        if (try_write_work_dir(conf->work_path) < 0) {
                goto done;
        }

        if (setup_multipart() < 0) {
                goto done;
        }

        if (setup_sanitize_comment(conf) < 0) {
                goto done;
        }

        if (setup_sanitize_file(conf) < 0) {
                goto done;
        }

        if (setup_tripcodes(conf) < 0) {
                goto done;
        }

        if (setup_locks(conf) < 0) {
                goto done;
        }

        if (setup_write_thread(conf) < 0) {
                goto done;
        }

        if (try_get_all_lock_files(conf->boards_num) < 0) {
                goto done;
        }

        /* Since we have the lock files now, we need not fear races */
        if (setup_dbs(conf) < 0) {
                goto done;
        }

        if (try_write_www_dir(conf->boards, conf->boards_num,
                              conf->static_www_folder) < 0) {
                goto done;
        }

        ret = 0;
done:

        if (try_drop_all_lock_files(conf->boards_num) < 0) {
                ret = -1;
        }

        return ret;
}
