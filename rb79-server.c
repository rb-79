/*
 * Copyright (c) 2017-2020, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <limits.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <unistd.h>

#include <fcgiapp.h>

#include "macros.h"
#include "rb79.h"

#include "config.h"

const char *program_name = "rb79-server";

/* Print out a page saying the request was malformed (400) */
static int
report_bad_request(FCGX_Request *r, const char *reason)
{
        if (!reason) {
                reason = "That's not a real request. That's all we know.";
        }

        FCGX_FPrintF(r->out, BAD_REQUEST_FMT, reason);

        return 0;
}

/* Print out a page saying they failed the CAPTCHA (403) */
static int
report_bad_challenge(FCGX_Request *r)
{
        FCGX_FPrintF(r->out, BAD_CHALLENGE_FMT);

        return 0;
}

/* Print out a BANNED page (403) */
static int
report_ban(FCGX_Request *r, char *ban_until, char *ban_reason)
{
        FCGX_FPrintF(r->out, BAN_FMT, UBSAFES(ban_until), UBSAFES(ban_reason));

        return 0;
}

/* Print out a BAD METHOD page (405) */
static int
report_bad_method(FCGX_Request *r)
{
        FCGX_FPrintF(r->out, BAD_METHOD_FMT);

        return 0;
}

/* Print out a FILE TOO LARGE page (413) */
static int
report_too_large(FCGX_Request *r, const char *large_thing)
{
        FCGX_FPrintF(r->out, TOO_LARGE_FMT, UBSAFES(large_thing));

        return 0;
}

/* Print out a page saying they're posting too fast (429) */
static int
report_cooldown(FCGX_Request *r, char *cooldown_length)
{
        FCGX_FPrintF(r->out, COOLDOWN_FMT, UBSAFES(cooldown_length));

        return 0;
}

/* Print out an INTERNAL ERROR page (500) */
static int
report_internal_error(FCGX_Request *r)
{
        FCGX_FPrintF(r->out, INTERNAL_ERROR_FMT);

        return 0;
}

/* Print out a POST SUCCESSFUL page (200) */
static int
report_post_successful(FCGX_Request *r, const char *buf)
{
        FCGX_FPrintF(r->out, POST_SUCCESSFUL_FMT, buf);

        return 0;
}

/* Normal POST SUCCESSFUL page, with a redirect back to the given board */
static int
report_post_successful_with_redir(FCGX_Request *r, const char *board_name)
{
        char *buf = 0;
        size_t len = snprintf(0, 0, "/%s", board_name);
        int ret = -1;

        if (len + 1 < len) {
                ERROR_MESSAGE("overflow");
                report_internal_error(r);
                goto done;
        }

        if (!(buf = malloc(len + 1))) {
                PERROR_MESSAGE("malloc");
                report_internal_error(r);
                goto done;
        }

        sprintf(buf, "/%s", board_name);
        ret = report_post_successful(r, buf);
done:
        free(buf);

        return ret;
}

/* Normal POST SUCCESSFUL page, with redirect back to the given thread */
static int
report_post_successful_no_redir(FCGX_Request *r, const char *board_name, const
                                char *thread)
{
        char *buf = 0;
        size_t len = snprintf(0, 0, "/%s/res/%s", board_name, thread);
        int ret = -1;

        if (len + 1 < len) {
                ERROR_MESSAGE("overflow");
                report_internal_error(r);
                goto done;
        }

        if (!(buf = malloc(len + 1))) {
                PERROR_MESSAGE("malloc");
                report_internal_error(r);
                goto done;
        }

        sprintf(buf, "/%s/res/%s", board_name, thread);
        ret = report_post_successful(r, buf);
done:
        free(buf);

        return ret;
}

/* Make sure every board has a page (really only for brand-new boards) */
static int
board_pages_init(struct configuration *conf)
{
        int ret = -1;
        uintmax_t *thread_ids = 0;
        size_t thread_ids_num = 0;
        size_t board_pages_num = 0;

        for (size_t j = 0; j < conf->boards_num; ++j) {
                free(thread_ids);
                thread_ids = 0;

                if (lock_acquire(j) < 0) {
                        goto done;
                }

                if (db_cull_and_report_threads(j, &thread_ids, &thread_ids_num,
                                               &board_pages_num) < 0) {
                        goto done;
                }

                if (wt_write_board(j, thread_ids, thread_ids_num,
                                   board_pages_num) < 0) {
                        goto done;
                }

                lock_release(j);
        }

        ret = 0;
done:
        free(thread_ids);

        return ret;
}

/* Free what needs to be freed */
static void
clean_post_cmd(struct post_cmd *p)
{
        if (!p) {
                return;
        }

        free(p->raw.action);
        free(p->raw.board);
        free(p->raw.thread);
        free(p->raw.post);
        free(p->raw.name);
        free(p->raw.email);
        free(p->raw.tripcode);
        free(p->raw.subject);
        free(p->raw.comment);
        free(p->raw.file_name);
        free(p->raw.file_contents);
        free(p->raw.challenge_id);
        free(p->raw.challenge_response);
        free(p->prepared.name);
        free(p->prepared.email);
        free(p->prepared.tripcode);
        free(p->prepared.subject);
        free(p->prepared.comment);
        free(p->prepared.ext);
        free(p->prepared.file_name);
        free(p->prepared.system_full_path);
        free(p->prepared.system_thumb_path);
        free(p->prepared.file_info);
        free(p->scannable_comment);
        free(p->scannable_name);
        free(p->scannable_email);
        free(p->scannable_subject);
        free(p->scannable_filename);
        free(p->comment_position_map);
        free(p->name_position_map);
        free(p->email_position_map);
        free(p->subject_position_map);
        free(p->filename_position_map);
        *p = (struct post_cmd) { 0 };
}

/* The bulk of work for processing a post */
static void
handle_op_or_reply(struct configuration *conf, FCGX_Request *r, struct
                   post_cmd *pc, const char *ip, size_t parent_thread)
{
        char *buf = 0;
        char *abs_file_path = 0;
        int our_fault = 0;
        uintmax_t real_thread = 0;
        int cooldown = 0;
        int thread_dne = 0;
        int thread_full = 0;
        int thread_closed = 0;
        const struct filetype *f;
        size_t board_pages_num = 0;
        uintmax_t *thread_ids = 0;
        size_t thread_ids_num = 0;
        uint_fast8_t need_to_unlock = 0;

        if (!parent_thread &&
            (!pc->raw.file_contents ||
             !pc->raw.file_contents_len)) {
                LOG("New thread, yet no file (400)");
                report_bad_request(r, "New threads must have a file");
                goto done;
        }

        /* pc comes in with a bunch of these lens set not-as-desired */
        if (pc->raw.file_name_len > conf->max_text_len) {
                LOG("File name length (%zu) larger than max (%zu) (413)",
                    pc->raw.file_name_len, conf->max_text_len);
                report_too_large(r, "Filename");
                goto done;
        }

        if (pc->raw.subject_len > conf->max_text_len) {
                LOG("Subject length (%zu) larger than max (%zu) (413)",
                    pc->raw.subject_len, conf->max_text_len);
                report_too_large(r, "Subject text");
                goto done;
        }

        if (pc->raw.email_len > conf->max_text_len) {
                LOG("Email length (%zu) larger than max (%zu) (413)",
                    pc->raw.email_len, conf->max_text_len);
                report_too_large(r, "Email address");
                goto done;
        }

        if (pc->raw.comment_len > conf->max_text_len) {
                LOG("Comment length (%zu) larger than max (%zu) (413)",
                    pc->raw.comment_len, conf->max_text_len);
                report_too_large(r, "Comment text");
                goto done;
        }

        if (pc->raw.file_contents_len > conf->max_file_size) {
                LOG("File size (%zu) larger than max (%zu) (413)",
                    pc->raw.file_contents_len, conf->max_file_size);
                report_too_large(r, "File size");
                goto done;
        }

        if (sf_check_mime_type(pc->raw.file_contents, pc->raw.file_contents_len,
                               &f) < 0) {
                LOG("Bad MIME check (400)");
                report_bad_request(r, "Unsupported file type");
                goto done;
        }

        /* Calculate tripcodes before HTML-escaping everything */
        if (tripcodes_calculate(pc) < 0) {
                LOG("Error in tripcodes_calculate (500)");
                report_internal_error(r);
                goto done;
        }

        /* HTML-escape, wordfilter, linkify */
        uint_fast8_t is_forbidden = 0;
        int ban_duration = 0;
        const char *ban_reason = 0;

        if (st_sanitize_text(pc, &our_fault, &is_forbidden, &ban_duration,
                             &ban_reason) < 0) {
                if (our_fault) {
                        LOG("Error in st_sanitize_text (500)");
                        report_internal_error(r);
                        goto done;
                }

                if (is_forbidden) {
                        LOG("Bad text (400)");
                        LOG("Comment was \"%s\"", UBSAFES(pc->raw.comment));

                        if (ban_duration) {
                                time_t start = time(0);
                                time_t end = start + ban_duration;
                                int is_secret = !(ban_reason);

                                if (db_insert_ban(1, 0, ip, ip, ban_reason,
                                                  is_secret, start, end) < 0) {
                                        LOG("Error in db_insert_ban (500)");
                                        report_internal_error(r);
                                        goto done;
                                }
                        }

                        report_bad_request(r, "Disallowed text");
                        goto done;
                } else {
                        LOG("Unknown error (400)");
                        LOG("Comment was \"%s\"", UBSAFES(pc->raw.comment));
                        report_bad_request(r, "Disallowed text");
                        goto done;
                }
        }

        cooldown = pc->prepared.comment_len ?
                   conf->boards[pc->board_idx].text_cooldown :
                   conf->boards[pc->board_idx].blank_cooldown;

        /*
         * From now on, everything must be under lock, since we
         * could be touching the filesystem. Strictly, we don't
         * need to worry about locking for db-only operations, so
         * this could be delayed a bit.
         */
        if (lock_acquire(pc->board_idx) < 0) {
                LOG("Error in lock_acquire (500)");
                report_internal_error(r);
                goto done;
        }

        need_to_unlock = 1;

        if (db_insert_post(ip, parent_thread, cooldown, pc, &thread_dne,
                           &thread_closed, &thread_full, &pc->prepared.id) <
            0) {
                LOG("Error in insert_post (500)");
                report_internal_error(r);
                goto done;
        }

        LOG("Post %ju on board /%s/", pc->prepared.id,
            conf->boards[pc->board_idx].name);

        if (thread_dne) {
                LOG("Thread %zu does not exist (400)", (size_t) 0);
                report_bad_request(r, "Thread does not exist");
                goto done;
        }

        if (thread_full) {
                LOG("Thread %zu is full (400)", (size_t) 0);
                report_bad_request(r, "Thread is full");
                goto done;
        }

        if (thread_closed) {
                LOG("Thread %zu is closed (400)", (size_t) 0);
                report_bad_request(r, "Thread is closed");
                goto done;
        }

        /* Make thumbnails and insert them */
        if (f) {
                if (sf_install_files(pc->board_idx, pc->raw.file_contents,
                                     pc->raw.file_contents_len,
                                     &pc->prepared.now, f, &abs_file_path,
                                     &pc->prepared.system_full_path,
                                     &pc->prepared.system_full_path_len,
                                     &pc->prepared.system_thumb_path,
                                     &pc->prepared.system_thumb_path_len,
                                     &our_fault) < 0) {
                        if (our_fault) {
                                LOG("Error in sf_install_files (500)");
                                report_internal_error(r);
                                goto done;
                        }

                        LOG("Couldn't install files (400)");
                        report_bad_request(r, "Bad file upload");
                        goto done;
                }

                /* ... and now that they're inserted, describe them ... */
                if (sf_describe_file(f->mime_type, abs_file_path,
                                     &pc->prepared.file_info,
                                     &pc->prepared.file_info_len) < 0) {
                        LOG("Error in sf_describe_file (500)");
                        report_internal_error(r);
                        goto done;
                }

                /* ... and alert the db about that description. */
                if (db_update_file_info(pc->board_idx, pc->prepared.id,
                                        pc->prepared.file_info,
                                        pc->prepared.file_info_len,
                                        pc->prepared.system_full_path,
                                        pc->prepared.system_full_path_len,
                                        pc->prepared.system_thumb_path,
                                        pc->prepared.system_thumb_path_len) <
                    0) {
                        LOG("Error in db_update_post_description (500)");
                        report_internal_error(r);
                        goto done;
                }
        }

        /*
         * We're about ready to write out the threads, boards, etc.
         * Therefore, we must now check for thread culling, and
         * also calculate how many board pages we need.
         */
        if (db_cull_and_report_threads(pc->board_idx, &thread_ids,
                                       &thread_ids_num, &board_pages_num) < 0) {
                LOG("Error in db_cull_and_report_threads (500)");
                report_internal_error(r);
                goto done;
        }

        real_thread = parent_thread ? parent_thread : pc->prepared.id;

        if (wt_write_thread(pc->board_idx, real_thread) < 0) {
                LOG("Error in wt_write_thread (500)");
                report_internal_error(r);
                goto done;
        }

        if (wt_write_board(pc->board_idx, thread_ids, thread_ids_num,
                           board_pages_num) < 0) {
                LOG("Error in wt_write_board (500)");
                report_internal_error(r);
                goto done;
        }

        if (pc->raw.email &&
            !strcmp(pc->raw.email, "noko")) {
                report_post_successful_no_redir(r, pc->raw.board,
                                                pc->raw.thread);
        } else {
                report_post_successful_with_redir(r, pc->raw.board);
        }

done:

        if (need_to_unlock) {
                lock_release(pc->board_idx);
        }

        free(buf);
        free(abs_file_path);
        free(thread_ids);
}

/* Rebuild every thread and every board */
static void
handle_rebuild (struct configuration *conf, FCGX_Request *r)
{
        uint_fast8_t had_errors = util_rebuild(conf);

        FCGX_FPrintF(r->out, "Status: 200\r\nContent-type: text/plain\r\n\r\n"
                             "Rebuild complete%s\n", had_errors ?
                     " with errors" : "");

        return;
}

/* Figure out what they want us to do */
static void
handle(struct configuration *conf, FCGX_Request *r)
{
        char *p = 0;
        char *content_type = 0;
        char *content_len_str = 0;
        size_t content_len = 0;
        char *buf_main = 0;
        size_t buf_main_len = 0;
        const char *content_type_prefix = "Content-Type: ";
        struct post_cmd post_cmd = { 0 };
        const char *ip_raw = FCGX_GetParam("REMOTE_ADDR", r->envp);
        char *ip = 0;
        char *ban_reason = 0;
        char *ban_until = 0;
        char *cooldown_length = 0;
        uint_fast8_t found_idx = 0;

        /* In case someone is trying for a time GET, prioritize that */
        time(&post_cmd.prepared.now);
        LOG("-----------------------------------------");
        LOG("Handling post at %zu from %s", (size_t) post_cmd.prepared.now,
            UBSAFES(ip_raw));

        if (!ip_raw) {
                LOG("Couldn't get REMOTE_ADDR (500)");
                report_internal_error(r);
                goto done;
        }

        if (util_normalize_ip(ip_raw, &ip) < 0) {
                LOG("Couldn't normalize ip (500)");
                report_internal_error(r);
                goto done;
        }

        /* You can only POST to /action */
        if (!(p = FCGX_GetParam("REQUEST_METHOD", r->envp))) {
                LOG("Couldn't get request method (500)");
                report_internal_error(r);
                goto done;
        }

        if (strcmp(p, "POST")) {
                LOG("request method was not POST (405)");
                report_bad_method(r);
                goto done;
        }

        /* We have to somehow feed this into multipart */
        if (!(content_type = FCGX_GetParam("CONTENT_TYPE", r->envp))) {
                LOG("Can't get CONTENT_TYPE (500)");
                report_internal_error(r);
                goto done;
        }

        if (!(content_len_str = FCGX_GetParam("CONTENT_LENGTH", r->envp))) {
                LOG("Can't get CONTENT_LENGTH (500)");
                report_internal_error(r);
                goto done;
        }

        content_len = (size_t) strtoll(content_len_str, 0, 0);

        if (content_len > max_form_data_size) {
                LOG("Buffer would have exceeded %zuB (413)",
                    max_form_data_size);
                report_too_large(r, "Total POST");
                goto done;
        }

        buf_main_len = strlen(content_type_prefix) + strlen(content_type) +
                       strlen("\r\n\r\n") + content_len;

        if (buf_main_len + 1 < buf_main_len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (!(buf_main = malloc(buf_main_len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        size_t offset = sprintf(buf_main, "%s%s\r\n\r\n", content_type_prefix,
                                content_type);

        /* Try and swallow this thing into a buffer */
        FCGX_GetStr(buf_main + offset, content_len, r->in);

        /* Okay, we've got it in the buffer */
        if (multipart_decompose(buf_main, buf_main_len, &post_cmd) < 0) {
                LOG("Decoding message failed, returning (400)");
                report_bad_request(r, "Invalid multipart/form-data");
                goto done;
        }

        /* Now we can check what they actually wanted us to DO */
        if (!post_cmd.raw.action) {
                LOG("No action specified (400)");
                report_bad_request(r, "You have to give action=something");
                goto done;
        } else if (!(strcmp(post_cmd.raw.action, "reply"))) {
                post_cmd.action_id = REPLY;
        } else if (!(strcmp(post_cmd.raw.action, "newthread"))) {
                post_cmd.action_id = NEWTHREAD;
        } else if (!(strcmp(post_cmd.raw.action, "rebuild"))) {
                post_cmd.action_id = REBUILD;
        }

        if (post_cmd.raw.thread) {
                post_cmd.thread_id = strtoll(post_cmd.raw.thread, 0, 0);
        }

        if (post_cmd.action_id == NONE) {
                LOG("Invalid action \"%s\" (400)", post_cmd.raw.action);
                report_bad_request(r, "That's not a valid action");
                goto done;
        }

        /*
         * XXX: the idea is to only accept REBUILD commmands from
         * the local machine. Is this necessary and sufficient in
         * the world of ipv6?
         */
        if (post_cmd.action_id == REBUILD) {
                /* Note that the IP is normalized so we can sort it */
                if (strcmp(ip, "127.000.000.001") &&
                    strcmp(ip, "000.000.000.000") &&
                    strcmp(ip, "0000:0000:0000:0000:0000:0000:0000:0001")) {
                        LOG("REBUILD requested from invalid ip %s", ip);
                        report_bad_request(r, "You can(not) rebuild");
                        goto done;
                }

                goto take_action;
        }

        /* And we can find where they wanted to do it */
        found_idx = 0;

        if (!post_cmd.raw.board) {
                LOG("No board specified (400)");
                report_bad_request(r, "You have to give board=something");
                goto done;
        }

        if (post_cmd.action_id == REPLY &&
            !post_cmd.thread_id) {
                LOG("Reply, yet no thread (400)");
                report_bad_request(r, "You have to give thread=something");
                goto done;
        }

        for (size_t j = 0; j < conf->boards_num; ++j) {
                if (!strcmp(post_cmd.raw.board, conf->boards[j].name)) {
                        post_cmd.board_idx = j;
                        found_idx = 1;
                        break;
                }
        }

        if (!found_idx) {
                LOG("Invalid board \"%s\" (400)", post_cmd.raw.board);
                report_bad_request(r, "That's not a valid board");
                goto done;
        }

        int is_banned = 0;
        int is_secret = 0;

        if (db_check_bans(ip, post_cmd.board_idx, post_cmd.prepared.now,
                          &is_banned, &ban_until, &is_secret, &ban_reason) <
            0) {
                LOG("Couldn't determine ban status (500)");
                report_internal_error(r);
                goto done;
        }

        if (is_banned) {
                if (is_secret) {
                        LOG("Ban[s] (until=\"%s\", reason=\"%s\") (200)",
                            ban_until, UBSAFES(ban_reason));
                        report_post_successful_with_redir(r,
                                                          post_cmd.raw.board);
                        goto done;
                } else {
                        /* This should give HTTP 403 */
                        LOG("Ban (until=\"%s\", reason=\"%s\") (403)",
                            ban_until, UBSAFES(ban_reason));
                        report_ban(r, ban_until, ban_reason);
                        goto done;
                }
        }

        if (post_cmd.action_id == REPLY ||
            post_cmd.action_id == NEWTHREAD) {
                int is_cooled = 0;

                if (db_check_cooldowns(ip, post_cmd.board_idx,
                                       post_cmd.prepared.now, &is_cooled,
                                       &cooldown_length) < 0) {
                        LOG("Couldn't determine cooldown status (500)");
                        report_internal_error(r);
                        goto done;
                }

                if (is_cooled) {
                        /* This should give HTTP 429 */
                        LOG("Cooldown triggered (length=\"%s\") (429)",
                            cooldown_length);
                        report_cooldown(r, cooldown_length);
                        goto done;
                }

                int correct_challenge = 0;

                if (!post_cmd.raw.challenge_id) {
                        LOG("No challenge id given (403)");
                        report_bad_challenge(r);
                        goto done;
                }

                char *e = 0;
                size_t challenge_idx = (size_t) strtoll(
                        post_cmd.raw.challenge_id, &e, 0);

                if (e &&
                    *e) {
                        challenge_idx = conf->challenges_num;
                }

                if (challenge_idx >= conf->challenges_num) {
                        LOG("Bad challenge id \"%s\" given (403)",
                            post_cmd.raw.challenge_id);
                        report_bad_challenge(r);
                        goto done;
                }

                if (!post_cmd.raw.challenge_response) {
                        LOG("No challenge response given (403)");
                        report_bad_challenge(r);
                        goto done;
                }

                for (size_t j = 0; j < NUM_CHALLENGE_ANSWERS; ++j) {
                        if (!conf->challenges[challenge_idx].answers[j]) {
                                continue;
                        }

                        if (!strcasecmp(post_cmd.raw.challenge_response,
                                        conf->challenges[challenge_idx].answers[
                                                j])) {
                                correct_challenge = 1;
                        }
                }

                if (!correct_challenge) {
                        LOG("Incorrect response \"%s\" to challenge %s (403)",
                            post_cmd.raw.challenge_response,
                            post_cmd.raw.challenge_id);
                        LOG("Comment was \"%s\"", UBSAFES(
                                    post_cmd.raw.comment));
                        report_bad_challenge(r);
                        goto done;
                }
        }

take_action:

        /* Now we split into specific actions */
        switch (post_cmd.action_id) {
        case REPLY:
                LOG("reply to /%s/%ju", UBSAFES(post_cmd.raw.board),
                    post_cmd.thread_id);
                handle_op_or_reply(conf, r, &post_cmd, ip, post_cmd.thread_id);
                break;
        case NEWTHREAD:
                LOG("newthread on /%s/", UBSAFES(post_cmd.raw.board));
                handle_op_or_reply(conf, r, &post_cmd, ip, 0);
                break;
        case REBUILD:
                LOG("rebuild");
                handle_rebuild(conf, r);
                break;
        case NONE:
                ERROR_MESSAGE("Impossible");
                report_internal_error(r);
                break;
        }

done:
        clean_post_cmd(&post_cmd);
        free(buf_main);
        free(ban_reason);
        free(ban_until);
        free(cooldown_length);
        free(ip);
}

/* Do the thing */
int
main(void)
{
        int ret = 1;
        FCGX_Request r = { 0 };
        struct configuration conf = { 0 };

        setlocale(LC_ALL, "");

        /* tedu@ is probably laughing at me right now.  Hi! */
        srand(time(0));
        conf = (struct configuration) {
                /* */
                .static_www_folder = static_www_folder,             /* */
                .work_path = work_path,                             /* */
                .temp_dir_template = temp_dir_template,             /* */
                .trip_salt = trip_salt,                             /* */
                .trip_salt_len = strlen(trip_salt),                 /* */
                .boards = boards,                                   /* */
                .boards_num = NUM_OF(boards),                       /* */
                .max_form_data_size = max_form_data_size,           /* */
                .max_file_size = max_file_size,                     /* */
                .max_text_len = max_text_len,                       /* */
                .filetypes = filetypes,                             /* */
                .filetypes_num = NUM_OF(filetypes),                 /* */
                .file_description_prog = file_description_prog,     /* */
                .headers = headers,                                 /* */
                .headers_num = NUM_OF(headers),                     /* */
                .challenges = challenges,                           /* */
                .challenges_num = NUM_OF(challenges),               /* */
                .wordfilter_inputs = wordfilter_inputs,             /* */
                .wordfilter_inputs_num = NUM_OF(wordfilter_inputs), /* */
                .forbidden_inputs = forbidden_inputs,               /* */
                .forbidden_inputs_num = NUM_OF(forbidden_inputs),   /* */
        };

        if (preconditions_check(&conf) < 0) {
                goto done;
        }

        if (board_pages_init(&conf) < 0) {
                goto done;
        }

        FCGX_Init();
        FCGX_InitRequest(&r, 0, 0);

        while (FCGX_Accept_r(&r) == 0) {
                handle(&conf, &r);
                FCGX_Finish_r(&r);
        }

        ret = 0;
done:
        clean_dbs();
        clean_locks();
        clean_multipart();
        clean_sanitize_comment();
        clean_sanitize_file();
        clean_tripcodes();
        clean_write_thread();

        return ret;
}
