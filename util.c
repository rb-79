/*
 * Copyright (c) 2017-2020, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "macros.h"
#include "rb79.h"

/*
 * How many characters are needed to hold "1141709097-06-13 06:26:07Z",
 * which is the representation of 2^55 - 1, the largest time_t that
 * this, rather reasonable system can hold.
 */
#define ISO8601_MAX 30

/*
 * Convert t to a string, like "2000-01-01 23:59:59Z"
 *
 * Preconditions:
 *
 *   - None.
 *
 * Postconditions:
 *
 *   - Either a string in ISO 8601 format has been returned, or 0
 *     has been returned and an error message has been logged.
 */
char *
util_iso8601_from_time_t(time_t t)
{
        /*
         * XXX: If this becomes multithreaded, we have bigger
         * problems, but we also need to use gmtime_r() or the
         * equivalent.
         */
        struct tm *tm = gmtime(&t);
        char *ret = 0;

        if (!tm) {
                ERROR_MESSAGE("gmtime");

                return 0;
        }

        /*
         * Even if t > 2^55 - 1, this should only truncate the
         * result, not corrupt memory.
         */
        if (!(ret = malloc(ISO8601_MAX + 1))) {
                ERROR_MESSAGE("malloc");

                return 0;
        }

        strftime(ret, ISO8601_MAX, "%F %H:%M:%SZ", tm);

        return ret;
}

/* Put an IP address into a range-comparable form */
int
util_normalize_ip(const char *in, char **out)
{
        int ret = -1;
        size_t built_len = 0;
        char *built = 0;

        if (strchr(in, ':')) {
                /*
                 * If your C implementation doesn't implement the
                 * IPV6 functionality, you should be able to simply
                 * lop out this section entirely with no harm done.
                 */
                struct in6_addr a = { 0 };

                if (inet_pton(AF_INET6, in, &a) != 1) {
                        PERROR_MESSAGE("inet_pton");
                        goto done;
                }

                built_len = (4 * 8) + 7;

                if (!(built = malloc(built_len + 1))) {
                        PERROR_MESSAGE("malloc");
                        goto done;
                }

                sprintf(built, "%02x%02x:%02x%02x:%02x%02x:%02x%02x:"
                               "%02x%02x:%02x%02x:%02x%02x:%02x%02x", (unsigned
                                                                       int) a.
                        s6_addr[0], (unsigned int) a.s6_addr[1], (unsigned
                                                                  int)
                        a.s6_addr[2], (unsigned int) a.s6_addr[3], (unsigned
                                                                    int)
                        a.s6_addr[4], (unsigned int) a.s6_addr[5], (unsigned
                                                                    int)
                        a.s6_addr[6], (unsigned int) a.s6_addr[7], (unsigned
                                                                    int)
                        a.s6_addr[8], (unsigned int) a.s6_addr[9], (unsigned
                                                                    int)
                        a.s6_addr[10], (unsigned int) a.s6_addr[11], (unsigned
                                                                      int)
                        a.s6_addr[12], (unsigned int) a.s6_addr[13], (unsigned
                                                                      int)
                        a.s6_addr[14], (unsigned int) a.s6_addr[15]);
        } else if (strchr(in, '.')) {
                struct in_addr a = { 0 };

                if (inet_pton(AF_INET, in, &a) != 1) {
                        PERROR_MESSAGE("inet_pton");
                        goto done;
                }

                built_len = (3 * 4) + 3;

                if (!(built = malloc(built_len + 1))) {
                        PERROR_MESSAGE("malloc");
                        goto done;
                }

                sprintf(built, "%03u.%03u.%03u.%03u",  /* */
                        (a.s_addr & 0x000000ff) >> 0,  /* */
                        (a.s_addr & 0x0000ff00) >> 8,  /* */
                        (a.s_addr & 0x00ff0000) >> 16, /* */
                        (a.s_addr & 0xff000000) >> 24  /* */
                        );
        } else {
                ERROR_MESSAGE("\"%s\" is not a valid IP address", in);
                goto done;
        }

        ret = 0;
done:

        if (ret) {
                free(built);
        } else {
                *out = built;
        }

        return ret;
}

/*
 * Call wt_write_thread and wt_write_board on everything
 *
 * Note: this ACQUIRES all locks, which means it must be called
 * with NO LOCKS acquired
 */
int
util_rebuild(struct configuration *conf)
{
        uint_fast8_t had_errors = 0;

        for (size_t j = 0; j < conf->boards_num; ++j) {
                uint_fast8_t need_to_unlock = 0;
                uintmax_t *thread_ids = 0;
                size_t thread_ids_num = 0;
                size_t board_pages_num = 0;

                if (lock_acquire(j) < 0) {
                        LOG("Error in lock_acquire (500)");
                        had_errors = 1;
                        goto done;
                }

                need_to_unlock = 1;

                if (db_cull_and_report_threads(j, &thread_ids, &thread_ids_num,
                                               &board_pages_num) < 0) {
                        LOG("Error in db_cull_and_report_threads (j = %zu)", j);
                        goto done_with_this_board;
                }

                for (size_t k = 0; k < thread_ids_num; ++k) {
                        if (wt_write_thread(j, thread_ids[k]) < 0) {
                                LOG("Error in wt_write_thread (j = %zu, "
                                    "thread_ids[k] = %ju)", j, thread_ids[k]);
                                had_errors = 1;
                        }
                }

                wt_write_board(j, thread_ids, thread_ids_num, board_pages_num);
done_with_this_board:

                if (need_to_unlock) {
                        lock_release(j);
                }
        }

        if (lock_acquire_recent() < 0) {
                LOG("Error in lock_acquire_recent (500)");
                had_errors = 1;
                goto done;
        }

        wt_write_recent_page();
        lock_release_recent();
done:

        return had_errors;
}
