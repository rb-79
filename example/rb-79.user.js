// ==UserScript==
// @name        RB-79 Userscript         [ Change this ]
// @namespace   RB-79                    [ Change this ]
// @description Fancy features for RB-79 [ Change this ]
// @include     http://127.0.0.1/*       [ Change this ]
// @version     1
// @grant       none
// ==/UserScript==

var img_re = /\.(png|gif|jpg|webp|jpeg|tif|tiff|bpg|svg)$/i;
var video_re = /\.(webm|mkv|mp4|mp3|ogg|m4a|flac|opus|mka)$/i;

/*
 * Turn off any sort of link for the userscript that's being advertised
 */
document.querySelectorAll('.userscript-link').forEach(function (b) {
  b.parentNode.removeChild(b);
});

/*
 * Turn all post numbers into clickable links, for quoting and thread-jumping
 */
if (window.location.href.includes('/res/')) {
  document.querySelectorAll('span.post-number').forEach(function (s) {
    s.onclick = function () {
      document.querySelectorAll('textarea[name=comment]').forEach(function (t) {
        t.value = t.value + '>>' + s.innerText.replace(/No. +/, '') + '\n';
      });
    };
    s.innerHTML = '<a href="#" class="a-subtle">' + s.innerHTML + '</a>';
  });
} else {
  document.querySelectorAll('div.thread_preview').forEach(function (d) {
    var thread_url = '';
    for (var j = 0; j < d.children.length; j++) {
      var c = d.children[j];
      if (c.className == 'op-post') {
        c.querySelectorAll('a').forEach(function (a) {
          if (a.href.includes('/res/')) {
            thread_url = a.href;
          }
        });
      }
      if (c.className == 'op-post' || c.className == 'reply-container') {
        c.querySelectorAll('span.post-number').forEach(function (s) {
          var post_number = s.innerText.replace(/No. +/, '');
          s.innerHTML = '<a href="' + thread_url + '#post' + post_number +
          '" class="a-subtle">' + s.innerHTML + '</a>';
        });
      }
    }
  });
}

/*
 * Shift+Click to remove files
 */
document.querySelectorAll('input[type=file]').forEach(function (i) {
  i.title = 'Shift+Click to remove';
  i.onclick = function (ev) {
    if (ev.shiftKey) {
      i.value = '';
      return false;
    }
    return true;
  };
});

/*
 * Expand images on click
 */
document.querySelectorAll('a.filelink').forEach(function (s) {
  if (img_re.test(s.href)) {
    s.onclick = function (ev) {
      if (ev.which == 2) {
        return true;
      }
      if (!this.expanded) {
        this.childNodes[0].style.maxWidth = '95vw';
        this.childNodes[0].style.maxHeight = '95vh';
        this.expanded = this.childNodes[0].src;
        this.childNodes[0].src = this.href;
      } else {
        this.childNodes[0].src = this.expanded;
        this.expanded = '';
      }
      return false;
    };
  } else if (video_re.test(s.href)) {
    s.onclick = function (ev) {
      if (ev.which == 2) {
        return true;
      }
      if (!this.expanded) {
        var aNode = this;
        aNode.expanded = 'yes';
        var thumbNode = this.childNodes[0];
        var parentNode = this.parentNode;
        thumbNode.style.display = 'none';
        var videoNode = document.createElement('video');
        videoNode.src = this.href;
        videoNode.controls = ' ';
        videoNode.loop = ' ';
        videoNode.autoplay = ' ';
        var closeNode = document.createElement('div');
        var lBracket = document.createTextNode('[');
        var closeLink = document.createElement('a');
        var rBracket = document.createTextNode(']');
        closeLink.href = '#';
        closeLink.innerText = 'Close';
        closeLink.onclick = function (ev1) {
          thumbNode.style.display = '';
          aNode.expanded = '';
          parentNode.removeChild(videoNode);
          parentNode.removeChild(closeNode);
          return false;
        };
        closeNode.appendChild(lBracket);
        closeNode.appendChild(closeLink);
        closeNode.appendChild(rBracket);
        parentNode.appendChild(closeNode);
        parentNode.appendChild(videoNode);
      }
      return false;
    };
  }
});

/*
 * IQDB link for images
 */
document.querySelectorAll('a.filelink').forEach(function (s) {
   if (s.childElementCount > 0 && s.childNodes[0].nodeType == 1 && s.childNodes[0].tagName.toLowerCase() == "img") {
     return true;
   }
   if (img_re.test(s.href)) {
     var parentNode = s.parentNode;
     var url = location.protocol + "//iqdb.org/?url=" + s.href;
     var aNode = document.createElement('a');
     aNode.href = url;
     aNode.innerText = "IQDB";
     var lBracket = document.createTextNode(' [');
     var rBracket = document.createTextNode(']');
     parentNode.appendChild(lBracket);
     parentNode.appendChild(aNode);
     parentNode.appendChild(rBracket);
   } else if (video_re.test(s.href)) {
     var parentNode = s.parentNode;
     var thumbNode = parentNode.parentNode.childNodes[4].childNodes[0].childNodes[0];
     var url = location.protocol + "//iqdb.org/?url=" + thumbNode.src;
     var aNode = document.createElement('a');
     aNode.href = url;
     aNode.innerText = "IQDB";
     var lBracket = document.createTextNode(' [');
     var rBracket = document.createTextNode(']');
     parentNode.appendChild(lBracket);
     parentNode.appendChild(aNode);
     parentNode.appendChild(rBracket);
   }
   return true;
});
