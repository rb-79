/*
 * Copyright (c) 2017-2020, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sodium.h>

#include "macros.h"
#include "rb79.h"

#include "config.h"

const char *program_name = "rb79-ban-ip";

/* Show usage message, do not exit */
void
usage(void)
{
        size_t len = strlen(program_name);

        printf("Usage: %s [ -i individual-ip ] # -i, -f mutually exclusive\n",
               program_name);
        printf("       %*s [ -f first-ip -l last-ip ]\n", (int) len, "");
        printf("       %*s [ -m ban-message ]\n", (int) len, "");
        printf("       %*s [ -b board-name ] # default: global ban\n",
               (int) len, "");
        printf("       %*s [ -u banned-until ] # default: 7 days from now\n",
               (int) len, "");
}

/* Do the thing */
int
main(int argc, char **argv)
{
        int ret = EINVAL;
        struct configuration conf = { 0 };
        int opt = 0;
        const char *b_arg = 0;
        size_t board_idx = 0;
        uint_fast8_t global_ban = 0;
        const char *first_ip = 0;
        char *normalized_first_ip = 0;
        const char *last_ip = 0;
        char *normalized_last_ip = 0;
        const char *ban_message = 0;
        uint_fast8_t is_secret = 0;
        const char *raw_until = 0;
        time_t until = 0;

        setlocale(LC_ALL, "");

        /* Parse options */
        while ((opt = getopt(argc, argv, "si:f:l:m:u:b:")) != -1) {
                switch (opt) {
                case 's':
                        is_secret = 1;
                        break;
                case 'b':

                        if (b_arg) {
                                ERROR_MESSAGE("-b already specified");
                                goto done;
                        }

                        b_arg = optarg;
                        break;
                case 'i':

                        if (first_ip) {
                                ERROR_MESSAGE("Only one IP address or"
                                              " range may be specified");
                                goto done;
                        }

                        first_ip = optarg;
                        last_ip = optarg;
                        break;
                case 'f':

                        if (first_ip) {
                                ERROR_MESSAGE("Only one IP address or"
                                              " range may be specified");
                                goto done;
                        }

                        first_ip = optarg;
                        break;
                case 'l':

                        if (last_ip) {
                                ERROR_MESSAGE("Only one IP address or"
                                              " range may be specified");
                                goto done;
                        }

                        last_ip = optarg;
                        break;
                case 'm':

                        if (ban_message) {
                                ERROR_MESSAGE("Only one ban message"
                                              " may be specified");
                                goto done;
                        }

                        ban_message = optarg;
                        break;
                case 'u':

                        if (raw_until) {
                                ERROR_MESSAGE("Only one expiry time"
                                              " may be specified");
                                goto done;
                        }

                        raw_until = optarg;
                        break;
                default:
                        usage();
                        goto done;
                }
        }

        if (!first_ip ||
            !last_ip) {
                ERROR_MESSAGE("An IP address or range must be specified");
                usage();
                goto done;
        }

        if (!ban_message) {
                ban_message = "";
        }

        if (raw_until) {
                until = (time_t) strtoull(raw_until, 0, 0);
        } else {
                until = time(0) + (7 * 24 * 60 * 60);
        }

        conf = (struct configuration) {
                /* */
                .static_www_folder = static_www_folder,             /* */
                .work_path = work_path,                             /* */
                .temp_dir_template = temp_dir_template,             /* */
                .trip_salt = trip_salt,                             /* */
                .trip_salt_len = strlen(trip_salt),                 /* */
                .boards = boards,                                   /* */
                .boards_num = NUM_OF(boards),                       /* */
                .max_form_data_size = max_form_data_size,           /* */
                .max_file_size = max_file_size,                     /* */
                .max_text_len = max_text_len,                       /* */
                .filetypes = filetypes,                             /* */
                .filetypes_num = NUM_OF(filetypes),                 /* */
                .file_description_prog = file_description_prog,     /* */
                .headers = headers,                                 /* */
                .headers_num = NUM_OF(headers),                     /* */
                .challenges = challenges,                           /* */
                .challenges_num = NUM_OF(challenges),               /* */
                .wordfilter_inputs = wordfilter_inputs,             /* */
                .wordfilter_inputs_num = NUM_OF(wordfilter_inputs), /* */
                .forbidden_inputs = forbidden_inputs,               /* */
                .forbidden_inputs_num = NUM_OF(forbidden_inputs),   /* */
        };

        /* Interpret board */
        if (!b_arg) {
                global_ban = 1;
        } else {
                board_idx = (size_t) -1;

                for (size_t j = 0; j < conf.boards_num; ++j) {
                        if (!strcmp(conf.boards[j].name, b_arg)) {
                                board_idx = j;
                        }
                }

                if (board_idx == (size_t) -1) {
                        ERROR_MESSAGE("No board \"%s\" known", b_arg);
                        goto done;
                }
        }

        /* Interpret IP addresses */
        if (util_normalize_ip(first_ip, &normalized_first_ip) < 0) {
                goto done;
        }

        if (util_normalize_ip(last_ip, &normalized_last_ip) < 0) {
                goto done;
        }

        /* Set up a minimal part of the system */
        if (setup_locks(&conf) < 0) {
                goto done;
        }

        if (setup_dbs(&conf) < 0) {
                goto done;
        }

        if (setup_write_thread(&conf) < 0) {
                goto done;
        }

        ret = db_insert_ban(global_ban, board_idx, normalized_first_ip,
                            normalized_last_ip, ban_message, is_secret, time(0),
                            until);
done:
        clean_locks();
        clean_dbs();
        clean_write_thread();
        free(normalized_first_ip);
        free(normalized_last_ip);

        return ret;
}
