/*
 * Copyright (c) 2017-2020, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include <magic.h>

#include "macros.h"
#include "rb79.h"

static magic_t mime_cookie;
static char *temp_dir;

/* Global configuration */
static const struct configuration *conf;

/* A buffer this long can hold any static_thumbnail path */
static size_t max_static_thumb_len;

/*
 * Set up libmagic and all its friends
 *
 * Preconditions:
 *
 *  - setup_sanitize_file() was not invoked more recently than
 *    clean_sanitize_file().
 *
 * Postconditions (success):
 *
 *  - Any other function in this file may be safely called.
 */
int
setup_sanitize_file(const struct configuration *in_conf)
{
        conf = in_conf;
        max_static_thumb_len = 1;

        for (size_t j = 0; j < conf->filetypes_num; j++) {
                const struct filetype *f = &conf->filetypes[j];

                if (f->static_thumbnail) {
                        size_t t = strlen(f->static_thumbnail);

                        if (t + 1 > max_static_thumb_len) {
                                max_static_thumb_len = t + 1;
                        }
                }
        }

        if (!(mime_cookie = magic_open(MAGIC_MIME_TYPE))) {
                ERROR_MESSAGE("magic_open(): %s", magic_error(mime_cookie));

                return -1;
        }

        if (magic_load(mime_cookie, 0) < 0) {
                ERROR_MESSAGE("magic_open(): %s", magic_error(mime_cookie));

                return -1;
        }

        if (!(temp_dir = malloc(strlen(conf->temp_dir_template) + 1))) {
                return -1;
        }

        sprintf(temp_dir, "%s", conf->temp_dir_template);

        if (!(mkdtemp(temp_dir))) {
                PERROR_MESSAGE("mkdtemp");

                return -1;
        }

        return 0;
}

/*
 * Check the MIME type of the content of buf
 *
 * Preconditions:
 *
 *  - setup_sanitize_file() was invoked more recently than
 *    clean_sanitize_file().
 *
 *  - buf is memory of length len.
 *
 *  - out_filetype is not 0.
 *
 *  - Overwriting *out_filetype shall not cause a memory leak.
 *
 * Postconditions (success):
 *
 *  - The contents of buf have been examined by libmagic. If the
 *    MIME type corresponds to an entry in allowed_filetypes,
 *    *out_filetype is that entry. If not, *out_filetype is 0.
 */
int
sf_check_mime_type(const char *buf, size_t len, const struct
                   filetype **out_filetype)
{
        const char *mime = 0;

        if (!buf ||
            !len) {
                *out_filetype = 0;

                return 0;
        }

        if (!(mime = magic_buffer(mime_cookie, buf, len))) {
                ERROR_MESSAGE("magic_buffer(): %s", magic_error(mime_cookie));

                return -1;
        }

        for (size_t j = 0; j < conf->filetypes_num; ++j) {
                if (!strcmp(mime, conf->filetypes[j].mime_type)) {
                        *out_filetype = &conf->filetypes[j];

                        return 0;
                }
        }

        LOG("The mime type \"%s\" is unsupported", mime);

        return -1;
}

/*
 * Run file_description_prog, store the result in out_description
 *
 * It is intended that this be called lazily by write-thread, because
 * the standard posting path (at present) inserts into the database
 * before saving files (to reduce error paths).
 *
 * Preconditions:
 *
 *  - setup_sanitize_file() was invoked more recently than
 *    clean_sanitize_file().
 *
 *  - mimetype is a string of length mimetype_len, and filepath is
 *    a string of length filepath_len.
 *
 *  - filepath is the absolute path to the file in question, so is
 *    something like "/var/www/rb79/m/src/1942067545.jpg".
 *
 *  - The mimetype of that file is mimetype (this should be satisfied
 *    if both strings come from a db row), so is something like
 *    "image/jpeg".
 *
 *  - out_description and out_len are not 0.
 *
 *  - Overwriting *out_description shall not cause a memory leak.
 *
 * Postconditions (success):
 *
 *  - *out_description is a string of length *out_len, and is
 *    suitable for inclusion directly into HTML.
 */
int
sf_describe_file(const char *mimetype, const char *filepath,
                 char **out_description, size_t *out_description_len)
{
        /* XXX: do we need to do any signal magic here? */
        int ret = -1;
        int fds[2];
        pid_t fret = 0;
        ssize_t rret = 0;
        size_t pos = 0;
        size_t dlen = 128;

        if (!filepath ||
            !mimetype) {
                ret = 0;
                goto done;
        }

        if (pipe(fds) < 0) {
                PERROR_MESSAGE("pipe");
                goto done;
        }

        /* XXX: should we have an automagically growing buffer? */
        if (!(*out_description = calloc(dlen, sizeof **out_description))) {
                PERROR_MESSAGE("calloc");
                goto done;
        }

        if ((fret = fork()) == -1) {
                PERROR_MESSAGE("fork");
                goto done;
        }

        if (!fret) {
                /* We are child: ``1>fds[1] 2>&1''  */
                close(fds[0]);
                dup2(fds[1], STDOUT_FILENO);
                dup2(fds[1], STDERR_FILENO);
                execlp(conf->file_description_prog, conf->file_description_prog,
                       mimetype, filepath, (const char *) 0);

                /* An error has occured but we dare not log */
                _exit(0);
        }

        /* We are parent */
        close(fds[1]);

        do {
                rret = read(fds[0], *out_description + pos, dlen - pos - 1);

                if (rret == -1) {
                        close(fds[0]);
                        PERROR_MESSAGE("read");
                        goto done;
                }

                pos += rret;

                if (pos >= dlen - 1) {
                        LOG("$(%s %s %s) is too long, aborting",
                            conf->file_description_prog, mimetype, filepath);
                        break;
                }
        } while (rret);

        /* If file_description_prog is rampant, this blocks. */
        waitpid(fret, 0, 0);
        (*out_description)[pos] = '\0';
        *out_description_len = pos;
        close(fds[0]);
        ret = 0;
done:

        return ret;
}

/*
 * Treat the contents of buf as a file. Run board.install_command
 * and board.thumb_creation_command (if relevant) to place it at
 * some place like /var/www/rb79/sf/src/2684425523.png. Record that
 * full path in *out_abs_path. Record a truncated version, suitable
 * for use in <a>, into *out_path. Record the thumbnail's path (also
 * suitable for <a>) in *out_thumb_path.
 *
 * Preconditions:
 *
 *  - setup_sanitize_file() was invoked more recently than
 *    clean_sanitize_file().
 *
 *  - board_idx represents a board, AND THE LOCK IS HELD.
 *
 *  - buf is memory of length len, and when treated as a file has
 *    MIME type corresponding to *filetype.
 *
 *  - now, out_abs_path, out_path, out_thumb_path, our_fault are
 *    not 0.
 *
 *  - *now does not correspond to a timestamp of any other file
 *    (otherwise the filenames will clobber).
 *
 *  - Overwriting *out_abs_path, *out_path, and *out_thumb_path
 *    shall not cause a memory leak.
 *
 * Postconditions (success):
 *
 *  - *out_abs_path is an absolute path, like
 *    "/var/www/rb79/sf/src/12343252.jpg".
 *
 *  - *out_path is a path suitable for use in <a>, like
 *    "/sf/src/12343242.jpg".
 *
 *  - *out_thumb_path is also a path suitable for use in <a>.
 *
 *  - *out_path_len and *out_thumb_path_len are the relevant lengths
 *    (note there is no out length for *out_abs_path.
 *
 *  - buf was written to disk, and filetype's .install_command was
 *    run with that file as the first argument and *out_abs_path
 *    as the second argument.
 *
 *  - If filetype.thumb_creation_command is not 0, it was run with
 *    that file as the first argument and the file corresponding
 *    to *out_thumb_path as the second.
 *
 *  - Otherwise, *out_thumb_path is a copy of filetype.static_thumbnail.
 */
int
sf_install_files(size_t board_idx, const char *buf, size_t len, time_t *now,
                 const struct filetype *filetype, char **out_abs_path,
                 char **out_path,
                 size_t *out_path_len, char **out_thumb_path,
                 size_t *out_thumb_path_len,
                 int *our_fault)
{
        int ret = -1;
        size_t abs_path_len = snprintf(0, 0, "%s/%s/src/%ju.%s",
                                       conf->static_www_folder,
                                       conf->boards[board_idx].name,
                                       (uintmax_t) -1,
                                       filetype->ext);
        size_t thumb_path_len = snprintf(0, 0, "%s/%s/src/%jus.png",
                                         conf->static_www_folder,
                                         conf->boards[board_idx].name,
                                         (uintmax_t) -1);
        size_t temp_path_len = snprintf(0, 0, "%s/%ju.%s", temp_dir,
                                        (uintmax_t) -1, filetype->ext);

        if (max_static_thumb_len > thumb_path_len) {
                thumb_path_len = max_static_thumb_len;
        }

        char *abs_path = 0;
        char *system_path = 0;
        char *thumb_path = 0;
        char *temp_path = 0;
        size_t thumb_cmd_len = 0;
        char *thumb_cmd = 0;
        size_t full_cmd_len = 0;
        char *full_cmd = 0;
        FILE *out = 0;

        if (abs_path_len + 1 < abs_path_len) {
                ERROR_MESSAGE("overflow");
                *our_fault = 1;
                goto done;
        }

        if (!(abs_path = malloc(abs_path_len + 1))) {
                PERROR_MESSAGE("malloc");
                *our_fault = 1;
                goto done;
        }

        if (!(system_path = malloc(abs_path_len + 1))) {
                PERROR_MESSAGE("malloc");
                *our_fault = 1;
                goto done;
        }

        if (thumb_path_len + 1 < thumb_path_len) {
                ERROR_MESSAGE("overflow");
                *our_fault = 1;
                goto done;
        }

        if (!(thumb_path = malloc(thumb_path_len + 1))) {
                PERROR_MESSAGE("malloc");
                *our_fault = 1;
                goto done;
        }

        if (temp_path_len + 1 < temp_path_len) {
                ERROR_MESSAGE("overflow");
                *our_fault = 1;
                goto done;
        }

        if (!(temp_path = malloc(temp_path_len + 1))) {
                PERROR_MESSAGE("malloc");
                *our_fault = 1;
                goto done;
        }

        while (1) {
                FILE *f = 0;

                sprintf(abs_path, "%s/%s/src/%ju.%s", conf->static_www_folder,
                        conf->boards[board_idx].name, (uintmax_t) *now,
                        filetype->ext);
                f = fopen(abs_path, "r");

                if (f) {
                        /*
                         * Since we have the filesystem lock, this
                         * should correctly avoid file clobbering.
                         */
                        if (fclose(f)) {
                                PERROR_MESSAGE("fclose");
                                *our_fault = 1;
                                goto done;
                        }

                        (*now)++;
                        continue;
                }

                if (errno == ENOENT) {
                        break;
                }

                PERROR_MESSAGE("fopen");
                *our_fault = 1;
                goto done;
        }

        /* At this point, *now and abs_path are correct */
        sprintf(thumb_path, "%s/%s/src/%jus.png", conf->static_www_folder,
                conf->boards[board_idx].name, (uintmax_t) *now);
        sprintf(temp_path, "%s/%ju.%s", temp_dir, (uintmax_t) *now,
                filetype->ext);

        if (!(out = fopen(temp_path, "w"))) {
                PERROR_MESSAGE("fopen");
                ERROR_MESSAGE("cannot open path \"%s\"", temp_path);
                *our_fault = 1;
                goto done;
        }

        if (fwrite(buf, 1, len, out) < len) {
                PERROR_MESSAGE("fwrite");
                ERROR_MESSAGE("cannot write to  path \"%s\"", temp_path);
                *our_fault = 1;
                goto done;
        }

        if (fclose(out)) {
                PERROR_MESSAGE("fclose");
                *our_fault = 1;
                goto done;
        }

        /* Now we have something on the filesystem */
        if (filetype->thumb_creation_command) {
                thumb_cmd_len = snprintf(0, 0, filetype->thumb_creation_command,
                                         temp_path, thumb_path);

                if (!(thumb_cmd = malloc(thumb_cmd_len + 1))) {
                        PERROR_MESSAGE("malloc");
                        *our_fault = 1;
                        goto done;
                }

                sprintf(thumb_cmd, filetype->thumb_creation_command, temp_path,
                        thumb_path);
                int tc_ret = system(thumb_cmd);

                if (!WIFEXITED(tc_ret)) {
                        ERROR_MESSAGE(
                                "Thumnail cmd \u00ab%s\u00bb did not exit",
                                thumb_cmd);
                        LOG("Thumnail cmd \u00ab%s\u00bb did not exit",
                            thumb_cmd);
                        goto done;
                } else if (WEXITSTATUS(tc_ret)) {
                        LOG("Thumnail cmd \u00ab%s\u00bb exited %d", thumb_cmd,
                            (int) WEXITSTATUS(tc_ret));
                        goto done;
                }
        } else if (filetype->static_thumbnail) {
                sprintf(thumb_path, "%s", filetype->static_thumbnail);
        }

        full_cmd_len = snprintf(0, 0, filetype->install_command, temp_path,
                                abs_path);

        if (full_cmd_len + 1 < full_cmd_len) {
                ERROR_MESSAGE("overflow");
                *our_fault = 1;
                goto done;
        }

        if (!(full_cmd = malloc(full_cmd_len + 1))) {
                PERROR_MESSAGE("malloc");
                *our_fault = 1;
                goto done;
        }

        sprintf(full_cmd, filetype->install_command, temp_path, abs_path);
        int fc_ret = system(full_cmd);

        if (!WIFEXITED(fc_ret)) {
                ERROR_MESSAGE("Install cmd \u00ab%s\u00bb did not exit",
                              full_cmd);
                LOG("Install cmd \u00ab%s\u00bb did not exit", full_cmd);
                goto done;
        } else if (WEXITSTATUS(fc_ret)) {
                LOG("Full cmd \u00ab%s\u00bb exited %d", full_cmd,
                    (int) WEXITSTATUS(fc_ret));
                goto done;
        }

        /* Now *now is correct, files are where they need to be. */
        ret = 0;

        /* Cut base_path out for out_path and out_thumb_path */
        if (filetype->thumb_creation_command) {
                sprintf(thumb_path, "/%s/src/%jus.png",
                        conf->boards[board_idx].name, (uintmax_t) *now);
        }

        sprintf(system_path, "/%s/src/%ju.%s", conf->boards[board_idx].name,
                (uintmax_t) *now, filetype->ext);
done:

        if (temp_path) {
                unlink(temp_path);
        }

        free(temp_path);
        free(full_cmd);
        free(thumb_cmd);
        *out_abs_path = abs_path;
        *out_path = system_path;
        *out_path_len = strlen(*out_path);
        *out_thumb_path = thumb_path;
        *out_thumb_path_len = strlen(*out_thumb_path);

        return ret;
}

/*
 * Clean up any memory from this file
 *
 * Postconditions (success):
 *
 *  - Valgrind won't report any memory leas from this file.
 *
 *  - setup_sanitize_file() can safely be called again.
 */
int
clean_sanitize_file(void)
{
        /* XXX: is this safe if errors in setup_sanitize_file()? */
        magic_close(mime_cookie);
        mime_cookie = (magic_t) { 0 };
        conf = 0;

        /*
         * Note: we explicitly don't unlink temp_dir. If this thing
         * crashes, I want to know what's there. It will certainly
         * be the file of whatever caused us to crash. That's like
         * a log file, and we shouldn't delete logs.
         */
        free(temp_dir);
        temp_dir = 0;

        return 0;
}
