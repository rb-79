#!/bin/sh

format="${1}"
file="${2}"

size=$(/usr/bin/du -h "${file}" | /usr/bin/cut -d'	' -f1)

case "${format}" in
        "video/"*)
                width=$( ffprobe -v error -of flat=s=_ \
                                -select_streams v:0 \
                                -show_entries stream=width  \
                                "${file}" | \
                        cut -d= -f2)
                height=$(ffprobe -v error -of flat=s=_ \
                                -select_streams v:0 \
                                -show_entries stream=height \
                                "${file}" | \
                        cut -d= -f2)
                duration=$(ffprobe -v error -of default=noprint_wrappers=1:nokey=1 \
                                -select_streams v:0 \
                                -show_entries format=duration\
                                -sexagesimal \
                                "${file}" | \
                        cut -d= -f2)
                printf "%s, " "${format}"
                printf "%s, " "${size}"
                printf "%sx%s, " "${width}" "${height}"
                printf "%s" "${duration}"
                ;;
        "image/"*)
                width=$( ffprobe -v error -of flat=s=_ \
                                -select_streams v:0 \
                                -show_entries stream=width  \
                                "${file}" | \
                        cut -d= -f2)
                height=$(ffprobe -v error -of flat=s=_ \
                                -select_streams v:0 \
                                -show_entries stream=height \
                                "${file}" | \
                        cut -d= -f2)
                printf "%s, " "${format}"
                printf "%s, " "${size}"
                printf "%sx%s" "${width}" "${height}"
                ;;
        "application/epub+zip")
                printf "%s, " "${format}"
                printf "%s" "${size}"
                ;;
        *)
                printf %s "${format}"
                ;;
esac
