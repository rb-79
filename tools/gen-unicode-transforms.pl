#!/usr/bin/perl -w -CS

# Copyright (c) 2017, De Rais <derais@cock.li>
# 
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all
# copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

# This tool creates a header file for use in sanitize-comment.c.
# The goal is to turn Unicode input into Unicode input that can be
# easily searched (via regex or such). To do this, there are a few
# conceptual steps.
#
#  - Perform every transformation listed in confusables.txt.  (This
#    turns £ into L).
#
#  - Replace every character with its Decompositional_Mapping, if
#    appropriate. (This turns ᴬ into A.)
#
#  - Delete every character whose General_Category is Mark, Control,
#    Format, Modifier_Symbol, Punctuation (this gets rid of *, the
#    ( and ) from ⑨ , etc.)
#
#  - Delete every character whose General_Category is Space, but
#    is not U+0020. (This gets rid of U+200A HAIR SPACE, etc.)
#
# The problem is that these operations do not obviously compose
# nicely. It's not assured that a Decompositional_Mapping won't
# result in a confusable character, nor that a confusable transformation
# will never result in a decomposable character. So we (conceptually)
# create the identity transformation, then repeatedly hit it with
# all four of these transformations until it (hopefully) stabilizes.
# There's no guarantee that it will actually stabilize, which is
# why it's broken out into a separate script.

use strict;
use warnings;
use utf8;

use Data::Dumper;

$| = 1;

# Key: a string like "00A3". Value: an array like [ "004C" ]
my $transform = {};

# We start off with the recommendations of confusables.txt
#open CONFUSABLES, "wget -q -O- http://www.unicode.org/Public/security/latest/confusables.txt|" or die;
open CONFUSABLES, "<confusables.txt" or die;
while (<CONFUSABLES>) {
        my ($from, $to) = $_ =~ m/^\s*([0-9A-Fa-f]+)\s*;\s*([0-9A-Fa-f ]+)\s*;/;

        if (not $to) {
                next;
        }

        $transform->{$from} = [ split / /, $to ];
}
close CONFUSABLES;

# We now add the decompositional mappings
#open EVERYTHING, "wget -q -O- http://www.unicode.org/Public/UNIDATA/UnicodeData.txt|" or die;
open EVERYTHING, "<UnicodeData.txt" or die;
while (<EVERYTHING>) {
        my @contents = split /;/,$_;
        my $codepoint = $contents[0];
        my $codepoint_h = hex($codepoint);
        my $name = $contents[1];
        my $general_category = $contents[2];
        my $decompositional_mapping = $contents[5];
        $decompositional_mapping =~ s/<[^>]*>\s*//;

        if ($general_category =~ /^(M|C|P|Sk)/ and $codepoint ne "0020") {
                $transform->{$codepoint} = [];
                next;
        }

        # These take up more space than they're worth
        if ($name =~ /^(ARABIC LIGATURE|CJK|HIRANGA|HANGUL|TAMIL|CANADIAN)/) {
                $transform->{$codepoint} = [];
                next;
        }

        # Now, do the decompositions and the confusables have a
        # contest? Confusables win.
        if ($transform->{$codepoint}) {
                next;
        }

        # Otherwise, put in stuff for decomposition
        if ($decompositional_mapping ne "") {
                $transform->{$codepoint} = [ split / /, $decompositional_mapping ];
        }
}
close EVERYTHING;

# A bunch of CJK stuff
for (my $codepoint_h = 0x31c0; $codepoint_h <= 0x9fff; ++$codepoint_h) {
        my $codepoint = uc sprintf "%04x", $codepoint_h;
        $transform->{$codepoint} = [];
}

# A bunch of Arabic stuff
for (my $codepoint_h = 0x1ee00; $codepoint_h <= 0x1eeff; ++$codepoint_h) {
        my $codepoint = uc sprintf "%04x", $codepoint_h;
        $transform->{$codepoint} = [];
}

# Some minor languages that don't really look like Latin-1
for (my $codepoint_h = 0x1b00; $codepoint_h <= 0x1ccf; ++$codepoint_h) {
        my $codepoint = uc sprintf "%04x", $codepoint_h;
        $transform->{$codepoint} = [];
}

# Squared capital letters that haven't caught up
for (my $codepoint_h = 0x1f170; $codepoint_h <= 0x1f189; ++$codepoint_h) {
        my $dest = uc sprintf "%04x", ($codepoint_h - 0x1f170 + 0x41);
        my $codepoint = uc sprintf "%04x", $codepoint_h;
        $transform->{$codepoint} = [ $dest ];
        $codepoint = uc sprintf "%04x", ($codepoint_h - 0x1f170 + 0x1f130);
        $transform->{$codepoint} = [ $dest ];
}

# Some of the transforms are dumb.
delete $transform->{"0030"}; # 0
delete $transform->{"0031"}; # 1
delete $transform->{"0049"}; # I
delete $transform->{"006D"}; # m
delete $transform->{"0077"}; # w
delete $transform->{"007C"}; # |
$transform->{"FF29"} = [ "0049" ]; # Ｉ should go to I, not l

# The Unicode Consortium will never take these
$transform->{"00A9"} = [ "0043" ];
$transform->{"00AE"} = [ "0052" ];
$transform->{"00DF"} = [ "0042" ];
$transform->{"01AB"} = [ "0074" ];
$transform->{"0272"} = [ "006E" ];
$transform->{"0274"} = [ "004E" ];
$transform->{"0291"} = [ "007A" ];
$transform->{"0298"} = [ "004F" ];
$transform->{"029F"} = [ "004C" ];
$transform->{"02B3"} = [ "0072" ];
$transform->{"0629"} = [ "006F" ];
$transform->{"0644"} = [ "004A" ];
$transform->{"1472"} = [ "0062" ];
$transform->{"1473"} = [ "0062" ];
$transform->{"1D07"} = [ "0045" ];
$transform->{"2117"} = [ "0050" ];
$transform->{"2365"} = [ "004F" ];
$transform->{"A793"} = [ "0065" ];

$transform->{"2776"} = [ "0028", "0031", "0029" ];
$transform->{"2777"} = [ "0028", "0032", "0029" ];
$transform->{"2778"} = [ "0028", "0033", "0029" ];
$transform->{"2779"} = [ "0028", "0034", "0029" ];
$transform->{"277A"} = [ "0028", "0035", "0029" ];
$transform->{"277B"} = [ "0028", "0036", "0029" ];
$transform->{"277C"} = [ "0028", "0037", "0029" ];
$transform->{"277D"} = [ "0028", "0038", "0029" ];
$transform->{"277E"} = [ "0028", "0039", "0029" ];
$transform->{"277F"} = [ "0028", "0031", "0030", "0029" ];

$transform->{"2780"} = [ "0028", "0031", "0029" ];
$transform->{"2781"} = [ "0028", "0032", "0029" ];
$transform->{"2782"} = [ "0028", "0033", "0029" ];
$transform->{"2783"} = [ "0028", "0034", "0029" ];
$transform->{"2784"} = [ "0028", "0035", "0029" ];
$transform->{"2785"} = [ "0028", "0036", "0029" ];
$transform->{"2786"} = [ "0028", "0037", "0029" ];
$transform->{"2787"} = [ "0028", "0038", "0029" ];
$transform->{"2788"} = [ "0028", "0039", "0029" ];
$transform->{"2789"} = [ "0028", "0031", "0030", "0029" ];

$transform->{"278A"} = [ "0028", "0031", "0029" ];
$transform->{"278B"} = [ "0028", "0032", "0029" ];
$transform->{"278C"} = [ "0028", "0033", "0029" ];
$transform->{"278D"} = [ "0028", "0034", "0029" ];
$transform->{"278E"} = [ "0028", "0035", "0029" ];
$transform->{"278F"} = [ "0028", "0036", "0029" ];
$transform->{"2790"} = [ "0028", "0037", "0029" ];
$transform->{"2791"} = [ "0028", "0038", "0029" ];
$transform->{"2792"} = [ "0028", "0039", "0029" ];
$transform->{"2793"} = [ "0028", "0031", "0030", "0029" ];

# I disagree with m ~ rn and w ~ vv - moved to the iteration
$transform->{"0460"} = [ "0077" ];
# $transform->{"0461"} = [ "0077" ];
# $transform->{"047D"} = [ "0077" ];
# $transform->{"04CE"} = [ "006D" ];
# $transform->{"1D55e"} = [ "006D" ];
# $transform->{"1D568"} = [ "0077" ];
# $transform->{"1D592"} = [ "006D" ];
# $transform->{"1D59C"} = [ "0077" ];
# $transform->{"1D5C6"} = [ "006D" ];
# $transform->{"1D5D0"} = [ "0077" ];
# $transform->{"1D5FA"} = [ "006D" ];
# $transform->{"1D604"} = [ "0077" ];
# $transform->{"1D62E"} = [ "0077" ];
# $transform->{"1D638"} = [ "006D" ];
# $transform->{"1D662"} = [ "006D" ];
# $transform->{"1D66C"} = [ "0077" ];
# $transform->{"1D696"} = [ "006D" ];
# $transform->{"1D6A0"} = [ "0077" ];

# The unicode consortium might take these, but I don't care to wait
$transform->{"0138"} = [ "006B" ];
$transform->{"0185"} = [ "0062" ];
$transform->{"01A9"} = [ "03A3" ];
$transform->{"01F6"} = [ "0048" ];
$transform->{"024B"} = [ "0071" ];
$transform->{"0262"} = [ "0047" ];
$transform->{"0262"} = [ "0047" ];
$transform->{"0278"} = [ "03A6" ];
$transform->{"0280"} = [ "0052" ];
$transform->{"028A"} = [ "0055" ];
$transform->{"028C"} = [ "039B" ];
$transform->{"028D"} = [ "004D" ];
$transform->{"0299"} = [ "0042" ];
$transform->{"029C"} = [ "0048" ];
$transform->{"03C0"} = [ "006E" ];
$transform->{"03C7"} = [ "0058" ];
$transform->{"03DD"} = [ "0066" ];
$transform->{"0423"} = [ "0079" ];
$transform->{"0427"} = [ "0079" ];
$transform->{"0447"} = [ "0079" ];
$transform->{"04B6"} = [ "0079" ];
$transform->{"04B7"} = [ "0079" ];
$transform->{"1471"} = [ "0064" ];
$transform->{"1D00"} = [ "0041" ];
$transform->{"1D05"} = [ "0044" ];
$transform->{"1D0A"} = [ "004A" ];
$transform->{"1D18"} = [ "0050" ];
$transform->{"1D1B"} = [ "0054" ];
$transform->{"1E9F"} = [ "03B4" ];
$transform->{"A727"} = [ "0068" ];

delete $transform->{"03A3"};

# Now we have to run the transform on itself.

my $times = 0;
my $need_another_run = 1;
while ($need_another_run > 0) {
        $need_another_run = 0;
        $times++;
        if ($times > 8) {
                die "Look, we seem to be in some kind of a feedback loop here.";
        }

        foreach my $k (%$transform) {
                my $prev = Dumper($transform->{$k});
                $prev =~ s{\s+}{ }gs;
                if (not exists($transform->{$k})) {
                        next;
                }

                my @new = ();
                foreach my $v (@{$transform->{$k}}) {
                        if (exists($transform->{$v})) {
                                push @new, @{$transform->{$v}};
                        } else {
                                push @new, $v;
                        }
                }

                # I disagree that w~vv and m~rn
                if ($#new == 1 and $new[0] eq "0072" and $new[1] eq "006E") {
                        $transform->{$k} = [ "006D" ];
                } elsif ($#new == 1 and $new[0] eq "0076" and $new[1] eq "0076") {
                        $transform->{$k} = [ "0077" ];
                } else {
                        $transform->{$k} = [ @new ];
                }

                my $now = Dumper($transform->{$k});
                $now =~ s{\s+}{ }gs;

                if ($prev ne $now) {
                        $need_another_run = 1;
                }
        }
}

print "/* Autogenerated by gen-unicode-transforms.pl */\n";
print "struct translate {\n";
print "        wchar_t from_s;\n";
print "        wchar_t from_t;\n";
print "        const char *to;\n";
print "};\n";
print "\n";
print "static struct translate translates[] = {\n";

my $k_s = 0x20;
my $k_t = 0x20;
my $last_repl_str = "nope";
my $last_desc_str = "nope";
my $last_len = 0;

foreach my $k (sort { hex($a) <=> hex($b) } keys %$transform) {
        my $kh = hex($k);

        my $repl_str = "";
        my $desc_str = "";
        my $len = 0;
        foreach my $v (@{$transform->{$k}}) {
                my $vh = hex($v);
                if ($vh > 0 and $vh < 127) {
                        $repl_str = $repl_str . chr($vh);
                        $len += 1;
                } elsif ($vh < 0x10000) {
                        $repl_str = $repl_str . (sprintf "\\u%04x", $vh);
                        $len += 6;
                } else {
                        $repl_str = $repl_str . (sprintf "\\U%08x", $vh);
                        $len += 10;
                }

                if (chr($vh) eq "\n") {
                        $desc_str = $desc_str . "\\n";
                } elsif (chr($vh) eq "\t") {
                        $desc_str = $desc_str . "\\t";
                } else {
                        $desc_str = $desc_str . chr($vh);
                }
        }


        # Are we just continuing the range?
        if ($kh == $k_t + 1 and $last_repl_str eq $repl_str) {
                $k_t = $kh;
                next;
        }

        # Okay, we're not.  We've got to print out that last thing
        if ($k_s != 0x20) {
                print "        {";
                printf ".from_s = 0x%x", $k_s;
                printf ", .from_t = 0x%x", $k_t;
                printf ", .to = \"%s\" }, ", $last_repl_str;
                print " "x(30 - $last_len);
                printf "/* \"%s\"..\"%s\"", chr($k_s), chr($k_t);
                printf " -> \"%s\" */\n", $last_desc_str;
        }

        # Now we start a new range
        $k_s = $kh;
        $k_t = $kh;
        $last_repl_str = $repl_str;
        $last_desc_str = $desc_str;
        $last_len = $len;
}

# And print the last thing out
print "        {";
printf ".from_s = 0x%x", $k_s;
printf ", .from_t = 0x%x", $k_t;
printf ", .to = \"%s\" }, ", $last_repl_str;
print " "x(20 - $last_len);
printf "/* \"%s\"..\"%s\"", chr($k_s), chr($k_t);
printf " -> \"%s\" */\n", $last_repl_str;
print "};";
