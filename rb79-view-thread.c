/*
 * Copyright (c) 2017-2020, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sodium.h>

#include "macros.h"
#include "rb79.h"

#include "config.h"

const char *program_name = "rb79-view-thread";

/* Options */
static uint_fast8_t use_color = 1;

/* Show usage message, do not exit */
void
usage(void)
{
        size_t len = strlen(program_name);

        printf("Usage: %s -b board-name\n", program_name);
        printf("       %*s -t thread-id\n", (int) len, "");
        printf("       %*s [ -C ] # Do not use color\n", (int) len, "");
}

/* Print out enough of a post to give the mod information */
int
print_post(struct prepared_post *p, FILE *f, uint_fast8_t is_op, uint_fast8_t
           is_summary, uint_fast8_t is_recent, const char *board_name, uintmax_t
           in_thread,
           uintmax_t total_posts_in_thread, uint_fast8_t hr_before)
{
        char *time_str = util_iso8601_from_time_t(p->now);
        unsigned char out_hash[9] = { 0 };

        UNUSED(is_summary);
        UNUSED(is_recent);
        UNUSED(hr_before);
        UNUSED(board_name);
        UNUSED(in_thread);
        UNUSED(total_posts_in_thread);

        if (crypto_generichash(out_hash, 9, (const unsigned char *) p->ip,
                               p->ip_len, 0, 0)) {
                PERROR_MESSAGE("crypto_generichash_init");
                goto done;
        }

        if (is_op) {
                fprintf(f, "--------------------------------------\n");
        }

        fprintf(f, " ");

        if (use_color) {
                fprintf(f, "\033[48;2;%d;%d;%dm  ", 0xff & out_hash[0], 0xff &
                        out_hash[1], 0xff & out_hash[2]);
                fprintf(f, "\033[48;2;%d;%d;%dm  ", 0xff & out_hash[3], 0xff &
                        out_hash[4], 0xff & out_hash[5]);
                fprintf(f, "\033[48;2;%d;%d;%dm  ", 0xff & out_hash[6], 0xff &
                        out_hash[7], 0xff & out_hash[8]);
                fprintf(f, "\033[0m");
        } else {
                fprintf(f, "      ");
        }

        fprintf(f, " %s%*s", p->ip, (int) (20 > p->ip_len ? 20 - p->ip_len : 1),
                " ");
        fprintf(f, "%s  ", UBSAFES(time_str));
        fprintf(f, "No. %ju\n", p->id);

        if (p->file_name) {
                fprintf(f, "        [ File: %s ]\n", p->file_name);
        }

        fprintf(f, "        ");

        if (p->comment_len > 40) {
                fprintf(f, "%.*s...", 37, p->comment);
        } else if (p->comment_len) {
                fprintf(f, "%s", p->comment);
        }

        fprintf(f, "\n");
        fprintf(f, "--------------------------------------\n");
done:
        free(time_str);

        return 0;
}

/* Do the thing */
int
main(int argc, char **argv)
{
        int ret = EINVAL;
        struct configuration conf = { 0 };
        int opt = 0;
        const char *b_arg = 0;
        size_t board_idx = 0;
        const char *t_arg = 0;
        uintmax_t thread_id = 0;

        setlocale(LC_ALL, "");

        /* Parse options */
        while ((opt = getopt(argc, argv, "b:t:C")) != -1) {
                switch (opt) {
                case 'b':

                        if (b_arg) {
                                ERROR_MESSAGE("-b already specified");
                                goto done;
                        }

                        b_arg = optarg;
                        break;
                case 't':

                        if (t_arg) {
                                ERROR_MESSAGE("-t already specified");
                                goto done;
                        }

                        t_arg = optarg;
                        break;
                case 'C':
                        use_color = 0;
                        break;
                default:
                        usage();
                        goto done;
                }
        }

        if (!b_arg ||
            !t_arg) {
                usage();
                goto done;
        }

        conf = (struct configuration) {
                /* */
                .static_www_folder = static_www_folder,             /* */
                .work_path = work_path,                             /* */
                .temp_dir_template = temp_dir_template,             /* */
                .trip_salt = trip_salt,                             /* */
                .trip_salt_len = strlen(trip_salt),                 /* */
                .boards = boards,                                   /* */
                .boards_num = NUM_OF(boards),                       /* */
                .max_form_data_size = max_form_data_size,           /* */
                .max_file_size = max_file_size,                     /* */
                .max_text_len = max_text_len,                       /* */
                .filetypes = filetypes,                             /* */
                .filetypes_num = NUM_OF(filetypes),                 /* */
                .file_description_prog = file_description_prog,     /* */
                .headers = headers,                                 /* */
                .headers_num = NUM_OF(headers),                     /* */
                .challenges = challenges,                           /* */
                .challenges_num = NUM_OF(challenges),               /* */
                .wordfilter_inputs = wordfilter_inputs,             /* */
                .wordfilter_inputs_num = NUM_OF(wordfilter_inputs), /* */
                .forbidden_inputs = forbidden_inputs,               /* */
                .forbidden_inputs_num = NUM_OF(forbidden_inputs),   /* */
        };

        /* Interpret board */
        board_idx = (size_t) -1;

        for (size_t j = 0; j < conf.boards_num; ++j) {
                if (!strcmp(conf.boards[j].name, b_arg)) {
                        board_idx = j;
                }
        }

        if (board_idx == (size_t) -1) {
                ERROR_MESSAGE("No board \"%s\" known", b_arg);
                goto done;
        }

        /* Interpret thread */
        errno = 0;
        thread_id = strtoull(t_arg, 0, 0);

        if (errno) {
                ret = errno;
                PERROR_MESSAGE("strtoull");
                goto done;
        }

        /* Set up a minimal part of the system */
        if (setup_locks(&conf) < 0) {
                goto done;
        }

        if (setup_dbs(&conf) < 0) {
                goto done;
        }

        if (setup_write_thread(&conf) < 0) {
                goto done;
        }

        ret = db_writeback_posts_in_thread(board_idx, thread_id, stdout,
                                           print_post);
done:
        clean_locks();
        clean_dbs();
        clean_write_thread();

        return ret;
}
