/*
 * Configure your site here
 */

/*
 * Where is the directory full of static files that we'll be
 * modifying? (absolute path to a folder, please)
 */
static const char *static_www_folder = "/var/www/rb79";

/*
 * So, we have to have databases and lockfiles and stuff. Where
 * do they go? Probably somewhere far away from network access, and
 * regularly backed-up, I'd guess. (absolute path to a folder,
 * please)
 */
static const char *work_path = "/opt/rb79";

/*
 * When we convert files (stripping EXIF out of jpegs, etc.) they
 * go to a temporary directory, which is created through mkdtemp.
 * What is the template for that directory name? It should end in
 * "XXXXXX", and it should be somewhere that isn't automatically
 * destroyed (e.g. by a cleanup cronjob).
 */
static const char *temp_dir_template = "/tmp/rb79_conv_XXXXXX";

/*
 * The boards: each name should just be a folder inside static_www_folder.
 * You should probably avoid names starting with underscores since
 * I reserve those names to put templates and stuff in.
 */
static const struct board boards[] = {
        /* */
        { .name = "m", .title = "Mecha",             /* */
          .text_cooldown = 40, .blank_cooldown = 20, /* */
          .threads_per_page = 10, .num_pages = 20,   /* */
          .appears_in_recent = 1 },                  /* */
        { .name = "b", .title = "Random",            /* */
          .text_cooldown = 20, .blank_cooldown = 10, /* */
          .threads_per_page = 10, .num_pages = 10,   /* */
          .appears_in_recent = 0 },                  /* */
        { .name = "sf", .title = "General SF",       /* */
          .text_cooldown = 40, .blank_cooldown = 20, /* */
          .threads_per_page = 10, .num_pages = 20,   /* */
          .appears_in_recent = 1 },                  /* */
};

/*
 * What's the tripcode salt?
 */
static const char *trip_salt =
        "The color of television, tuned to a dead channel";

/*
 * Here are some error messages. They will be fed into fprintf, so
 * they are kept as #defines so that the compiler can provide some
 * surety against misuse.
 */

/*
 * Here's the generic HTTP 400 template. For things like using the
 * wrong method, uploading a file with the wrong mime type, etc.
 * This should be a format suitable for feeding to printf with
 * EXACTLY one %s, which is a brief explanation of the problem.
 */
#define BAD_REQUEST_FMT \
        "Status: 400\r\n" \
        "Content-type: text/html\r\n\r\n" \
        "<!DOCTYPE html>" \
        "<html>" \
        "<head>" \
        "<meta http-equiv=\"Content-Type\" content=\"text/html; " \
        "charset=utf-8\" />" \
        "<title>Kiteo. His eyes closed.</title>" \
        "<link rel=\"stylesheet\" type=\"text/css\" " \
        "href=\"/css/futaba.css\" />" \
        "<link rel=\"icon\" type=\"image/png\" href=\"/icon.png\" />" \
        "</head>" \
        "<body>" \
        "<div class=\"center-wrapper\">" \
        "<div class=\"notice-box\">" \
        "<p class=\"notice-title\">Malformed request</p>" \
        "<p>%s</p>" \
        "</div>" \
        "</div>" \
        "</body>" \
        "</html>\n"

/*
 * And here's the ban message template.  This time there should be
 * EXACTLY TWO %s format specifier. The first one gives ban reason,
 * the second gives ban expiry. If you want to reorder them, you
 * can use %2$s and %1$s.
 */
#define BAN_FMT \
        "Status: 403\r\n" \
        "Content-type: text/html\r\n\r\n" \
        "<!DOCTYPE html>" \
        "<html>" \
        "<head>" \
        "<meta http-equiv=\"Content-Type\" content=\"text/html; " \
        "charset=utf-8\" />" \
        "<title>\u515c\u7532\u5150\u3001\u30de\u30b0\u30de\u306b" \
        "\u6b7b\u3059!</title>" \
        "<link rel=\"stylesheet\" type=\"text/css\" " \
        "href=\"/css/futaba.css\" />" \
        "<link rel=\"icon\" type=\"image/png\" href=\"/icon.png\" />" \
        "</head>" \
        "<body>" \
        "<div class=\"center-wrapper\">" \
        "<div class=\"notice-box\">" \
        "<p class=\"notice-title\">You are banned! Σ(ﾟДﾟ )</p>" \
        "<ul><li>Expiry: %s</li>" \
        "<li>Reason: %s</li></ul>" \
        "</div>" \
        "</div>" \
        "</body>" \
        "</html>\n"

/*
 * Here's what they get if they fail a challenge (see below). No %s at all.
 */
#define BAD_CHALLENGE_FMT \
        "Status: 403\r\n" \
        "Content-type: text/html\r\n\r\n" \
        "<!DOCTYPE html>" \
        "<html>" \
        "<head>" \
        "<meta http-equiv=\"Content-Type\" content=\"text/html; " \
        "charset=utf-8\" />" \
        "<title>(It's not “swordfish”)</title>" \
        "<link rel=\"stylesheet\" type=\"text/css\" " \
        "href=\"/css/futaba.css\" />" \
        "<link rel=\"icon\" type=\"image/png\" href=\"/icon.png\" />" \
        "</head>" \
        "<body>" \
        "<div class=\"center-wrapper\">" \
        "<div class=\"notice-box\">" \
        "<p class=\"notice-title\">You gotta answer the challenge!</p>" \
        "<p>It's for your own good, you know.</p>" \
        "</div>" \
        "</div>" \
        "</body>" \
        "</html>\n"

/*
 * Here's what they get if they use the wrong HTTP method on /action.
 * No %s at all.
 */
#define BAD_METHOD_FMT \
        "Status: 405\r\n" \
        "Content-type: text/html\r\n\r\n" \
        "<!DOCTYPE html>" \
        "<html>" \
        "<head>" \
        "<meta http-equiv=\"Content-Type\" content=\"text/html; " \
        "charset=utf-8\" />" \
        "<title>This is highly unorthodox</title>" \
        "<link rel=\"stylesheet\" type=\"text/css\" " \
        "href=\"/css/futaba.css\" />" \
        "<link rel=\"icon\" type=\"image/png\" href=\"/icon.png\" />" \
        "</head>" \
        "<body>" \
        "<div class=\"center-wrapper\">" \
        "<div class=\"notice-box\">" \
        "<p class=\"notice-title\">That wasn't a POST</p>" \
        "<p>We only take POSTs here.</p>" \
        "</div>" \
        "</div>" \
        "</body>" \
        "</html>\n"

/*
 * Upload was large enough for the server to accept it, but overflowed
 * some other bound. This should be a template suitable for feeding
 * into printf, with EXACTLY one %s, which will be something like
 * "file" or "subject" or "comment".
 */
#define TOO_LARGE_FMT \
        "Status: 413\r\n" \
        "Content-type: text/html\r\n\r\n" \
        "<!DOCTYPE html>" \
        "<html>" \
        "<head>" \
        "<meta http-equiv=\"Content-Type\" content=\"text/html; " \
        "charset=utf-8\" />" \
        "<title>Once the Big Zam is uploaded...</title>" \
        "<link rel=\"stylesheet\" type=\"text/css\" " \
        "href=\"/css/futaba.css\" />" \
        "<link rel=\"icon\" type=\"image/png\" href=\"/icon.png\" />" \
        "</head>" \
        "<body>" \
        "<div class=\"center-wrapper\">" \
        "<div class=\"notice-box\">" \
        "<p class=\"notice-title\">Too large!</p>" \
        "<p>%s too large.</p>" \
        "</div>" \
        "</div>" \
        "</body>" \
        "</html>\n"

/*
 * User is posting when they've posted too recently. This should
 * be a template suitable for feeding into printf, with EXACTLY one
 * %s, which will be replaced with something like "25 seconds".
 */
#define COOLDOWN_FMT \
        "Status: 429\r\n" \
        "Content-type: text/html\r\n\r\n" \
        "<!DOCTYPE html>" \
        "<html>" \
        "<head>" \
        "<meta http-equiv=\"Content-Type\" content=\"text/html; " \
        "charset=utf-8\" />" \
        "<title>This sensation, is it Char!?</title>" \
        "<link rel=\"stylesheet\" type=\"text/css\" " \
        "href=\"/css/futaba.css\" />" \
        "<link rel=\"icon\" type=\"image/png\" href=\"/icon.png\" />" \
        "</head>" \
        "<body>" \
        "<div class=\"center-wrapper\">" \
        "<div class=\"notice-box\">" \
        "<p class=\"notice-title\">Slow down!</p>" \
        "<p>You're posting too fast! Try again in %s.</p>" \
        "</div>" \
        "</div>" \
        "</body>" \
        "</html>\n"

/*
 * Some kind of server error occured: out of memory, invalid UTF-8
 * generated, somewhere, etc. Apologize profusely.
 */
#define INTERNAL_ERROR_FMT \
        "Status: 500\r\n" \
        "Content-type: text/html\r\n\r\n" \
        "<!DOCTYPE html>" \
        "<html>" \
        "<head>" \
        "<meta http-equiv=\"Content-Type\" content=\"text/html; " \
        "charset=utf-8\" />" \
        "<title>Bitter failure</title>" \
        "<link rel=\"stylesheet\" type=\"text/css\" " \
        "href=\"/css/futaba.css\" />" \
        "<link rel=\"icon\" type=\"image/png\" href=\"/icon.png\" />" \
        "</head>" \
        "<body>" \
        "<div class=\"center-wrapper\">" \
        "<div class=\"notice-box\">" \
        "<p class=\"notice-title\">Internal error</p>" \
        "<p>Please accept our apologies. " \
        "Something unexpected has happened.</p>" \
        "</div>" \
        "</div>" \
        "</body>" \
        "</html>\n"

/*
 * Here's the "Post successful" screen.  There's one %s, which is
 * the page to redirect back to.
 */
#define POST_SUCCESSFUL_FMT \
        "Status: 200\r\n" \
        "Content-type: text/html\r\n\r\n" \
        "<!DOCTYPE html>" \
        "<html>" \
        "<head>" \
        "<meta http-equiv=\"Content-Type\" content=\"text/html; " \
        "charset=utf-8\" />" \
        "<meta http-equiv=\"refresh\" content=\"3;URL='%s'\" />" \
        "<title>\u9280\u6cb3\u306e\u6b74\u53f2\u304c\u307e\u305f1\u30da" \
        "\u30fc\u30b8</title>" \
        "<link rel=\"stylesheet\" type=\"text/css\" " \
        "href=\"/css/futaba.css\" />" \
        "<link rel=\"icon\" type=\"image/png\" href=\"/icon.png\" />" \
        "</head>" \
        "<body>" \
        "<div class=\"center-wrapper\">" \
        "<div class=\"notice-box\">" \
        "<p class=\"notice-title\">Post successful</p>" \
        "<p>Returning you in 3\u20262\u20261\u2026</p>" \
        "</div>" \
        "</div>" \
        "</body>" \
        "</html>\n"

/*
 * How large of a multipart/form-data (in bytes) should we listen
 * to before throwing 413?  This should be a bit larger than the
 * max file size, but not large enough to cause DoS by malloc().
 *
 * This is separate from any upper limit configured in the web
 * server. The web server's limit should be well above this value.
 */
static const size_t max_form_data_size = (5 * (1 << 20));

/*
 * How large of a file (in bytes) should we accept?
 */
static const size_t max_file_size = (4 * (1 << 20));

/*
 * How long of a comment (in bytes) should we accept?
 */
static const size_t max_text_len = (3 << 10);

/*
 * What mimetypes are allowed? Which ones should be thumbnailed (by
 * what external command?), and which ones should simply have a
 * fixed thumbnail (and where is it on the server?)
 *
 * This is handled by libmagic(3), in case you're wondering about
 * that. We don't trust the Content-Type of the upload (but we do
 * trust libmagic, and we trust libmagic to trust things... ugh).
 *
 * Please note that all format strings should take two arguments
 * (%s). The first is the source, the latter is destination. If
 * they need to be used multiple times, %1$s and %2$s will work.
 */
static const struct filetype filetypes[] = {
        /* */
        { .mime_type = "image/jpeg",                             /* */
          .ext = "jpg",                                          /* */
          .install_command = "jhead -purejpg %1$s >/dev/null"    /* */
                             " 2>/dev/null && cp %1$s %2$s",     /* */
          .thumb_creation_command = "convert %s -thumbnail"      /* */
                                    " 150x150 %s" },             /* */
        { .mime_type = "image/gif",                              /* */
          .ext = "gif",                                          /* */
          .install_command = "mv %s %s",                         /* */
          .thumb_creation_command = "convert %s[0] -thumbnail"   /* */
                                    " 150x150 %s" },             /* */
        { .mime_type = "image/png",                              /* */
          .ext = "png",                                          /* */
          .install_command = "mv %s %s",                         /* */
          .thumb_creation_command = "convert %s -thumbnail"      /* */
                                    " 150x150 %s" },             /* */
        { .mime_type = "video/webm",                             /* */
          .ext = "webm",                                         /* */
          .install_command = "ffmpeg -y -i %s -an -c:v copy %s"  /* */
                             " >/dev/null 2>/dev/null",          /* */
          .thumb_creation_command =
                  "ffmpeg -y -i %s  -c:v mjpeg -ss 0 -vframes 1" /* */
                  " -an -vf scale=w=150:h=150:force_original"    /* */
                  "_aspect_ratio=decrease -f rawvideo %s"        /* */
                  " >/dev/null 2>/dev/null" },                   /* */
        { .mime_type = "application/epub+zip",                   /* */
          .ext = "epub", .install_command = "mv %s %s",          /* */
          .static_thumbnail = "/_icons/book.jpg" },
};

/*
 * Information about a file should be displayed: something like
 * "webm, 201KB, 00:36". Where is the program which prints this
 * information?
 *
 * The script will be passed two arguments. The first is the mimetype
 * of the file, a string like "image/jpeg" or "application/epub+zip".
 * The second will be an absolute path to the file itself.
 *
 * A decent starting point is located at tools/describe-file.sh of
 * the source repository. The Makefile installs this to something
 * like /usr/bin/rb79-describe-file, so that's what the default
 * configuration uses.
 */
static const char *file_description_prog = "/usr/bin/rb79-describe-file";

/* A list of all possible header images, randomly selected from */
static const char *headers[] = {
        /* */
        "/_banners/1.png", /* */
        "/_banners/2.png", /* */
        "/_banners/3.png", /* */
        "/_banners/4.png", /* */
};

/*
 * Spambot traps (you can give up to 5 answers for each question,
 * case insensitive)
 */
static const struct challenge challenges[] = {
        /* */
        { .question = "<img src=\"/_hints/1.png\" alt=\"B__L\" />"  /* */
                      "What is the more common name of the RB-79?", /* */
          .answers = { "ball", 0 } },                               /* */
        { .question = "<img src=\"/_hints/2.png\" alt=\"H___\" />"  /* */
                      " Which spherical toy did Amuro build?",      /* */
          .answers = { "haro", "ball", 0 } },                       /* */
        { .question = "<img src=\"/_hints/3.png\" "                 /* */
                      "alt=\"Is______\"/> Where did the Fifth "     /* */
                      "Battle of Iserlohn happen? ",                /* */
          .answers = { "iserlohn", 0 } },                           /* */
        { .question = "<img src=\"/_hints/4.png\" "                 /* */
                      "alt=\"J______\"/> Which planet is the "      /* */
                      "source of all evil? ",                       /* */
          .answers = { "jupiter", 0 } },                            /* */
        { .question = "<img src=\"/_hints/5.png\" "                 /* */
                      "alt=\"B_s____l\"/> What is Sisko's  "        /* */
                      "favorite sport?",                            /* */
          .answers = { "baseball", 0 } },
};

/*
 * What are the wordfilters? pattern will be compiled by pcre2 with
 * only the UTF compatible option, replace will be what the whole
 * match gets replaced with.
 *
 * If you want case-insensitive, put (?i) at the start. If you want
 * fancy backreferences, send a patch. If you want to replace with
 * unicode, use \u1234 (this happens before HTML-escaping).  If you
 * want to use this to implement [spoiler], please don't.
 */
static const struct wordfilter_input wordfilter_inputs[] = {
        /* */
        { .pattern = "(?i)nina purpleton", .replacement = "worst girl" },
        { .pattern = "(?i)\\bkes\\b", .replacement = "Quess" },
        { .pattern = "(?i)\\bquess\\b", .replacement = "Kes" },
        { .pattern = "(?i).*?\\b(smh|fam|tbh|succ|thicc)\\b.*",
          .replacement = "( \u0361\u00b0 \u035c\u0296 \u0361\u00b0)" },
};

/*
 * What are some phrases that can't be posted? As with wordfilters,
 * pattern will be compiled with pcre2, only with the UTF compatible
 * option. ban_duration is (if non-zero) the amount of time they'll be
 * banned for, with reason ban_reason (secret if empty)
 */
static const struct forbidden_input forbidden_inputs[] = {
        /* */
        { .pattern = "Actually [Ss][Ee][Ee][Dd] Destiny was good",
          .ban_duration = 43200, .ban_reason = "I disagree" },
        { .pattern = "dick pills", .ban_duration = 31536000,
          .ban_reason = 0 },
};
