/*
 * Copyright (c) 2017-2020, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sodium.h>

#include "macros.h"
#include "rb79.h"

#include "config.h"

const char *program_name = "rb79-delete-post";

/* Show usage message, do not exit */
void
usage(void)
{
        size_t len = strlen(program_name);

        printf("Usage: %s -b board-name\n", program_name);
        printf("       %*s -p post-id\n", (int) len, "");
}

/* Do the thing */
int
main(int argc, char **argv)
{
        int ret = EINVAL;
        struct configuration conf = { 0 };
        int opt = 0;
        const char *b_arg = 0;
        size_t board_idx = 0;
        const char *p_arg = 0;
        uintmax_t post_id = 0;
        uint_fast8_t actually_is_op = 0;

        setlocale(LC_ALL, "");

        /* Parse options */
        while ((opt = getopt(argc, argv, "b:p:")) != -1) {
                switch (opt) {
                case 'b':

                        if (b_arg) {
                                ERROR_MESSAGE("-b already specified");
                                goto done;
                        }

                        b_arg = optarg;
                        break;
                case 'p':

                        if (p_arg) {
                                ERROR_MESSAGE("-p already specified");
                                goto done;
                        }

                        p_arg = optarg;
                        break;
                default:
                        usage();
                        goto done;
                }
        }

        if (!b_arg ||
            !p_arg) {
                usage();
                goto done;
        }

        conf = (struct configuration) {
                /* */
                .static_www_folder = static_www_folder,             /* */
                .work_path = work_path,                             /* */
                .temp_dir_template = temp_dir_template,             /* */
                .trip_salt = trip_salt,                             /* */
                .trip_salt_len = strlen(trip_salt),                 /* */
                .boards = boards,                                   /* */
                .boards_num = NUM_OF(boards),                       /* */
                .max_form_data_size = max_form_data_size,           /* */
                .max_file_size = max_file_size,                     /* */
                .max_text_len = max_text_len,                       /* */
                .filetypes = filetypes,                             /* */
                .filetypes_num = NUM_OF(filetypes),                 /* */
                .file_description_prog = file_description_prog,     /* */
                .headers = headers,                                 /* */
                .headers_num = NUM_OF(headers),                     /* */
                .challenges = challenges,                           /* */
                .challenges_num = NUM_OF(challenges),               /* */
                .wordfilter_inputs = wordfilter_inputs,             /* */
                .wordfilter_inputs_num = NUM_OF(wordfilter_inputs), /* */
                .forbidden_inputs = forbidden_inputs,               /* */
                .forbidden_inputs_num = NUM_OF(forbidden_inputs),   /* */
        };

        /* Interpret board */
        board_idx = (size_t) -1;

        for (size_t j = 0; j < conf.boards_num; ++j) {
                if (!strcmp(conf.boards[j].name, b_arg)) {
                        board_idx = j;
                }
        }

        if (board_idx == (size_t) -1) {
                ERROR_MESSAGE("No board \"%s\" known", b_arg);
                goto done;
        }

        /* Interpret post */
        errno = 0;
        post_id = strtoull(p_arg, 0, 0);

        if (errno) {
                ret = errno;
                PERROR_MESSAGE("strtoull");
                goto done;
        }

        /* Set up a minimal part of the system */
        if (setup_locks(&conf) < 0) {
                goto done;
        }

        if (setup_dbs(&conf) < 0) {
                goto done;
        }

        if (setup_write_thread(&conf) < 0) {
                goto done;
        }

        if (db_is_op(board_idx, post_id, &actually_is_op) < 0) {
                goto done;
        }

        if (lock_acquire(board_idx) < 0) {
                goto done;
        }

        if (actually_is_op) {
                if (db_remove_thread_and_files(board_idx, post_id) < 0) {
                        goto done;
                }
        } else {
                if (db_remove_post_and_files(board_idx, post_id) < 0) {
                        goto done;
                }
        }

        lock_release(board_idx);
        util_rebuild(&conf);
        ret = 0;
done:
        clean_locks();
        clean_dbs();
        clean_write_thread();

        return ret;
}
