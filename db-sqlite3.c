/*
 * Copyright (c) 2017-2020, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sqlite3.h>

#include "macros.h"
#include "rb79.h"

#define TRY_BIND_T(s, db, i, v) \
        do { \
                if ((sqlite3_bind_text((s), \
                                       sqlite3_bind_parameter_index((s), (i)), \
                                       (v), -1, SQLITE_STATIC)) \
                    != SQLITE_OK) { \
                        ERROR_MESSAGE("sqlite3_bind_text(): cannot bind " \
                                      "%s := \"%s\": %s", \
                                      (i), (v), \
                                      sqlite3_errmsg(db)); \
                        goto done; \
                } \
        } while (0)

#define TRY_BIND_I(s, db, i, v) \
        do { \
                if ((sret = sqlite3_bind_int64((s), \
                                               sqlite3_bind_parameter_index((s), \
                                                                            (i)), \
                                               (sqlite3_int64) (v))) != \
                    SQLITE_OK) { \
                        ERROR_MESSAGE("sqlite3_bind_int64(): cannot bind " \
                                      "%s := %lld: %s", \
                                      (i), ((long long int) (v)), \
                                      sqlite3_errmsg(db)); \
                        goto done; \
                } \
        } while (0)

#define TRY_MAKE_STATEMENT_ARRAY(name) \
        do { \
                if (!(board_ ## name ## _stmt = \
                              calloc(conf->boards_num, \
                                     sizeof *board_ ## name ## _stmt))) \
                { \
                        PERROR_MESSAGE("calloc"); \
                        goto done; \
                } \
        } while (0)

#define TRY_PREPARE_FOR_BOARD(j, name) \
        do { \
                if (sqlite3_prepare_v2(board_dbs[j], \
                                       board_ ## name ## _txt, -1, \
                                       &board_ ## name ## _stmt[j], \
                                       0) != SQLITE_OK) { \
                        ERROR_MESSAGE("Preparing statement failed: %s", \
                                      sqlite3_errmsg(board_dbs[j])); \
                        goto done; \
                } \
        } while (0)

#define FINALIZE_FOR_BOARD(j, name) \
        do { \
                if (board_ ## name ## _stmt) { \
                        sqlite3_finalize(board_ ## name ## _stmt[j]); \
                        board_ ## name ## _stmt[j] = 0; \
                } \
        } while (0)

#define CLEAN_UP_STATEMENT_ARRAY(name) \
        do { \
                free(board_ ## name ## _stmt); \
                board_ ## name ## _stmt = 0; \
        } while (0);

#define EXFILTRATE_TEXT(s, n, str, len) \
        do { \
                char *tmp = (char *) sqlite3_column_text(s, n); \
                if (tmp) { \
                        str = strdup(tmp); \
                        len = sqlite3_column_bytes(s, n); \
                } \
                else { \
                        str = 0; \
                        len = 0; \
                } \
        } while (0)

/* Preparing DBs */
static const char *make_comment_table =
        "create table if not exists comments                          \n" /* */
        "        (id                integer primary key autoincrement,\n" /* */
        "         thread_closed     integer default 0,                \n" /* */
        "         thread_full       integer default 0,                \n" /* */
        "         thread_stickied   integer default 0,                \n" /* */
        "         thread_last_reply integer default 0,                \n" /* */
        "         thread_bumpable   integer default 1,                \n" /* */
        "         in_thread         integer default 0,                \n" /* */
        "         ip                text    default '0.0.0.0',        \n" /* */
        "         date              integer default 0,                \n" /* */
        "         name              text    default 'Anonymous',      \n" /* */
        "         tripcode          text    default '',               \n" /* */
        "         email             text    default '',               \n" /* */
        "         comment           text    default '',               \n" /* */
        "         subject           text    default '',               \n" /* */
        "         users_filename    text    default '',               \n" /* */
        "         system_full_path  text    default '',               \n" /* */
        "         system_thumb_path text    default '',               \n" /* */
        "         file_info         text    default ''                \n" /* */
        "        );";                                                     /* */
static const char *make_ban_table =
        "create table if not exists bans                       \n"        /* */
        "        (id         integer primary key autoincrement,\n"        /* */
        "         ip_start   text    default '',               \n"        /* */
        "         ip_end     text    default '',               \n"        /* */
        "         date_start integer default 0,                \n"        /* */
        "         date_end   integer default 0,                \n"        /* */
        "         is_secret  integer default 0,                \n"        /* */
        "         reason     text    default 'No reason given' \n"        /* */
        "        );";                                                     /* */
static const char *make_cooldown_table =
        "create table if not exists cooldowns          \n"                /* */
        "        (ip              text    primary key, \n"                /* */
        "         cooldown_expiry integer default 0    \n"                /* */
        "        );";                                                     /* */
/* Are you banned globally? */
static const char *global_check_ban_txt =
        "select date_end, is_secret, reason from bans\n" /* */
        "    where @ip between ip_start and ip_end \n"   /* */
        "          and date_end > @now;            \n";  /* */
static sqlite3_stmt *global_check_ban_stmt;

/* Create a global ban */
static const char *global_insert_ban_txt =
        "insert into bans ( ip_start,             \n"  /* */
        "                   ip_end,               \n"  /* */
        "                   date_start,           \n"  /* */
        "                   date_end,             \n"  /* */
        "                   is_secret,            \n"  /* */
        "                   reason)               \n"  /* */
        "          values (@ip_start,             \n"  /* */
        "                  @ip_end,               \n"  /* */
        "                  @date_start,           \n"  /* */
        "                  @date_end,             \n"  /* */
        "                  @is_secret,            \n"  /* */
        "                  @reason);              \n"; /* */
static sqlite3_stmt *global_insert_ban_stmt;

/* Are you banned? */
static const char *board_check_ban_txt =
        "select date_end, is_secret, reason from bans\n" /* */
        "    where @ip between ip_start and ip_end \n"   /* */
        "          and date_end > @now;            \n";  /* */
static sqlite3_stmt **board_check_ban_stmt;

/* When was your last post? */
static const char *board_check_cooldown_txt =
        "select cooldown_expiry from cooldowns     \n" /* */
        "where ip is @ip;";
static sqlite3_stmt **board_check_cooldown_stmt;

/* That thread, uhh, exists, right? */
static const char *board_check_thread_exists_txt =
        "select thread_closed, thread_full from comments \n" /* */
        "where id is @thread;                            \n";
static sqlite3_stmt **board_check_thread_exists_stmt;

/* How many replies are in this thread? */
static const char *board_count_posts_txt =
        "select count(*) from comments               \n" /* */
        "where coalesce(in_thread, id) is @thread;   \n";
static sqlite3_stmt **board_count_posts_stmt;

/* Delete a thread */
static const char *board_delete_thread_txt =
        "delete from comments where coalesce(in_thread, id) is @thread;";
static sqlite3_stmt **board_delete_thread_stmt;

/* Delete a post */
static const char *board_delete_post_txt =
        "delete from comments where id is @id;";
static sqlite3_stmt **board_delete_post_stmt;

/* Get the thread for a post */
static const char *board_find_containing_thread_txt =
        "select coalesce(in_thread, id) from comments   \n" /* */
        "where id is @id order by id;                   \n";
static sqlite3_stmt **board_find_containing_thread_stmt;

/* Get the subject of a thread */
static const char *board_get_subject_txt =
        "select subject from comments where id is @thread;";
static sqlite3_stmt **board_get_subject_stmt;

/* Get the contents of a thread */
static const char *board_get_thread_contents_txt =
        "select id, date, name, subject, email,   \n"  /* */
        "    tripcode, comment, users_filename,   \n"  /* */
        "    system_full_path, system_thumb_path, \n"  /* */
        "    file_info, ip, thread_closed,        \n"  /* */
        "    thread_stickied from comments        \n"  /* */
        "where coalesce(in_thread, id) is @thread;\n"; /* */
static sqlite3_stmt **board_get_thread_contents_stmt;

/* Get enough of a thread to write a summary on a board page */
static const char *board_get_thread_summary_txt =
        "select id, date, name, subject, email,   \n"  /* */
        "    tripcode, comment, users_filename,   \n"  /* */
        "    system_full_path, system_thumb_path, \n"  /* */
        "    file_info, ip, thread_closed,        \n"  /* */
        "    thread_stickied  from comments where \n"  /* */
        "    coalesce(in_thread, id) is @thread   \n"  /* */
        "    and (id in (                         \n"  /* */
        "         select id from comments where   \n"  /* */
        "             id is @thread or            \n"  /* */
        "             in_thread is @thread        \n"  /* */
        "         order by id desc limit 3        \n"  /* */
        "     ) or id is @thread) order by id asc;\n"; /* */
static sqlite3_stmt **board_get_thread_summary_stmt;

/* Get the contents of a post */
static const char *board_get_post_contents_txt =
        "select id, date, name, subject, email,   \n"  /* */
        "    tripcode, comment, users_filename,   \n"  /* */
        "    system_full_path, system_thumb_path, \n"  /* */
        "    file_info, ip, in_thread,            \n"  /* */
        "    thread_closed, thread_stickied       \n"  /* */
        "    from comments                        \n"  /* */
        "where id is @post;                       \n"; /* */
static sqlite3_stmt **board_get_post_contents_stmt;

/* Create a global ban */
static const char *board_insert_ban_txt =
        "insert into bans ( ip_start,             \n"  /* */
        "                   ip_end,               \n"  /* */
        "                   date_start,           \n"  /* */
        "                   date_end,             \n"  /* */
        "                   is_secret,            \n"  /* */
        "                   reason)               \n"  /* */
        "          values (@ip_start,             \n"  /* */
        "                  @ip_end,               \n"  /* */
        "                  @date_start,           \n"  /* */
        "                  @date_end,             \n"  /* */
        "                  @is_secret,            \n"  /* */
        "                  @reason);              \n"; /* */
static sqlite3_stmt **board_insert_ban_stmt;

/* Make a post/thread/whatever */
static const char *board_insert_comment_txt =
        "insert into comments ( ip,               \n"  /* */
        "                       date,             \n"  /* */
        "                       thread_last_reply,\n"  /* */
        "                       in_thread,        \n"  /* */
        "                       name,             \n"  /* */
        "                       tripcode,         \n"  /* */
        "                       email,            \n"  /* */
        "                       subject,          \n"  /* */
        "                       comment,          \n"  /* */
        "                       users_filename,   \n"  /* */
        "                       system_full_path, \n"  /* */
        "                       system_thumb_path \n"  /* */
        "                       )                 \n"  /* */
        "            values   (@ip,               \n"  /* */
        "                      @date,             \n"  /* */
        "                      @date,             \n"  /* */
        "                      @in_thread,        \n"  /* */
        "                      @name,             \n"  /* */
        "                      @tripcode,         \n"  /* */
        "                      @email,            \n"  /* */
        "                      @subject,          \n"  /* */
        "                      @comment,          \n"  /* */
        "                      @users_filename,   \n"  /* */
        "                      @system_full_path, \n"  /* */
        "                      @system_thumb_path \n"  /* */
        "                      );                 \n"; /* */
static sqlite3_stmt **board_insert_comment_stmt;

/* Insert comment part II: adjust the thread */
static const char *board_insert_comment_II_txt =
        "update comments set                      \n"  /* */
        "    thread_last_reply =                  \n"  /* */
        "    (case when @should_bump is 1 and     \n"  /* */
        "               thread_bumpable is 1 then \n"  /* */
        "         @date                           \n"  /* */
        "     else                                \n"  /* */
        "         thread_last_reply               \n"  /* */
        "     end),                               \n"  /* */
        "                                         \n"  /* */
        "     thread_bumpable =                   \n"  /* */
        "     (case when (select count(*)         \n"  /* */
        "       from comments                     \n"  /* */
        "       where in_thread is @in_thread)    \n"  /* */
        "       >= 300 then                       \n"  /* */
        "         0                               \n"  /* */
        "     else                                \n"  /* */
        "         thread_bumpable                 \n"  /* */
        "     end),                               \n"  /* */
        "                                         \n"  /* */
        "     thread_full =                       \n"  /* */
        "     (case when (select count(*)         \n"  /* */
        "       from comments                     \n"  /* */
        "       where in_thread is @in_thread)    \n"  /* */
        "       >= 500 then                       \n"  /* */
        "         0                               \n"  /* */
        "     else                                \n"  /* */
        "         thread_full                     \n"  /* */
        "     end)                                \n"  /* */
        "    where id is @in_thread;              \n"; /* */
static sqlite3_stmt **board_insert_comment_II_stmt;

/* Find all threads on this board */
static const char *board_list_threads_txt =
        "select id from comments where in_thread is NULL\n"  /* */
        "order by thread_stickied desc,                 \n"  /* */
        "         thread_last_reply desc;               \n"; /* */
static sqlite3_stmt **board_list_threads_stmt;

/* Cooldown */
static const char *board_set_cooldown_txt =
        "insert or replace into cooldowns (ip,              \n"  /* */
        "                                  cooldown_expiry) \n"  /* */
        "values (@ip, @cooldown_expiry);                      "; /* */
static sqlite3_stmt **board_set_cooldown_stmt;

/* Change the sorts of things that moderation needs */
static const char *board_update_by_moderation_txt =
        "update comments set                       \n"  /* */
        "      comment         = @comment,         \n"  /* */
        "      thread_stickied = @thread_stickied, \n"  /* */
        "      thread_closed   = @thread_closed    \n"  /* */
        " where id is @id;                         \n"; /* */
static sqlite3_stmt **board_update_by_moderation_stmt;

/* Update the file_info field for a post (after creation) */
static const char *board_update_file_info_txt =
        "update comments set                           \n"  /* */
        "       file_info         = @file_info,        \n"  /* */
        "       system_full_path  = @system_full_path, \n"  /* */
        "       system_thumb_path = @system_thumb_path \n"  /* */
        " where id is @id;                             \n"; /* */
static sqlite3_stmt **board_update_file_info_stmt;

/* Get the last few posts on this board for /recent/ purposes */
static const char *board_get_recent_posts_txt =
        "select id, date, name, subject, email,       \n"  /* */
        "       tripcode, comment, users_filename,    \n"  /* */
        "       system_full_path, system_thumb_path,  \n"  /* */
        "       file_info, ip, in_thread              \n"  /* */
        "from comments order by date desc limit 10;   \n"; /* */
static sqlite3_stmt **board_get_recent_posts_stmt;

/* Our connections */
static sqlite3 **board_dbs = 0;
static size_t num_connected_db = 0;
static sqlite3 *global_db = 0;

/* Global configuration */
static const struct configuration *conf;

/* Clean the internals of a prepared_post */
static void
clean_prepared_post(struct prepared_post *p)
{
        free(p->name);
        free(p->subject);
        free(p->email);
        free(p->tripcode);
        free(p->comment);
        free(p->file_name);
        free(p->system_full_path);
        free(p->system_thumb_path);
        free(p->file_info);
        free(p->ip);
        *p = (struct prepared_post) { 0 };
}

/*
 * Make sure we can connect to the DBs and that they're in working order
 *
 * Preconditions:
 *
 *  - setup_dbs() was not invoked more recently than clean_dbs().
 *
 * Postconditions (success):
 *
 *  - Any other function in this file may be safely called.
 */
int
setup_dbs(const struct configuration *in_conf)
{
        int ret = -1;
        int sret = 0;
        size_t len = 0;
        char *path = 0;
        char *error_message = 0;

        conf = in_conf;

        /* Memory for all our board-specific things */
        if (!(board_dbs = calloc(conf->boards_num, sizeof *board_dbs))) {
                PERROR_MESSAGE("calloc");
                goto done;
        }

        TRY_MAKE_STATEMENT_ARRAY(check_ban);
        TRY_MAKE_STATEMENT_ARRAY(check_cooldown);
        TRY_MAKE_STATEMENT_ARRAY(check_thread_exists);
        TRY_MAKE_STATEMENT_ARRAY(count_posts);
        TRY_MAKE_STATEMENT_ARRAY(delete_thread);
        TRY_MAKE_STATEMENT_ARRAY(delete_post);
        TRY_MAKE_STATEMENT_ARRAY(find_containing_thread);
        TRY_MAKE_STATEMENT_ARRAY(get_subject);
        TRY_MAKE_STATEMENT_ARRAY(get_thread_contents);
        TRY_MAKE_STATEMENT_ARRAY(get_thread_summary);
        TRY_MAKE_STATEMENT_ARRAY(get_post_contents);
        TRY_MAKE_STATEMENT_ARRAY(insert_ban);
        TRY_MAKE_STATEMENT_ARRAY(insert_comment);
        TRY_MAKE_STATEMENT_ARRAY(insert_comment_II);
        TRY_MAKE_STATEMENT_ARRAY(list_threads);
        TRY_MAKE_STATEMENT_ARRAY(set_cooldown);
        TRY_MAKE_STATEMENT_ARRAY(update_by_moderation);
        TRY_MAKE_STATEMENT_ARRAY(update_file_info);
        TRY_MAKE_STATEMENT_ARRAY(get_recent_posts);

        /* Turn on global connection */
        len = snprintf(0, 0, "%s/global.db", conf->work_path);

        if (len + 1 < len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (!(path = malloc(len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        sprintf(path, "%s/global.db", conf->work_path);

        if ((sret = sqlite3_open(path, &global_db)) != SQLITE_OK) {
                ERROR_MESSAGE("Cannot open or create database %s: %s", path,
                              sqlite3_errstr(sret));
                goto done;
        }

        /* Set up global table (only bans) */
        if (sqlite3_exec(global_db, make_ban_table, 0, 0, &error_message) !=
            SQLITE_OK) {
                ERROR_MESSAGE("Cannot set up ban table in database %s: %s",
                              path, error_message);
                goto done;
        }

        /* Global statments (only ban creation/checking) */
        if (sqlite3_prepare_v2(global_db, global_check_ban_txt, -1,
                               &global_check_ban_stmt, 0) != SQLITE_OK) {
                ERROR_MESSAGE("Preparing statement failed: %s", sqlite3_errmsg(
                                      global_db));
                goto done;
        }

        if (sqlite3_prepare_v2(global_db, global_insert_ban_txt, -1,
                               &global_insert_ban_stmt, 0) != SQLITE_OK) {
                ERROR_MESSAGE("Preparing statement failed: %s", sqlite3_errmsg(
                                      global_db));
                goto done;
        }

        /* Board specific stuff */
        for (size_t j = 0; j < conf->boards_num; ++j) {
                free(path);
                path = 0;
                len = snprintf(0, 0, "%s/board_%s.db", conf->work_path,
                               conf->boards[j].name);

                if (len + 1 < len) {
                        ERROR_MESSAGE("overflow");
                        goto done;
                }

                if (!(path = malloc(len + 1))) {
                        PERROR_MESSAGE("malloc");
                        goto done;
                }

                sprintf(path, "%s/board_%s.db", conf->work_path,
                        conf->boards[j].name);

                /* Turn on board */
                if ((sret = sqlite3_open(path, &board_dbs[j])) != SQLITE_OK) {
                        ERROR_MESSAGE("Cannot open or create database %s: %s",
                                      path, sqlite3_errstr(sret));
                        goto done;
                }

                num_connected_db++;

                /* Set up tables */
                if (sqlite3_exec(board_dbs[j], make_comment_table, 0, 0,
                                 &error_message) != SQLITE_OK) {
                        ERROR_MESSAGE(
                                "Cannot set up comment table in database %s: %s",
                                path,
                                error_message);
                        goto done;
                }

                if (sqlite3_exec(board_dbs[j], make_ban_table, 0, 0,
                                 &error_message) != SQLITE_OK) {
                        ERROR_MESSAGE(
                                "Cannot set up ban table in database %s: %s",
                                path,
                                error_message);
                        goto done;
                }

                if (sqlite3_exec(board_dbs[j], make_cooldown_table, 0, 0,
                                 &error_message) != SQLITE_OK) {
                        ERROR_MESSAGE(
                                "Cannot set up cooldown table in database %s: %s",
                                path,
                                error_message);
                        goto done;
                }

                free(path);
                path = 0;

                /* Set up statements */
                TRY_PREPARE_FOR_BOARD(j, check_ban);
                TRY_PREPARE_FOR_BOARD(j, check_cooldown);
                TRY_PREPARE_FOR_BOARD(j, check_thread_exists);
                TRY_PREPARE_FOR_BOARD(j, count_posts);
                TRY_PREPARE_FOR_BOARD(j, delete_thread);
                TRY_PREPARE_FOR_BOARD(j, delete_post);
                TRY_PREPARE_FOR_BOARD(j, find_containing_thread);
                TRY_PREPARE_FOR_BOARD(j, get_subject);
                TRY_PREPARE_FOR_BOARD(j, get_thread_contents);
                TRY_PREPARE_FOR_BOARD(j, get_thread_summary);
                TRY_PREPARE_FOR_BOARD(j, get_post_contents);
                TRY_PREPARE_FOR_BOARD(j, insert_ban);
                TRY_PREPARE_FOR_BOARD(j, insert_comment);
                TRY_PREPARE_FOR_BOARD(j, insert_comment_II);
                TRY_PREPARE_FOR_BOARD(j, list_threads);
                TRY_PREPARE_FOR_BOARD(j, set_cooldown);
                TRY_PREPARE_FOR_BOARD(j, update_by_moderation);
                TRY_PREPARE_FOR_BOARD(j, update_file_info);
                TRY_PREPARE_FOR_BOARD(j, get_recent_posts);
        }

        ret = 0;
done:

        if (error_message) {
                sqlite3_free(error_message);
        }

        free(path);

        return ret;
}

/*
 * Construct something suitable for use in <a>
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - board is a sequence of ASCII characters of length board_len
 *    that represents a board.
 *
 *  - Board directories are located at "/", so that "/".board."/res/"
 *    is where thread pages live.
 *
 *  - post is a sequence of ASCII digits of length post_len.
 *
 *  - out, out_len, and found are not 0.
 *
 *  - Overwriting *out shall not cause a memory leak.
 *
 * Postconditions:
 *
 *  - If the post doesn't exist, *found = 0.
 *
 *  - Otherwise, *found is 1, and *out is a string like "/a/res/1235"
 *    of length *out_len, which can be used in a <a> element.
 */
int
db_construct_post_link(const char *board, size_t board_len, const char *post,
                       size_t post_len, int *found, char **out, size_t *out_len)
{
        int ret = -1;
        int sret = 0;
        size_t board_idx = (size_t) -1;
        size_t in_thread = 0;
        uintmax_t post_num = 0;
        sqlite3_stmt *s = 0;
        sqlite3 *db = 0;
        size_t len = 0;
        char *tmp = 0;

        if (board_len > INT_MAX / 2) {
                ERROR_MESSAGE("The board name \"%.*s...\" is way too long", 10,
                              board);
                goto done;
        }

        /*
         * We can't call strtoll(post, 0, 0) because board might
         * not be 0-terminated, it may point into internal PCRE2
         * memory for example. It's simpler to recreate base 10
         * strtoll than to malloc/copy/free a temp buffer.
         */
        for (size_t j = 0; j < post_len; ++j) {
                post_num = 10 * post_num + (post[j] - '0');
        }

        for (size_t j = 0; j < num_connected_db; ++j) {
                const struct board *b = &conf->boards[j];

                if (strlen(b->name) == board_len &&
                    !strcmp(board, b->name)) {
                        board_idx = j;
                        break;
                }
        }

        if (board_idx == (size_t) -1) {
                ERROR_MESSAGE("Board \"%.*s\" doesn't exist", (int) board_len,
                              board);
                goto done;
        }

        s = board_find_containing_thread_stmt[board_idx];
        db = board_dbs[board_idx];
        TRY_BIND_I(s, db, "@id", post_num);
        sret = sqlite3_step(s);

        switch (sret) {
        case SQLITE_DONE:
                *found = 0;
                ret = 0;
                goto done;
        case SQLITE_ROW:
                in_thread = sqlite3_column_int64(s, 0);
                break;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
                break;
        }

        len = snprintf(0, 0, "/%.*s/res/%zu#post%ju", (int) board_len, board,
                       in_thread, post_num);

        if (len + 1 < len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (!(tmp = malloc(len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        sprintf(tmp, "/%.*s/res/%zu#post%ju", (int) board_len, board, in_thread,
                post_num);
        *out = tmp;
        *out_len = len;
        *found = 1;
        ret = 0;
done:
        sqlite3_reset(s);
        sqlite3_clear_bindings(s);

        return ret;
}

/*
 * Ensure that there are not more than the proper number of threads
 * lying around; report how many pages we need.
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - board_idx represents a board, AND THE LOCK IS HELD.
 *
 *  - out_thread_ids, out_thread_id_num, and out_num_pages are not 0.
 *
 *  - Overwriting *out_thread_ids shall not cause a memory leak.
 *
 * Postconditions (success):
 *
 *  - There are num_pages * threads_per_page threads (rows with
 *    in_thread = 0) in the board's database.
 *
 *  - If rows had to be deleted, all relevant reply rows were also
 *    deleted.
 *
 *  - If rows had to be deleted, all files related to those rows
 *    (the thread page, the stored files for replies, etc.) have
 *    been deleted.
 */
int
db_cull_and_report_threads(size_t board_idx, uintmax_t **out_thread_ids,
                           size_t *out_thread_ids_num, size_t *out_num_pages)
{
        uintmax_t *to_delete = 0;
        size_t to_delete_num = 0;
        size_t to_delete_sz = 0;
        uintmax_t total_threads_seen = 0;
        uintmax_t threads_to_keep = 0;
        int ret = -1;
        int sret = 0;
        sqlite3_stmt *s = board_list_threads_stmt[board_idx];
        sqlite3 *db = board_dbs[board_idx];
        uint_fast8_t exhausted = 0;
        void *newmem = 0;
        uintmax_t *thread_ids = 0;
        const struct board *b = &conf->boards[board_idx];

        threads_to_keep = b->num_pages * b->threads_per_page;

        if (!(thread_ids = calloc(threads_to_keep, sizeof *thread_ids))) {
                PERROR_MESSAGE("calloc");
                goto done;
        }

        if (!(to_delete = malloc(sizeof *to_delete))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        to_delete_sz = 1;
        to_delete[0] = 0;

        while (!exhausted) {
                sret = sqlite3_step(s);

                switch (sret) {
                case SQLITE_DONE:
                        exhausted = 1;
                        break;
                case SQLITE_ROW:
                        total_threads_seen++;

                        if (total_threads_seen > threads_to_keep) {
                                to_delete[to_delete_num] = sqlite3_column_int64(
                                        s, 0);

                                if (to_delete_num + 1 >= to_delete_sz) {
                                        if (to_delete_sz + 16 < to_delete_sz ||
                                            ((to_delete_sz + 16) *
                                             sizeof *to_delete) /
                                            (to_delete_sz +
                                             16) !=
                                            sizeof *to_delete) {
                                                ERROR_MESSAGE("overflow "
                                                              "(to_delete_sz = %zu)",
                                                              to_delete_sz);
                                                goto done;
                                        }

                                        if (!(newmem = realloc(to_delete,
                                                               (to_delete_sz +
                                                                16) *
                                                               sizeof *to_delete)))
                                        {
                                                PERROR_MESSAGE("relloc");
                                                goto done;
                                        }

                                        to_delete = newmem;
                                        to_delete_sz += 16;
                                }

                                to_delete_num++;
                        } else {
                                thread_ids[total_threads_seen - 1] =
                                        sqlite3_column_int64(s, 0);
                        }

                        break;
                default:
                        ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                        goto done;
                }
        }

        for (size_t j = 0; j < to_delete_num; ++j) {
                db_remove_thread_and_files(board_idx, to_delete[j]);
        }

        *out_thread_ids = thread_ids;
        *out_thread_ids_num = (total_threads_seen > threads_to_keep) ?
                              threads_to_keep : total_threads_seen;
        *out_num_pages = 0;

        if (*out_thread_ids_num) {
                *out_num_pages = 1 + ((*out_thread_ids_num - 1) /
                                      b->threads_per_page);
        }

        ret = 0;
done:
        free(to_delete);
        to_delete = 0;
        sqlite3_reset(s);
        sqlite3_clear_bindings(s);

        return ret;
}

/*
 * Check whether a specific type of ban is active.
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - s is one of global_check_ban_stmt or a board_check_ban_stmt[j].
 *
 *  - db corresponds to s.
 *
 *  - ip is a string like "127.0.0.1"
 *
 *  - out_is_banned, out_ban_until, out_is_secret, out_ban_reason are not 0.
 *
 *  - Overwriting *out_ban_until and *out_ban_reason shall not cause
 *    a memory leak.
 *
 * Postconditions (success):
 *
 *  - *out_is_banned represents whether s returned a row for ip and
 *    row.
 *
 *  - If *out_banned != 0, then *out_ban_until and *out_ban_reason
 *    are informative text strings (*out_ban_until is something
 *    like "2020-01-01T12:34:56" and *out_ban_reason is something
 *    like "having wrong opinions"). They are not 0.
 */
static int
check_ban_h(sqlite3_stmt *s, sqlite3 * db, const char *ip, time_t now,
            int *out_is_banned, char **out_ban_until, int *out_is_secret,
            char **out_ban_reason)
{
        int ret = -1;
        int sret = 0;
        size_t dummy_len = 0;

        UNUSED(dummy_len);
        TRY_BIND_T(s, db, "@ip", ip);
        TRY_BIND_I(s, db, "@now", now);
        sret = sqlite3_step(s);

        switch (sret) {
        case SQLITE_DONE:
                /* No global ban */
                break;
        case SQLITE_ROW:
                *out_is_banned = 1;
                *out_is_secret = !!sqlite3_column_int64(s, 1);
                EXFILTRATE_TEXT(s, 2, *out_ban_reason, dummy_len);

                if (!(*out_ban_until = util_iso8601_from_time_t(
                              (time_t) sqlite3_column_int64(s, 0)))) {
                        goto done;
                }

                ret = 0;
                goto done;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        ret = 0;
done:
        sqlite3_reset(s);
        sqlite3_clear_bindings(s);

        return ret;
}

/*
 * Check whether any ban is active.
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - ip is a string like "127.0.0.1".
 *
 *  - out_is_banned, out_ban_until, out_ban_reason are not 0.
 *
 *  - Overwriting *out_ban_until and *out_ban_reason shall not cause
 *    a memory leak.
 *
 *  - board_idx corresponds to a board.
 *
 * Postconditions (success):
 *
 *  - *out_is_banned represents whether a row was found in the bans
 *    db, either globally or for board_idx, matching ip and now.
 *
 *  - If *out_banned != 0, then *out_ban_until and *out_ban_reason
 *    are informative text strings (*out_ban_until is something
 *    like "2020-01-01T12:34:56" and *out_ban_reason is something
 *    like "having wrong opinions"). They are not 0.
 */
int
db_check_bans(const char *ip, size_t board_idx, time_t now, int *out_is_banned,
              char **out_ban_until, int *out_is_secret, char **out_ban_reason)
{
        int ret = -1;

        /* First check global bans */
        if (check_ban_h(global_check_ban_stmt, global_db, ip, now,
                        out_is_banned, out_ban_until, out_is_secret,
                        out_ban_reason) < 0) {
                goto done;
        }

        if (*out_is_banned) {
                ret = 0;
                goto done;
        }

        /* Now board-specific */
        if (check_ban_h(board_check_ban_stmt[board_idx], board_dbs[board_idx],
                        ip, now, out_is_banned, out_ban_until, out_is_secret,
                        out_ban_reason) <
            0) {
                goto done;
        }

        ret = 0;
done:

        return ret;
}

/*
 * Check whether a cooldown is active.
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - ip is a string like "127.0.0.1".
 *
 *  - out_is_cooled, out_cooldown_length are not 0.
 *
 *  - Overwriting *out_cooldown_length shall not cause a memory
 *    leak.
 *
 *  - board_idx corresponds to a board.
 *
 * Postconditions (success):
 *
 *  - *out_is_cooled represents whether a row was found in the
 *    cooldowns table corresponding to board_idx.
 *
 *  - If *out_is_cooled != 0, then *out_cooldown_length is a string
 *    like "20 seconds", corresponding to the cooldown row.
 */
int
db_check_cooldowns(const char *ip, size_t board_idx, time_t now,
                   int *out_is_cooled, char **out_cooldown_length)
{
        int ret = -1;
        int sret = 0;
        time_t expiry = 0;
        long diff = 0;
        size_t len = 0;
        sqlite3_stmt *s = board_check_cooldown_stmt[board_idx];
        sqlite3 *db = board_dbs[board_idx];

        TRY_BIND_T(s, db, "@ip", ip);
        sret = sqlite3_step(s);

        switch (sret) {
        case SQLITE_DONE:
                *out_is_cooled = 0;
                break;
        case SQLITE_ROW:
                expiry = (time_t) sqlite3_column_int64(s, 0);
                diff = (expiry > now) ? expiry - now : -1;

                if (diff > 0) {
                        *out_is_cooled = 1;
                        len = snprintf(0, 0, "%ld seconds", diff);

                        if (len + 1 < len) {
                                ERROR_MESSAGE("overflow");
                                goto done;
                        }

                        if (!(*out_cooldown_length = malloc(len + 1))) {
                                PERROR_MESSAGE("malloc");
                                goto done;
                        }

                        sprintf(*out_cooldown_length, "%ld seconds", diff);
                }

                ret = 0;
                goto done;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        ret = 0;
done:
        sqlite3_reset(s);
        sqlite3_clear_bindings(s);

        return ret;
}

/*
 * Check whether a thread exists, is full, is closed.
 *
 * Preconditions:
 *
 *  - board_idx corresponds to a board.
 *
 *  - thread_dne, thread_closed, thread_full are not 0.
 */
static int
check_thread(uintmax_t id, size_t board_idx, int *thread_dne,
             int *thread_closed, int *thread_full)
{
        int ret = -1;
        int sret = 0;
        sqlite3 *db = board_dbs[board_idx];
        sqlite3_stmt *s = board_check_thread_exists_stmt[board_idx];

        TRY_BIND_I(s, db, "@thread", id);
        sret = sqlite3_step(s);

        switch (sret) {
        case SQLITE_DONE:
                *thread_dne = 1;
                ret = 0;
                goto done;
        case SQLITE_ROW:
                *thread_closed = sqlite3_column_int(s, 0);
                *thread_full = sqlite3_column_int(s, 1);
                ret = 0;
                goto done;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        ret = 0;
done:
        sqlite3_reset(s);
        sqlite3_clear_bindings(s);

        return ret;
}

/* Get the subject of a thread.
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - board_idx represents a board.
 *
 *  - thread is the id of a thread.
 *
 *  - out_subject and out_subject_len are not 0.
 *
 *  - overwriting *out_subject shall not cause a memory leak.
 *
 * Postconditions (success):
 *
 *  - *out_subject is a string of length *out_subject_len, which
 *    is the subject of the thread given by thread.
 *
 *  - The memory of *out_subject should be freed by the caller.
 */
int
db_extract_subject(size_t board_idx, uintmax_t thread, char **out_subject,
                   size_t *out_subject_len)
{
        int ret = -1;
        int sret = 0;
        sqlite3 *db = board_dbs[board_idx];
        sqlite3_stmt *s = board_get_subject_stmt[board_idx];

        TRY_BIND_I(s, db, "@thread", thread);
        sret = sqlite3_step(s);

        switch (sret) {
        case SQLITE_DONE:
                break;
        case SQLITE_ROW:
                EXFILTRATE_TEXT(s, 0, *out_subject, *out_subject_len);
                break;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        ret = 0;
done:
        sqlite3_reset(s);
        sqlite3_clear_bindings(s);

        return ret;
}

/*
 * Insert a ban, which may be either global or board-specific.
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - Either global_ban is non-zero, or board_idx represents a board.
 *
 *  - first_ip and last_ip are "normalized" ip addresses, in the
 *    sense of the output of util_normalize_ip(), not e.g. RFC 2373.
 *
 *  - message is a string.
 *
 * Postconditions (success):
 *
 *  - Depending on global_ban and board_idx, a row in the bans table
 *    of an appropriate database has been created, depending on the
 *    input parameters in an obvious way.
 */
int
db_insert_ban(uint_fast8_t global_ban, size_t board_idx, const char *first_ip,
              const char *last_ip, const char *message, uint_fast8_t is_secret,
              time_t
              ban_start, time_t ban_expiry)
{
        int ret = -1;
        int sret = 0;
        sqlite3_stmt *s = 0;
        sqlite3 *db = 0;

        if (global_ban) {
                s = global_insert_ban_stmt;
                db = global_db;
        } else {
                s = board_insert_ban_stmt[board_idx];
                db = board_dbs[board_idx];
        }

        TRY_BIND_T(s, db, "@ip_start", first_ip);
        TRY_BIND_T(s, db, "@ip_end", last_ip);
        TRY_BIND_I(s, db, "@date_start", ban_start);
        TRY_BIND_I(s, db, "@date_end", ban_expiry);
        TRY_BIND_I(s, db, "@is_secret", is_secret);
        TRY_BIND_T(s, db, "@reason", message);
        sret = sqlite3_step(s);

        switch (sret) {
        case SQLITE_DONE:
                break;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        ret = 0;
done:
        sqlite3_reset(s);
        sqlite3_clear_bindings(s);

        return ret;
}

/*
 * Insert a post, which may be a reply or a new thread. Some fields
 * (file_info) are not filled out - they are updated later, after
 * filesystem work has been completed.
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - ip is a string like "127.0.0.1".
 *
 *  - f is not 0 if pc contains a file.
 *
 *  - The prepared_XYZ fields of pc are filled out.
 *
 *  - If this post is a reply, in_thread is the id of the thread's
 *    OP.
 *
 *  - thread_dne, thread_closed, thread_full, post_id are not 0.
 *
 * Postconditions (success):
 *
 *  - If the post couldn't be made because the thread doesn't exist,
 *    *thread_dne is 1.
 *
 *  - Otherwise, if the post couldn't be made because the thread
 *    is closed, *thread_closed = 1.
 *
 *  - Otherwise, if the post couldn't be made because the thread
 *    is full, *thread_full = 1.
 *
 *  - Otherwise, the post was made, and the surrounding thread's
 *    reply date, fullness, etc. have been updated (no actual HTML
 *    regeneration, though).
 *
 *  - Furthermore, *post_id is the number of the inserted post.
 */
int
db_insert_post(const char *ip, size_t in_thread, int cooldown, struct
               post_cmd *pc, int *thread_dne, int *thread_closed,
               int *thread_full,
               uintmax_t *post_id)
{
        int ret = -1;
        int sret = 0;
        sqlite3_stmt *s = board_insert_comment_stmt[pc->board_idx];
        sqlite3_stmt *s2 = board_insert_comment_II_stmt[pc->board_idx];
        sqlite3_stmt *s3 = board_set_cooldown_stmt[pc->board_idx];
        sqlite3 *db = board_dbs[pc->board_idx];

        TRY_BIND_T(s, db, "@ip", ip);
        TRY_BIND_I(s, db, "@date", pc->prepared.now);

        if (in_thread) {
                if (check_thread(in_thread, pc->board_idx, thread_dne,
                                 thread_closed, thread_full) < 0) {
                        goto done;
                }

                if (*thread_dne ||
                    *thread_closed ||
                    *thread_full) {
                        ret = 0;
                        goto done;
                }

                TRY_BIND_I(s, db, "@in_thread", in_thread);
                TRY_BIND_I(s2, db, "@in_thread", in_thread);
                TRY_BIND_I(s2, db, "@date", pc->prepared.now);
                TRY_BIND_I(s2, db, "@should_bump", (!pc->prepared.email ||
                                                    strcmp(pc->prepared.email,
                                                           "sage")));
        }

        TRY_BIND_T(s, db, "@name", pc->prepared.name);
        TRY_BIND_T(s, db, "@tripcode", pc->prepared.tripcode);
        TRY_BIND_T(s, db, "@email", pc->prepared.email);
        TRY_BIND_T(s, db, "@subject", pc->prepared.subject);
        TRY_BIND_T(s, db, "@comment", pc->prepared.comment);
        TRY_BIND_T(s, db, "@users_filename", pc->prepared.file_name);

        /*
         * It's highly probable that these are blank. At the current
         * time of writing, db_insert_post() is called before
         * install_files(), and the resulting row is fixed up
         * afterwards in db_update_file_info(). These are currently
         * left in for the hack-ish hooks for writing posts
         * programatically.
         */
        TRY_BIND_T(s, db, "@system_full_path", pc->prepared.system_full_path);
        TRY_BIND_T(s, db, "@system_thumb_path", pc->prepared.system_thumb_path);
        sret = sqlite3_step(s);

        switch (sret) {
        case SQLITE_DONE:
                break;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        *post_id = sqlite3_last_insert_rowid(db);

        if (in_thread) {
                sret = sqlite3_step(s2);

                switch (sret) {
                case SQLITE_DONE:
                case SQLITE_ROW:
                        break;
                default:
                        ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                        goto done;
                }
        }

        TRY_BIND_T(s3, db, "@ip", ip);
        TRY_BIND_I(s3, db, "@cooldown_expiry", pc->prepared.now + cooldown);
        sret = sqlite3_step(s3);

        switch (sret) {
        case SQLITE_DONE:
        case SQLITE_ROW:
                break;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        ret = 0;
done:
        sqlite3_reset(s);
        sqlite3_clear_bindings(s);
        sqlite3_reset(s2);
        sqlite3_clear_bindings(s2);
        sqlite3_reset(s3);
        sqlite3_clear_bindings(s3);

        return ret;
}

/*
 * Check if a post is actually the OP of a thread.
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - board_idx represents a board.
 *
 *  - post_id represents a post.
 *
 *  - out_is_op is not 0.
 *
 * Postconditions (success):
 *
 *  - *out_is_op is either 1 (if the row with id = post_id has
 *    in_thread NULL), or 0 (otherwise).
 */
int
db_is_op(size_t board_idx, uintmax_t post_id, uint_fast8_t *out_is_op)
{
        int ret = -1;
        int sret = 0;
        sqlite3_stmt *s = board_get_post_contents_stmt[board_idx];
        sqlite3 *db = board_dbs[board_idx];

        TRY_BIND_I(s, db, "@post", post_id);
        sret = sqlite3_step(s);

        switch (sret) {
        case SQLITE_DONE:
                *out_is_op = 0;
                break;
        case SQLITE_ROW:
                *out_is_op = !sqlite3_column_int64(s, 12);
                break;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        ret = 0;
done:
        sqlite3_reset(s);
        sqlite3_clear_bindings(s);

        return ret;
}

/*
 * Perform minor adjustments to a post
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - board_idx represents a board.
 *
 *  - post_id represents a post.
 *
 *  - moderator_comment is either 0 or a string.
 *
 *  - If change_sticky or change_close are not 0, then post_id
 *    represents the OP of a thread.
 *
 * Postconditions (success):
 *
 *  - If change_sticky, then the thread_stickied will be adjusted
 *    to sticky_status.
 *
 *  - If change_close, then the thread_closed will be adjusted to
 *    close_status.
 */
int
db_moderate_post(size_t board_idx, uintmax_t post_id, const
                 char *moderator_comment, uint_fast8_t change_sticky,
                 uint_fast8_t sticky_status,
                 uint_fast8_t change_close, uint_fast8_t close_status)
{
        int ret = -1;
        int sret = 0;
        sqlite3_stmt *s = board_get_post_contents_stmt[board_idx];
        sqlite3_stmt *s2 = board_update_by_moderation_stmt[board_idx];
        sqlite3 *db = board_dbs[board_idx];
        uint_fast8_t thread_stickied = 0;
        uint_fast8_t thread_closed = 0;
        char *comment = 0;
        size_t comment_len = 0;
        char *new_comment = 0;
        size_t new_comment_len = 0;

        TRY_BIND_I(s, db, "@post", post_id);
        sret = sqlite3_step(s);

        switch (sret) {
        case SQLITE_DONE:
                LOG("Board /%s/, post %ju does not exist",
                    conf->boards[board_idx].name, post_id);
                goto done;
        case SQLITE_ROW:
                EXFILTRATE_TEXT(s, 6, comment, comment_len);
                thread_closed = !!sqlite3_column_int64(s, 13);
                thread_stickied = !!sqlite3_column_int64(s, 14);
                break;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        if (!moderator_comment) {
                TRY_BIND_T(s2, db, "@comment", comment);
        } else if (comment_len) {
                new_comment_len = snprintf(0, 0, "%s<br /><br />"
                                                 "<span class=\"mod-text\">"
                                                 "(%s)</span>", comment,
                                           moderator_comment);

                if (new_comment_len + 1 < new_comment_len) {
                        ERROR_MESSAGE("overflow");
                        goto done;
                }

                if (!(new_comment = malloc(new_comment_len + 1))) {
                        PERROR_MESSAGE("malloc");
                        goto done;
                }

                sprintf(new_comment, "%s<br /><br />"
                                     "<span class=\"mod-text\">(%s)</span>",
                        comment,
                        moderator_comment);
                TRY_BIND_T(s2, db, "@comment", new_comment);
        } else {
                new_comment_len = snprintf(0, 0, "<span class=\"mod-text\">"
                                                 "(%s)</span>",
                                           moderator_comment);

                if (new_comment_len + 1 < new_comment_len) {
                        ERROR_MESSAGE("overflow");
                        goto done;
                }

                if (!(new_comment = malloc(new_comment_len + 1))) {
                        PERROR_MESSAGE("malloc");
                        goto done;
                }

                sprintf(new_comment, "<span class=\"mod-text\">(%s)</span>",
                        moderator_comment);
                TRY_BIND_T(s2, db, "@comment", new_comment);
        }

        if (change_sticky) {
                TRY_BIND_I(s2, db, "@thread_stickied", sticky_status);
        } else {
                TRY_BIND_I(s2, db, "@thread_stickied", thread_stickied);
        }

        if (change_close) {
                TRY_BIND_I(s2, db, "@thread_closed", close_status);
        } else {
                TRY_BIND_I(s2, db, "@thread_closed", thread_closed);
        }

        TRY_BIND_I(s2, db, "@id", post_id);
        sret = sqlite3_step(s2);

        switch (sret) {
        case SQLITE_DONE:
                break;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        ret = 0;
done:
        free(comment);
        free(new_comment);
        sqlite3_reset(s);
        sqlite3_clear_bindings(s);

        return ret;
}

/*
 * Delete all files related to a post, remove row from the
 * database.
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - board_idx represents a board, AND THE LOCK IS HELD.
 *
 *  - post_id represents a post (a row with in_thread != NULL).
 *
 * Postconditions (success):
 *
 *  - wt_remove_files() has been called on the relevant paths.
 *
 *  - The row for which id is thread_id has been removed
 *    from the database.
 */
int
db_remove_post_and_files(size_t board_idx, uintmax_t post_id)
{
        int ret = -1;
        int sret = 0;
        sqlite3_stmt *s = board_get_post_contents_stmt[board_idx];
        sqlite3_stmt *s2 = board_delete_post_stmt[board_idx];
        sqlite3 *db = board_dbs[board_idx];
        char *system_full_path = 0;
        size_t system_full_path_len = 0;
        char *system_thumb_path = 0;
        size_t system_thumb_path_len = 0;

        TRY_BIND_I(s, db, "@post", post_id);
        sret = sqlite3_step(s);

        switch (sret) {
        case SQLITE_DONE:
                LOG("Board /%s/, post %ju does not exist",
                    conf->boards[board_idx].name, post_id);
                goto done;
        case SQLITE_ROW:
                EXFILTRATE_TEXT(s, 8, system_full_path, system_full_path_len);
                EXFILTRATE_TEXT(s, 9, system_thumb_path, system_thumb_path_len);
                wt_remove_files(system_full_path, system_full_path_len,
                                system_thumb_path, system_thumb_path_len);
                break;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        TRY_BIND_I(s2, db, "@id", post_id);
        sret = sqlite3_step(s2);

        switch (sret) {
        case SQLITE_DONE:
                break;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        ret = 0;
done:
        free(system_full_path);
        free(system_thumb_path);
        sqlite3_reset(s);
        sqlite3_reset(s2);
        sqlite3_clear_bindings(s);
        sqlite3_clear_bindings(s2);

        return ret;
}

/*
 * Delete all files related to a thread, remove rows from the
 * database.
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - board_idx represents a board, AND THE LOCK IS HELD.
 *
 *  - thread_id represents a thread (a row with in_thread = NULL).
 *
 * Postconditions (success):
 *
 *  - For every post in the thread, wt_remove_files() has been
 *    called on the relevant paths.
 *
 *  - wt_remove_thread_page() has been called on the relevant thread.
 *
 *  - Any row for which in_thread is thread_id has been removed
 *    from the database.
 */
int
db_remove_thread_and_files(size_t board_idx, uintmax_t thread_id)
{
        int ret = -1;
        int sret = 0;
        sqlite3_stmt *s = board_get_thread_contents_stmt[board_idx];
        sqlite3_stmt *s2 = board_delete_thread_stmt[board_idx];
        sqlite3 *db = board_dbs[board_idx];
        char *system_full_path = 0;
        size_t system_full_path_len = 0;
        char *system_thumb_path = 0;
        size_t system_thumb_path_len = 0;
        char first_try = 1;

        TRY_BIND_I(s, db, "@thread", thread_id);
again:
        sret = sqlite3_step(s);

        switch (sret) {
        case SQLITE_DONE:

                if (first_try) {
                        LOG("Board /%s/, post %ju does not exist",
                            conf->boards[board_idx].name, thread_id);
                        goto done;
                }

                goto nowthread;
        case SQLITE_ROW:
                EXFILTRATE_TEXT(s, 8, system_full_path, system_full_path_len);
                EXFILTRATE_TEXT(s, 9, system_thumb_path, system_thumb_path_len);
                wt_remove_files(system_full_path, system_full_path_len,
                                system_thumb_path, system_thumb_path_len);

                /* Clean up */
                free(system_full_path);
                free(system_thumb_path);
                system_full_path = 0;
                system_thumb_path = 0;
                first_try = 0;
                goto again;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

nowthread:

        if (wt_remove_thread_page(board_idx, thread_id) < 0) {
                goto done;
        }

        TRY_BIND_I(s2, db, "@thread", thread_id);
        sret = sqlite3_step(s2);

        switch (sret) {
        case SQLITE_DONE:
                break;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        ret = 0;
done:
        sqlite3_reset(s);
        sqlite3_reset(s2);
        sqlite3_clear_bindings(s);
        sqlite3_clear_bindings(s2);

        return ret;
}

/*
 * Update the file_info field for a comment (as it isn't known at insert time)
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - board_idx represents a board.
 *
 *  - post_id is the id of a row that exists in that board's comments.
 *
 *  - info is a string of length info_len.
 *
 * Postconditions (success):
 *
 *  - `select file_info from comments where id is @post_id', on the
 *    correct board, would return info.
 */
int
db_update_file_info(size_t board_idx, uintmax_t post_id, const char *info,
                    size_t info_len, const char *system_full_path, size_t
                    system_full_path_len,
                    const char *system_thumb_path, size_t system_thumb_path_len)
{
        int ret = -1;
        int sret = 0;
        sqlite3 *db = board_dbs[board_idx];
        sqlite3_stmt *s = board_update_file_info_stmt[board_idx];

        /* XXX: use this in TRY_BIND_T */
        UNUSED(info_len);
        UNUSED(system_full_path_len);
        UNUSED(system_thumb_path_len);
        TRY_BIND_I(s, db, "@id", post_id);
        TRY_BIND_T(s, db, "@file_info", info);
        TRY_BIND_T(s, db, "@system_full_path", system_full_path);
        TRY_BIND_T(s, db, "@system_thumb_path", system_thumb_path);
        sret = sqlite3_step(s);

        switch (sret) {
        case SQLITE_DONE:
                ret = 0;
                goto done;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        ret = 0;
done:
        sqlite3_reset(s);
        sqlite3_clear_bindings(s);

        return ret;
}

/*
 * The db side of write_thread(): pull all posts from a thread, and
 * call pw_function as appropriate (the abstraction is for the sake
 * of rb79-view-thread).
 *
 * *pw_function had better be one of wt_write_post(),
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - board_idx represents a board.
 *
 *  - thread is the id of a thread.
 *
 *  - f is an open filehandle that can be written to.
 *
 *  - *pw_function is something like wt_write_post().
 *
 * Postconditions (success):
 *
 *  - The thread has, somehow, been written out to f. In practice,
 *    this means wt_write_post() has been called on the rows that
 *    correspond to thread.
 */
int
db_writeback_posts_in_thread(size_t board_idx, uintmax_t thread, FILE *f,
                             post_writeback pw_function)
{
        int ret = -1;
        int sret = 0;
        uint_fast8_t first_post = 1;
        struct prepared_post p = { 0 };
        sqlite3_stmt *s = board_get_thread_contents_stmt[board_idx];
        sqlite3 *db = board_dbs[board_idx];

        TRY_BIND_I(s, db, "@thread", thread);
again:
        sret = sqlite3_step(s);

        switch (sret) {
        case SQLITE_DONE:
                ret = 0;
                goto done;
                break;
        case SQLITE_ROW:
                p = (struct prepared_post) { 0 };
                p.id = sqlite3_column_int64(s, 0);
                p.now = sqlite3_column_int64(s, 1);
                EXFILTRATE_TEXT(s, 2, p.name, p.name_len);
                EXFILTRATE_TEXT(s, 3, p.subject, p.subject_len);
                EXFILTRATE_TEXT(s, 4, p.email, p.email_len);
                EXFILTRATE_TEXT(s, 5, p.tripcode, p.tripcode_len);
                EXFILTRATE_TEXT(s, 6, p.comment, p.comment_len);
                EXFILTRATE_TEXT(s, 7, p.file_name, p.file_name_len);
                EXFILTRATE_TEXT(s, 8, p.system_full_path,
                                p.system_full_path_len);
                EXFILTRATE_TEXT(s, 9, p.system_thumb_path,
                                p.system_thumb_path_len);
                EXFILTRATE_TEXT(s, 10, p.file_info, p.file_info_len);
                EXFILTRATE_TEXT(s, 11, p.ip, p.ip_len);
                p.thread_closed = !!sqlite3_column_int64(s, 12);
                p.thread_stickied = !!sqlite3_column_int64(s, 13);

                if ((*pw_function)(&p, f, first_post, 0, 0, 0, 0, 0, 0) < 0) {
                        goto done;
                }

                first_post = 0;
                clean_prepared_post(&p);
                goto again;
        default:
                ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                goto done;
        }

        ret = 0;
done:
        sqlite3_reset(s);
        sqlite3_clear_bindings(s);

        return ret;
}

/*
 * The db side of write_recent_page(): pull recent posts from all
 * boards, sort, and call pw_function as appropriate.
 *
 * *pw_function had better be one of wt_write_recent_post(),
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - f is an open filehandle that can be written to.
 *
 *  - *pw_function is something like wt_write_recent_post().
 *
 * Postconditions (success):
 *
 *  - The few most recent posts of the board have, somehow, been
 *    written out to f. In practice, this means wt_write_recent_post()
 *    has been called on the rows that correspond to thread.
 */
int
db_writeback_recent_posts(FILE *f, post_writeback pw_function)
{
        int ret = -1;
        int sret = 0;
        struct prepared_post p[10];
        size_t on_board[10];
        uintmax_t in_thread[10];
        time_t now = 0;
        sqlite3_stmt *s = 0;
        sqlite3 *db = 0;

        for (size_t k = 0; k < 10; ++k) {
                p[k] = (struct prepared_post) { 0 };
                on_board[k] = (size_t) -1;
        }

        for (size_t j = 0; j < conf->boards_num; ++j) {
                if (!conf->boards[j].appears_in_recent) {
                        continue;
                }

                s = board_get_recent_posts_stmt[j];
                db = board_dbs[j];
again:
                sret = sqlite3_step(s);

                switch (sret) {
                case SQLITE_DONE:
                        break;
                case SQLITE_ROW:
                        now = sqlite3_column_int64(s, 1);

                        for (size_t k = 0; k < 10; ++k) {
                                if (p[k].now >= now) {
                                        continue;
                                }

                                clean_prepared_post(&p[9]);

                                for (size_t l = 9; l >= k + 1; --l) {
                                        memcpy(&p[l], &p[l - 1], sizeof p[0]);
                                        on_board[l] = on_board[l - 1];
                                        in_thread[l] = in_thread[l - 1];
                                }

                                on_board[k] = j;
                                p[k].id = sqlite3_column_int64(s, 0);
                                p[k].now = sqlite3_column_int64(s, 1);
                                EXFILTRATE_TEXT(s, 2, p[k].name, p[k].name_len);
                                EXFILTRATE_TEXT(s, 3, p[k].subject,
                                                p[k].subject_len);
                                EXFILTRATE_TEXT(s, 4, p[k].email,
                                                p[k].email_len);
                                EXFILTRATE_TEXT(s, 5, p[k].tripcode,
                                                p[k].tripcode_len);
                                EXFILTRATE_TEXT(s, 6, p[k].comment,
                                                p[k].comment_len);
                                EXFILTRATE_TEXT(s, 7, p[k].file_name,
                                                p[k].file_name_len);
                                EXFILTRATE_TEXT(s, 8, p[k].system_full_path,
                                                p[k].system_full_path_len);
                                EXFILTRATE_TEXT(s, 9, p[k].system_thumb_path,
                                                p[k].system_thumb_path_len);
                                EXFILTRATE_TEXT(s, 10, p[k].file_info,
                                                p[k].file_info_len);
                                EXFILTRATE_TEXT(s, 11, p[k].ip, p[k].ip_len);
                                in_thread[k] = sqlite3_column_int64(s, 12);

                                if (!in_thread[k]) {
                                        in_thread[k] = p[k].id;
                                }

                                break;
                        }

                        goto again;
                default:
                        ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                        goto done;
                }

                sqlite3_reset(s);
        }

        for (size_t k = 0; k < 10; ++k) {
                const struct board *b = 0;

                if (on_board[k] >= conf->boards_num) {
                        continue;
                }

                b = &conf->boards[on_board[k]];

                if ((*pw_function)(&p[k], f, 0, 0, 1, b->name, in_thread[k], 0,
                                   !!k) < 0) {
                        goto done;
                }
        }

done:

        for (size_t k = 0; k < 10; ++k) {
                clean_prepared_post(&p[k]);
        }

        return ret;
}

/*
 * The db side of write_board(): pull enough posts from a thread
 * to create a summary for the board page.
 *
 * Preconditions:
 *
 *  - setup_dbs() has been invoked more recently than clean_dbs().
 *
 *  - board_idx represents a board.
 *
 *  - thread_ids is an array of size (at least) thread_ids_num.
 *
 *  - each element of thread_ids represents a currently-active
 *    thread, and the list is sorted by bump order (most recent
 *    first).
 *
 *  - f is an open filehandle that can be written to.
 *
 * Postconditions (success):
 *
 *  - Each of the thread_ids_num threads represented in thread_ids
 *    has, somehow, been written out to f. In practice, this means
 *    write_op_in_board() and write_post_in_board() have been called
 *    on the rows that correspond to the OP and the last few posts
 *    of a thread.
 */
int
db_writeback_thread_summaries(size_t board_idx, uintmax_t *thread_ids, size_t
                              thread_ids_num, FILE *f)
{
        int ret = -1;
        int sret = 0;
        uint_fast8_t first_post = 1;
        sqlite3_stmt *s = board_count_posts_stmt[board_idx];
        sqlite3_stmt *s2 = board_get_thread_summary_stmt[board_idx];
        sqlite3 *db = board_dbs[board_idx];
        struct prepared_post p = { 0 };

        for (size_t j = 0; j < thread_ids_num; ++j) {
                uintmax_t total_post_num = 0;
                uintmax_t thread_id = thread_ids[j];

                first_post = 1;
                sqlite3_reset(s);
                sqlite3_clear_bindings(s);
                TRY_BIND_I(s, db, "@thread", thread_id);
                sret = sqlite3_step(s);

                switch (sret) {
                case SQLITE_DONE:
                        break;
                case SQLITE_ROW:
                        total_post_num = sqlite3_column_int64(s, 0);
                        break;
                default:
                        ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                        goto done;
                }

                sqlite3_reset(s2);
                sqlite3_clear_bindings(s2);
                TRY_BIND_I(s2, db, "@thread", thread_id);
again:
                sret = sqlite3_step(s2);

                switch (sret) {
                case SQLITE_DONE:
                        break;
                case SQLITE_ROW:
                        p = (struct prepared_post) { 0 };
                        p.id = sqlite3_column_int64(s2, 0);
                        p.now = sqlite3_column_int64(s2, 1);
                        EXFILTRATE_TEXT(s2, 2, p.name, p.name_len);
                        EXFILTRATE_TEXT(s2, 3, p.subject, p.subject_len);
                        EXFILTRATE_TEXT(s2, 4, p.email, p.email_len);
                        EXFILTRATE_TEXT(s2, 5, p.tripcode, p.tripcode_len);
                        EXFILTRATE_TEXT(s2, 6, p.comment, p.comment_len);
                        EXFILTRATE_TEXT(s2, 7, p.file_name, p.file_name_len);
                        EXFILTRATE_TEXT(s2, 8, p.system_full_path,
                                        p.system_full_path_len);
                        EXFILTRATE_TEXT(s2, 9, p.system_thumb_path,
                                        p.system_thumb_path_len);
                        EXFILTRATE_TEXT(s2, 10, p.file_info, p.file_info_len);
                        EXFILTRATE_TEXT(s2, 11, p.ip, p.ip_len);
                        p.thread_closed = !!sqlite3_column_int64(s2, 12);
                        p.thread_stickied = !!sqlite3_column_int64(s2, 13);
                        wt_write_post(&p, f, first_post, 1, 0,
                                      conf->boards[board_idx].name, 0,
                                      total_post_num,
                                      first_post);
                        first_post = 0;
                        clean_prepared_post(&p);
                        goto again;
                default:
                        ERROR_MESSAGE("sqlite3_step(): %s", sqlite3_errmsg(db));
                        break;
                }
        }

        ret = 0;
done:
        sqlite3_reset(s);
        sqlite3_clear_bindings(s);

        return ret;
}

/*
 * Clean up any memory from this file
 *
 * Postconditions (success):
 *
 *  - Valgrind won't report any memory leaks from this file.
 *
 *  - setup_dbs() can be safely called again.
 */
int
clean_dbs(void)
{
        /* Close board connections */
        for (size_t j = 0; j < num_connected_db; ++j) {
                FINALIZE_FOR_BOARD(j, check_ban);
                FINALIZE_FOR_BOARD(j, check_cooldown);
                FINALIZE_FOR_BOARD(j, check_thread_exists);
                FINALIZE_FOR_BOARD(j, count_posts);
                FINALIZE_FOR_BOARD(j, delete_thread);
                FINALIZE_FOR_BOARD(j, delete_post);
                FINALIZE_FOR_BOARD(j, find_containing_thread);
                FINALIZE_FOR_BOARD(j, get_subject);
                FINALIZE_FOR_BOARD(j, get_thread_contents);
                FINALIZE_FOR_BOARD(j, get_thread_summary);
                FINALIZE_FOR_BOARD(j, get_post_contents);
                FINALIZE_FOR_BOARD(j, insert_ban);
                FINALIZE_FOR_BOARD(j, insert_comment);
                FINALIZE_FOR_BOARD(j, insert_comment_II);
                FINALIZE_FOR_BOARD(j, list_threads);
                FINALIZE_FOR_BOARD(j, set_cooldown);
                FINALIZE_FOR_BOARD(j, update_by_moderation);
                FINALIZE_FOR_BOARD(j, update_file_info);
                FINALIZE_FOR_BOARD(j, get_recent_posts);

                if (board_dbs) {
                        sqlite3_close(board_dbs[j]);
                }

                board_dbs[j] = 0;
        }

        free(board_dbs);
        board_dbs = 0;
        conf = 0;

        /* Clean up prepared board statements */
        CLEAN_UP_STATEMENT_ARRAY(check_ban);
        CLEAN_UP_STATEMENT_ARRAY(check_cooldown);
        CLEAN_UP_STATEMENT_ARRAY(check_thread_exists);
        CLEAN_UP_STATEMENT_ARRAY(count_posts);
        CLEAN_UP_STATEMENT_ARRAY(delete_thread);
        CLEAN_UP_STATEMENT_ARRAY(delete_post);
        CLEAN_UP_STATEMENT_ARRAY(find_containing_thread);
        CLEAN_UP_STATEMENT_ARRAY(get_subject);
        CLEAN_UP_STATEMENT_ARRAY(get_thread_contents);
        CLEAN_UP_STATEMENT_ARRAY(get_thread_summary);
        CLEAN_UP_STATEMENT_ARRAY(get_post_contents);
        CLEAN_UP_STATEMENT_ARRAY(insert_ban);
        CLEAN_UP_STATEMENT_ARRAY(insert_comment);
        CLEAN_UP_STATEMENT_ARRAY(insert_comment_II);
        CLEAN_UP_STATEMENT_ARRAY(list_threads);
        CLEAN_UP_STATEMENT_ARRAY(set_cooldown);
        CLEAN_UP_STATEMENT_ARRAY(update_by_moderation);
        CLEAN_UP_STATEMENT_ARRAY(update_file_info);
        CLEAN_UP_STATEMENT_ARRAY(get_recent_posts);

        /* Close global connection */
        sqlite3_finalize(global_check_ban_stmt);
        global_check_ban_stmt = 0;
        sqlite3_finalize(global_insert_ban_stmt);
        global_insert_ban_stmt = 0;

        if (global_db) {
                sqlite3_close(global_db);
                global_db = 0;
        }

        return 0;
}
