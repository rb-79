/*
 * Copyright (c) 2017-2018, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/* Defines what's needed to identifiy a board['s folder] */
struct board {
        /* Short name, like "a" or "b" */
        const char *name;

        /* Like "Anime" or "Random" */
        const char *title;

        /* Cooldown after you post text */
        int text_cooldown;

        /* Cooldown after you post textless */
        int blank_cooldown;

        /* How many threads are on each page */
        unsigned int threads_per_page;

        /* How many pages total */
        unsigned int num_pages;

        /* Whether posts here show up in /recent */
        unsigned int appears_in_recent;
};

/* Things users can try to do */
enum action_t { NONE, REPLY, NEWTHREAD, REBUILD };

/*
 * A post as it will be stored in (or retrieved from) the database.
 * As part of post_cmd, this will be built up by rb79.c, inserted
 * into the db by db_insert_post, and retrieved for writing to
 * filesystem in write-thread.c
 */
struct prepared_post {
        /* When it was issued */
        time_t now;

        /*
         * This requires db access to retrieve, so it may be 0 for
         * most of the object's life
         */
        uintmax_t id;

        /* Flags, in case this is the OP of a thread */
        uint_fast8_t thread_closed;
        uint_fast8_t thread_stickied;

        /* HTML-esc'd, ready for inserting into pages */
        char *name;
        size_t name_len;
        char *email;
        size_t email_len;
        char *tripcode;
        size_t tripcode_len;
        char *subject;
        size_t subject_len;
        char *comment;
        size_t comment_len;

        /* Comes from a struct filetype. A copy, needs to be freed */
        char *ext;
        size_t ext_len;

        /* The name the uploader called the file (HTML-esc'd) */
        char *file_name;
        size_t file_name_len;

        /* The path for the full file, like /m/src/123.jpg */
        char *system_full_path;
        size_t system_full_path_len;

        /* The path for the thumbnail, like /m/src/123s.jpg */
        char *system_thumb_path;
        size_t system_thumb_path_len;

        /* A string like "image/jpeg, 30KiB, 500x399" */
        char *file_info;
        size_t file_info_len;

        /* The main server ignores this, but mod tools use it */
        char *ip;
        size_t ip_len;
};
struct post_cmd {
        /*
         * Strings are filled in by multipart.c, where we are quite
         * thankful that everything is guaranteed to be UTF-8.
         * Parsing, if needed, is done by rb79.c
         */
        struct {
                /* 0-terminated and normalized */
                char *ip;

                /* action= */
                char *action;
                size_t action_len;

                /* board= */
                char *board;
                size_t board_len;

                /* thread= */
                char *thread;
                size_t thread_len;

                /* post= */
                char *post;
                size_t post_len;

                /* name= */
                char *name;
                size_t name_len;

                /* email= */
                char *email;
                size_t email_len;

                /* tripcode - calculated from email */
                char *tripcode;
                size_t tripcode_len;

                /* subject= */
                char *subject;
                size_t subject_len;

                /* comment= */
                char *comment;
                size_t comment_len;

                /* file upload */
                char *file_name;
                size_t file_name_len;
                char *file_contents;
                size_t file_contents_len;

                /* challengeid= */
                char *challenge_id;
                size_t challenge_id_len;

                /* challengeresponse= */
                char *challenge_response;
                size_t challenge_response_len;
        } raw;

        /*
         * Everything below is filled in by rb79.c as derived data
         */
        enum action_t action_id;
        uintmax_t board_idx;
        uintmax_t thread_id;

        /* This is what goes into the database. */
        struct prepared_post prepared;

        /*
         * The following is intermediate data for sanitizing input
         * comments. XXX: figure out if these can be safely removed
         * from this object, because they're sort of messy.
         */

        /* A sort of normalized UTF-8 comment */
        char *scannable_comment;
        size_t scannable_comment_len;
        size_t *comment_position_map;
        size_t comment_position_map_len;
        char *scannable_name;
        size_t scannable_name_len;
        size_t *name_position_map;
        size_t name_position_map_len;
        char *scannable_email;
        size_t scannable_email_len;
        size_t *email_position_map;
        size_t email_position_map_len;
        char *scannable_subject;
        size_t scannable_subject_len;
        size_t *subject_position_map;
        size_t subject_position_map_len;
        char *scannable_filename;
        size_t scannable_filename_len;
        size_t *filename_position_map;
        size_t filename_position_map_len;
};

/* Filetype/thumbnailing options */
struct filetype {
        /* */
        const char *mime_type;
        const char *ext;
        const char *install_command;
        const char *thumb_creation_command;
        const char *static_thumbnail;

};

#define NUM_CHALLENGE_ANSWERS 5

/* wakarimasen-style CAPTCHA */
struct challenge {
        /* */
        const char *question;
        const char *answers[NUM_CHALLENGE_ANSWERS];
};

/* regex-backed wordfilter */
struct wordfilter_input {
        /* */
        const char *pattern;
        const char *replacement;
};

/* regex-backed forbidden input */
struct forbidden_input {
        /* */
        const char *pattern;
        int ban_duration;
        const char *ban_reason;
};

/* See config.def.h for detailed descriptions. */
struct configuration {
        /* */
        const char *static_www_folder;
        const char *work_path;
        const char *temp_dir_template;
        const char *trip_salt;
        size_t trip_salt_len;
        const struct board *boards;
        size_t boards_num;
        size_t max_form_data_size;
        size_t max_file_size;
        size_t max_text_len;
        const struct filetype *filetypes;
        size_t filetypes_num;
        const char *file_description_prog;
        const char **headers;
        size_t headers_num;
        const struct challenge *challenges;
        size_t challenges_num;
        const struct wordfilter_input *wordfilter_inputs;
        size_t wordfilter_inputs_num;
        const struct forbidden_input *forbidden_inputs;
        size_t forbidden_inputs_num;
};

/* db_writeback_ZZZ takes a callback. */
typedef int (*post_writeback)(struct prepared_post *p, FILE *f, uint_fast8_t
                              is_op, uint_fast8_t is_summary, uint_fast8_t
                              is_recent, const char *board_name,
                              uintmax_t in_thread, uintmax_t
                              total_posts_in_thread, uint_fast8_t hr_before);

/* db-ZZZ.c (currently only sqlite3, but it used to be MySQL) */
int setup_dbs(const struct configuration *conf);

int db_construct_post_link(const char *board, size_t board_len, const
                           char *post, size_t post_len, int *found, char **out,
                           size_t *out_len);

int db_cull_threads(size_t board_idx, size_t *out_num_pages);

int db_check_bans(const char *ip, size_t board_idx, time_t now,
                  int *out_is_banned, char **out_ban_until, int *out_is_secret,
                  char **out_ban_reason);

int db_check_cooldowns(const char *ip, size_t board_idx, time_t now,
                       int *out_is_cooled, char **out_cooldown_length);

int db_cull_and_report_threads(size_t board_idx, uintmax_t **out_thread_ids,
                               size_t *out_thread_id_num,
                               size_t *out_num_pages);

int db_extract_subject(size_t board_idx, uintmax_t thread, char **out_subject,
                       size_t *out_subject_len);

int db_insert_ban(uint_fast8_t global_ban, size_t board_idx, const
                  char *first_ip, const char *last_ip, const char *message,
                  uint_fast8_t
                  is_secret, time_t ban_start, time_t ban_expiry);

int db_insert_post(const char *ip, size_t in_thread, int cooldown, struct
                   post_cmd *pc, int *thread_dne, int *thread_closed,
                   int *thread_full,
                   uintmax_t *post_id);

int db_is_op(size_t board_idx, uintmax_t post_id, uint_fast8_t *out_is_op);

int db_moderate_post(size_t board_idx, uintmax_t post_id, const
                     char *moderator_comment, uint_fast8_t change_sticky,
                     uint_fast8_t sticky_status,
                     uint_fast8_t change_close, uint_fast8_t close_status);

int db_remove_thread_and_files(size_t board_idx, uintmax_t thread_id);

int db_remove_post_and_files(size_t board_idx, uintmax_t thread_id);

int db_update_file_info(size_t board_idx, uintmax_t post_id, const char *info,
                        size_t info_len, const char *system_full_path, size_t
                        system_full_path_len,
                        const char *system_thumb_path, size_t
                        system_thumb_path_len);

int db_writeback_posts_in_thread(size_t board_idx, uintmax_t thread, FILE *f,
                                 post_writeback pw_function);

int db_writeback_recent_posts(FILE *f, post_writeback pw_function);

int db_writeback_thread_summaries(size_t board_idx, uintmax_t *thread_ids,
                                  size_t thread_ids_num, FILE *f);

int clean_dbs(void);

/* locks.c */
int setup_locks(const struct configuration *conf);

int lock_acquire(size_t board_idx);

int lock_acquire_recent(void);

int lock_release(size_t board_idx);

int lock_release_recent(void);

int clean_locks(void);

/* multipart.c */
int setup_multipart(void);

int multipart_decompose(const char *full_data, size_t full_data_len, struct
                        post_cmd *post_cmd);

int clean_multipart(void);

/* preconditions.c */
int preconditions_check(const struct configuration *conf);

/* sanitize-comment.c */
int setup_sanitize_comment(const struct configuration *conf);

int st_sanitize_text(struct post_cmd *pc, int *our_fault,
                     uint_fast8_t *is_forbidden, int *ban_duration, const
                     char **ban_reason);

int clean_sanitize_comment(void);

/* sanitize-file.c */
int setup_sanitize_file(const struct configuration *conf);

int sf_check_mime_type(const char *buf, size_t len, const struct
                       filetype **out_filetype);

int sf_describe_file(const char *mimetype, const char *filepath,
                     char **out_description, size_t *out_len);

int sf_install_files(size_t board_idx, const char *buf, size_t len, time_t *now,
                     const struct filetype *filetype, char **out_abs_path,
                     char **out_path,
                     size_t *out_path_len, char **out_thumb_path,
                     size_t *out_thumb_path_len,
                     int *our_fault);

int clean_sanitize_file(void);

/* tripcodes.c */
int setup_tripcodes(const struct configuration *conf);

int tripcodes_calculate(struct post_cmd *p);

int clean_tripcodes(void);

/* util.c */
char * util_iso8601_from_time_t(time_t t);

int util_normalize_ip(const char *in, char **out);

int util_rebuild(struct configuration *conf);

/* write-thread.c */
int setup_write_thread(const struct configuration *conf);

int wt_remove_files(const char *system_full_path, size_t system_full_path_len,
                    const char *system_thumb_path, size_t
                    system_thumb_path_len);

int wt_remove_thread_page(size_t board_idx, uintmax_t thread_id);

int wt_write_board(size_t board_idx, uintmax_t *thread_ids, size_t
                   thread_ids_num, size_t board_pages_num);

int wt_write_post(struct prepared_post *p, FILE *f, uint_fast8_t is_op,
                  uint_fast8_t is_summary, uint_fast8_t is_recent, const
                  char *board_name,
                  uintmax_t in_thread, uintmax_t total_posts_in_thread,
                  uint_fast8_t hr_before);

int wt_write_recent_page(void);

int wt_write_thread(size_t board_idx, uintmax_t thread);

void clean_write_thread(void);
