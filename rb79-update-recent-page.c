/*
 * Copyright (c) 2017-2020, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "macros.h"
#include "rb79.h"

#include "config.h"

const char *program_name = "rb79-update-recent-page";

/* Show usage message, do not exit */
void
usage(void)
{
        printf("Usage: %s\n", program_name);
}

/* Do the thing */
int
main(void)
{
        int ret = EINVAL;
        struct configuration conf = { 0 };

        setlocale(LC_ALL, "");
        srand(time(0));
        conf = (struct configuration) {
                /* */
                .static_www_folder = static_www_folder,             /* */
                .work_path = work_path,                             /* */
                .temp_dir_template = temp_dir_template,             /* */
                .trip_salt = trip_salt,                             /* */
                .trip_salt_len = strlen(trip_salt),                 /* */
                .boards = boards,                                   /* */
                .boards_num = NUM_OF(boards),                       /* */
                .max_form_data_size = max_form_data_size,           /* */
                .max_file_size = max_file_size,                     /* */
                .max_text_len = max_text_len,                       /* */
                .filetypes = filetypes,                             /* */
                .filetypes_num = NUM_OF(filetypes),                 /* */
                .file_description_prog = file_description_prog,     /* */
                .headers = headers,                                 /* */
                .headers_num = NUM_OF(headers),                     /* */
                .challenges = challenges,                           /* */
                .challenges_num = NUM_OF(challenges),               /* */
                .wordfilter_inputs = wordfilter_inputs,             /* */
                .wordfilter_inputs_num = NUM_OF(wordfilter_inputs), /* */
                .forbidden_inputs = forbidden_inputs,               /* */
                .forbidden_inputs_num = NUM_OF(forbidden_inputs),   /* */
        };

        /* Set up a minimal part of the system */
        if (setup_locks(&conf) < 0) {
                goto done;
        }

        if (setup_dbs(&conf) < 0) {
                goto done;
        }

        if (setup_write_thread(&conf) < 0) {
                goto done;
        }

        if (lock_acquire_recent() < 0) {
                goto done;
        }

        ret = wt_write_recent_page();
        lock_release_recent();
done:
        clean_locks();
        clean_dbs();
        clean_write_thread();

        return ret;
}
