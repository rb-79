/*
 * Copyright (c) 2017-2020, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

#include "macros.h"
#include "rb79.h"

/* Global configuration */
static const struct configuration *conf;

/* A template for pages 1, 2, ... of every board */
static char *board_page;
static size_t board_page_len;

/* A template for a particular thread's page */
static char *thread_page;
static size_t thread_page_len;

/* A template for the /recent/index.html page */
static char *recent_page;
static size_t recent_page_len;

/* PCRE2 for parsing templates (pull out ${FOO}s) */
static pcre2_code *variables;

#define CHECK_FPRINTF(...) \
        do { \
                if (fprintf(__VA_ARGS__) < 0) { \
                        PERROR_MESSAGE("fprintf"); \
                        goto done; \
                } \
        } while (0)

/*
 * Read in the contents of ${static_www_folder}/_templates/${local_name}
 * to *out.
 *
 * Preconditions:
 *
 *  - ${static_www_folder}/_templates/${local_name} is a readable folder.
 *
 *  - out is not 0.
 *
 *  - Overwriting *out shall not cause a memory leak.
 *
 * Postconditions (success):
 *
 *  - *out contains the full contents of
 *    ${static_www_folder}/_templates/${local_name}.
 */
static int
slurp_file(const char *local_name, char **out)
{
        int ret = -1;
        FILE *f = 0;
        char *path = 0;
        size_t path_len = 0;
        size_t len = 0;

        /* Load board_page */
        path_len = snprintf(0, 0, "%s/_templates/%s", conf->static_www_folder,
                            local_name);

        if (path_len + 1 < path_len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (!(path = malloc(path_len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        sprintf(path, "%s/_templates/%s", conf->static_www_folder, local_name);

        if (!(f = fopen(path, "r"))) {
                ERROR_MESSAGE("cannot open \"%s\"", path);
                PERROR_MESSAGE("fopen");
                goto done;
        }

        fseek(f, 0, SEEK_END);
        len = ftell(f);

        if (len + 1 < len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (!(*out = malloc(len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        fseek(f, 0, SEEK_SET);

        if (fread(*out, 1, len, f) < len) {
                /* Short read - either error or file was modified under us */
                PERROR_MESSAGE("fread");
                goto done;
        }

        (*out)[len] = 0;
        ret = 0;
done:

        if (f) {
                if (fclose(f)) {
                        PERROR_MESSAGE("fclose");
                        ret = -1;
                }
        }

        free(path);

        return ret;
}

/*
 * Initialize any static elements needed for this file
 *
 * Preconditions:
 *
 *  - setup_write_thread() was not invoked more recently than
 *    clean_write_thread().
 *
 * Postconditions (success):
 *
 *  - Any other function in this file may be safely called.
 */
int
setup_write_thread(const struct configuration *in_conf)
{
        conf = in_conf;

        if (slurp_file("board_page", &board_page) < 0) {
                return -1;
        }

        if (!board_page) {
                ERROR_MESSAGE("Could not read board page template");

                return -1;
        }

        board_page_len = strlen(board_page);

        if (slurp_file("thread_page", &thread_page) < 0) {
                return -1;
        }

        thread_page_len = strlen(thread_page);

        if (slurp_file("recent_page", &recent_page) < 0) {
                return -1;
        }

        recent_page_len = strlen(recent_page);
        int err_code = 0;
        PCRE2_SIZE err_offset = 0;
        PCRE2_UCHAR8 err_buf[120];
        const char *variable_pattern_str = "[$][{](?<var>[^}]+)[}]";

        if (!(variables = pcre2_compile((PCRE2_SPTR8) variable_pattern_str,
                                        PCRE2_ZERO_TERMINATED, PCRE2_UTF,
                                        &err_code, &err_offset, 0))) {
                pcre2_get_error_message(err_code, err_buf, 120);
                ERROR_MESSAGE("pcre2_compile: error with pattern \"%s\": %s",
                              variable_pattern_str, err_buf);

                return -1;
        }

        return 0;
}

/*
 * Delete files which are in static_www_folder. Note that "full"
 * means "the full file", not "the full path to the file". This is
 * a defect.
 *
 * Preconditions:
 *
 *  - system_full_path is a string of length system_full_path_len,
 *    something like "/m/src/4921183834.png".
 *
 *  - system_thumb_path is a string of length system_thumb_path_len,
 *    something like "/m/src/4921183834.png".
 *
 *  - As an exception, system_full_path and system_thumb_path may
 *    be 0 if their respective lengths are 0.
 *
 *  - There is at most one board to which system_full_path and
 *    system_thumb_path are associated. For that board, THE LOCK
 *    IS HELD.
 *
 * Postconditions (success):
 *
 *  - Those files have been unlink()'d.
 */
int
wt_remove_files(const char *system_full_path, size_t system_full_path_len, const
                char *system_thumb_path, size_t system_thumb_path_len)
{
        int ret = -1;
        char *abs_full_path = 0;
        size_t abs_full_path_len = 0;
        char *abs_thumb_path = 0;
        size_t abs_thumb_path_len = 0;
        size_t j = 0;

        if (!system_full_path_len) {
                goto done_with_full;
        }

        abs_full_path_len = snprintf(0, 0, "%s%s", conf->static_www_folder,
                                     system_full_path);

        if (abs_full_path_len + 1 < abs_full_path_len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (!(abs_full_path = malloc(abs_full_path_len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        sprintf(abs_full_path, "%s%s", conf->static_www_folder,
                system_full_path);

        if (unlink(abs_full_path) < 0) {
                PERROR_MESSAGE("unlink");
                ERROR_MESSAGE("Failed to unlink(\"%s\")", abs_full_path);
                goto done;
        }

done_with_full:

        if (!system_thumb_path_len) {
                goto done_with_thumb;
        }

        for (j = 0; j < conf->filetypes_num; ++j) {
                const struct filetype *f = &conf->filetypes[j];

                if (f->static_thumbnail &&
                    !strcmp(f->static_thumbnail, system_thumb_path)) {
                        goto done_with_thumb;
                }
        }

        abs_thumb_path_len = snprintf(0, 0, "%s%s", conf->static_www_folder,
                                      system_thumb_path);

        if (abs_thumb_path_len + 1 < abs_thumb_path_len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (!(abs_thumb_path = malloc(abs_thumb_path_len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        sprintf(abs_thumb_path, "%s%s", conf->static_www_folder,
                system_thumb_path);

        if (unlink(abs_thumb_path) < 0) {
                PERROR_MESSAGE("unlink");
                ERROR_MESSAGE("Failed to unlink(\"%s\")", abs_thumb_path);
                goto done;
        }

done_with_thumb:
        ret = 0;
done:
        free(abs_full_path);
        free(abs_thumb_path);

        return ret;
}

/*
 * Delete files which are in static_www_folder. Note that "full"
 * means "the full file", not "the full path to the file". This is
 * a defect.
 *
 * Preconditions:
 *
 *  - board_idx represents a board, AND THE LOCK IS HELD.
 *
 *  - There is a page for thread_id up, e.g. at
 *    ${static_www_folder}/m/res/${thread_id}/index.html.
 *
 * Postconditions (success):
 *
 * - The folder for thread id (e.g.
 *   ${static_www_folder}/m/res/${thread_id}) no longer exists.
 */
int
wt_remove_thread_page(size_t board_idx, uintmax_t thread_id)
{
        int ret = -1;
        char *index_path = 0;
        char *folder_path = 0;
        size_t len = snprintf(0, 0, "%s/%s/res/%ju/index.html",
                              conf->static_www_folder,
                              conf->boards[board_idx].name, thread_id);

        if (len + 1 < len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (!(index_path = malloc(len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        sprintf(index_path, "%s/%s/res/%ju/index.html", conf->static_www_folder,
                conf->boards[board_idx].name, thread_id);

        if (unlink(index_path) < 0) {
                PERROR_MESSAGE("unlink");
                ERROR_MESSAGE("Failed to unlink(\"%s\")", index_path);
                goto done;
        }

        if (!(folder_path = malloc(len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        sprintf(folder_path, "%s/%s/res/%ju", conf->static_www_folder,
                conf->boards[board_idx].name, thread_id);

        if (rmdir(folder_path) < 0) {
                PERROR_MESSAGE("rmdir");
                ERROR_MESSAGE("Failed to rmdir(\"%s\")", folder_path);
                goto done;
        }

        ret = 0;
done:
        free(index_path);
        free(folder_path);

        return ret;
}

/*
 * Write out the HTML file for a board
 *
 * Preconditions:
 *
 *  - setup_write_thread() has been called more recently than
 *    clean_write_thread().
 *
 *  - board_idx represents a board, AND THE LOCK IS HELD.
 *
 *  - thread_ids is an array of length thread_ids_num.
 *
 *  - Each element of thread_ids represents a thread.
 *
 *  - thread_ids is sorted by bump order, most recent thread first.
 *
 * Postconditions (success):
 *
 *  - Files at ${static_www_folder}/${board}/${n}/index.html
 *    has been written out, representing the current state of the
 *    board thread, for n = 0 to however many there should be.
 */
int
wt_write_board(size_t board_idx, uintmax_t *thread_ids, size_t thread_ids_num,
               size_t board_pages_num)
{
        int ret = -1;
        FILE *f = 0;
        size_t len = 0;
        size_t j = 0;
        pcre2_match_data *match_data = 0;
        PCRE2_UCHAR *var_name = 0;
        uint_fast8_t more_left_to_delete = 1;
        char *path = 0;

        /* 3 capture groups: more than sufficient for ${foo} */
        if (!(match_data = pcre2_match_data_create(3, 0))) {
                PERROR_MESSAGE("pcre2_match_data_create");
                goto done;
        }

        len = snprintf(0, 0, "%s/%s/%zu/index.html", conf->static_www_folder,
                       conf->boards[board_idx].name, (size_t) -1);

        if (len + 1 < len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (!(path = malloc(len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        /* First, delete all the pages we won't need */

        /*
         * XXX: should we really be rmdir()ing? Can't we just leave
         * the old directories around?
         */
        j = board_pages_num ? board_pages_num : 1;

        while (j != (size_t) -1 &&
               more_left_to_delete) {
                sprintf(path, "%s/%s/%zu/index.html", conf->static_www_folder,
                        conf->boards[board_idx].name, j);

                if (unlink(path) < 0) {
                        if (errno == ENOENT) {
                                more_left_to_delete = 0;
                                break;
                        }

                        PERROR_MESSAGE("unlink");
                        ERROR_MESSAGE("Failed to unlink(\"%s\")", path);
                        goto done;
                }

                sprintf(path, "%s/%s/%zu", conf->static_www_folder,
                        conf->boards[board_idx].name, j);

                if (rmdir(path) < 0) {
                        PERROR_MESSAGE("rmdir");
                        ERROR_MESSAGE("Failed to rmdir(\"%s\")", path);
                        goto done;
                }

                j++;
        }

        /* Now build the pages we do need */
        for (size_t current_page = 0; !current_page ||
             (current_page < board_pages_num); ++current_page) {
                size_t idx = 0;
                size_t match_pos = 0;
                size_t after_match_pos = 0;
                int nret = 0;
                PCRE2_SIZE var_name_len = 0;
                size_t challenge_id = rand() % conf->challenges_num;
                size_t first_thread_idx = current_page *
                                          conf->boards[board_idx].
                                          threads_per_page;
                size_t num_threads_this_page =
                        conf->boards[board_idx].threads_per_page;

                if (first_thread_idx + num_threads_this_page >=
                    thread_ids_num) {
                        num_threads_this_page = thread_ids_num -
                                                first_thread_idx;
                }

                if (current_page) {
                        sprintf(path, "%s/%s/%zu", conf->static_www_folder,
                                conf->boards[board_idx].name, current_page);
                        errno = 0;

                        /* Make the directory. We shouldn't need mkdir -p. */
                        if (mkdir(path, 0755) < 0) {
                                if (errno != EEXIST) {
                                        PERROR_MESSAGE("mkdir");
                                        ERROR_MESSAGE("mkdir(\"%s\") failed",
                                                      path);
                                        goto done;
                                }
                        }

                        /* Now open the file */
                        sprintf(path, "%s/%s/%zu/index.html",
                                conf->static_www_folder,
                                conf->boards[board_idx].name,
                                current_page);
                } else {
                        sprintf(path, "%s/%s/index.html",
                                conf->static_www_folder,
                                conf->boards[board_idx].name);
                }

                if (f) {
                        if (fclose(f)) {
                                PERROR_MESSAGE("fclose");
                                goto done;
                        }
                }

                if (!(f = fopen(path, "w"))) {
                        PERROR_MESSAGE("fopen");
                        ERROR_MESSAGE("fopen(\"%s\", \"w\") failed", path);
                        goto done;
                }

find_next_bit:

                /* Are we done? */
                if (idx >= board_page_len) {
                        goto success;
                }

                /* Where does ${FOO} appear next? */
                nret = pcre2_match(variables, (PCRE2_SPTR) board_page,
                                   board_page_len, idx, 0, match_data, 0);

                if (nret == PCRE2_ERROR_NOMATCH) {
                        CHECK_FPRINTF(f, "%s", board_page + idx);
                        goto success;
                }

                if (nret < 0) {
                        PCRE2_UCHAR8 err_buf[120];

                        pcre2_get_error_message(nret, err_buf, 120);
                        ERROR_MESSAGE(
                                "pcre2_match: error while matching \"%.*s\": %s"
                                " (PCRE2 %d)", (int) (board_page_len - idx),
                                board_page + idx, err_buf, nret);
                        goto done;
                }

                /* What is FOO? */
                pcre2_substring_free(var_name);
                var_name = 0;
                var_name_len = 0;

                if ((nret = pcre2_substring_get_byname(match_data,
                                                       (PCRE2_SPTR) "var",
                                                       &var_name,
                                                       &var_name_len))) {
                        PCRE2_UCHAR8 err_buf[120];

                        pcre2_get_error_message(nret, err_buf, 120);
                        ERROR_MESSAGE(
                                "pcre2_substring_get_byname: %s (PCRE2 %d)",
                                err_buf,
                                nret);
                        goto done;
                }

                match_pos = pcre2_get_ovector_pointer(match_data)[0];
                after_match_pos = pcre2_get_ovector_pointer(match_data)[1];
                CHECK_FPRINTF(f, "%.*s", (int) (match_pos - idx), board_page +
                              idx);
                idx = match_pos;

                if (!strncasecmp((const char *) var_name, "BOARD",
                                 var_name_len)) {
                        CHECK_FPRINTF(f, "%s", conf->boards[board_idx].name);
                } else if (!strncasecmp((const char *) var_name, "BOARD_TITLE",
                                        var_name_len)) {
                        CHECK_FPRINTF(f, "%s", conf->boards[board_idx].title);
                } else if (!strncasecmp((const char *) var_name, "CHALLENGE",
                                        var_name_len)) {
                        CHECK_FPRINTF(f, "%s",
                                      conf->challenges[challenge_id].question);
                } else if (!strncasecmp((const char *) var_name, "CHALLENGE_ID",
                                        var_name_len)) {
                        CHECK_FPRINTF(f, "%zu", challenge_id);
                } else if (!strncasecmp((const char *) var_name, "PAGELINKS",
                                        var_name_len)) {
                        CHECK_FPRINTF(f, "[<a href=\"/%s\">0</a>] ",
                                      conf->boards[board_idx].name);

                        for (size_t j = 1; j < board_pages_num; ++j) {
                                CHECK_FPRINTF(f,
                                              "[<a href=\"/%s/%zu\">%zu</a>] ",
                                              conf->boards[board_idx].name, j,
                                              j);
                        }
                } else if (!strncasecmp((const char *) var_name,
                                        "RANDOM_HEADER", var_name_len)) {
                        CHECK_FPRINTF(f, "%s", conf->headers[rand() %
                                                             conf->headers_num]);
                } else if (!strncasecmp((const char *) var_name, "THREADS",
                                        var_name_len)) {
                        /*
                         * We have to transfer control to db-ZZZ
                         * for a bit - it comes back through
                         * wt_write_post().
                         */
                        db_writeback_thread_summaries(board_idx, thread_ids +
                                                      first_thread_idx,
                                                      num_threads_this_page, f);
                } else {
                        CHECK_FPRINTF(f, "${%.*s}", (int) var_name_len,
                                      var_name);
                }

                idx = after_match_pos;
                goto find_next_bit;
success:
                ;
        }

        ret = 0;
done:
        pcre2_match_data_free(match_data);
        pcre2_substring_free(var_name);

        if (f) {
                if (fclose(f)) {
                        PERROR_MESSAGE("fclose");
                        ret = -1;
                }
        }

        free(path);

        return ret;
}

/*
 * Write p out to f as the first post of a thread.
 *
 * This function is intended to be called by
 * db_writeback_posts_in_thread(), which is called from wt_write_thread().
 *
 * Preconditions:
 *
 *  - setup_write_thread() has been called more recently than
 *    clean_write_thread().
 *
 *  - p is a fully filled stored_post (i.e. from a row in the database).
 *
 *  - f is an open, writable FILE *.
 *
 *  - board_idx represents a baord.
 *
 *  - If is_summary is not zero, then board_name is a string like
 *    "m" or "sf". Furthermore, if is_op is also not zero, then
 *    total_posts_in_thread is a number suitable for describing the
 *    thread.
 *
 * Postconditions (success):
 *
 *  - the contents of p have been written to f, which returned no
 *    errors. The format is not precisely defined.
 */
int
wt_write_post(struct prepared_post *p, FILE *f, uint_fast8_t is_op, uint_fast8_t
              is_summary, uint_fast8_t is_recent, const char *board_name,
              uintmax_t in_thread,
              uintmax_t total_posts_in_thread, uint_fast8_t hr_before)
{
        int ret = -1;
        char *human_readable_time = util_iso8601_from_time_t(p->now);

        if (!human_readable_time) {
                ERROR_MESSAGE("util_iso_8601_from_time(%ju) failed",
                              (uintmax_t) p->now);
        }

        if (hr_before) {
                /* XXX: make sure this looks decent, both in ff and elinks */
                CHECK_FPRINTF(f, "<hr />");
        }

        if (is_op) {
                CHECK_FPRINTF(f, "<div class=\"op-post\" id=\"post%ju\">",
                              p->id);
        } else {
                CHECK_FPRINTF(f, "<div class=\"reply-container\">");

                if (is_recent) {
                        CHECK_FPRINTF(f, "<div class=\"full-link\">");
                        CHECK_FPRINTF(f, "<a class=\"a-subtle\" href=\"");
                        CHECK_FPRINTF(f, "/%s/res/%ju/#post%ju\">/%s/</a> ",
                                      board_name, in_thread, p->id, board_name);
                        CHECK_FPRINTF(f, "</div>");
                } else {
                        CHECK_FPRINTF(f,
                                      "<div class=\"reply-spacer\">..</div>");
                }

                CHECK_FPRINTF(f, "<div class=\"reply-post\" id=\"post%ju\">",
                              p->id);
        }

        CHECK_FPRINTF(f, "<span class=\"post-info\">");

        if (p->subject_len) {
                CHECK_FPRINTF(f, "<span class=\"post-subject\">%.*s</span> ",
                              (int) p->subject_len, p->subject);
        }

        if (p->name_len) {
                if (p->email_len) {
                        CHECK_FPRINTF(f, "<a href=\"mailto:%.*s\">",
                                      (int) p->email_len, p->email);
                }

                CHECK_FPRINTF(f, "<span class=\"post-author\">%.*s</span>",
                              (int) p->name_len, p->name);

                if (p->email_len) {
                        CHECK_FPRINTF(f, "</a>");
                }

                if (p->tripcode_len) {
                        CHECK_FPRINTF(f, "<span class=\"post-tripcode\">");
                        CHECK_FPRINTF(f, "!%.*s</span>", (int) p->tripcode_len,
                                      p->tripcode);
                }
        }

        CHECK_FPRINTF(f, " <span class=\"post-time\">%s</span>",
                      human_readable_time ? human_readable_time : "NEVER");
        CHECK_FPRINTF(f, " <span class=\"post-number\">No. %ju</span>", p->id);

        if (is_op) {
                if (p->thread_closed) {
                        CHECK_FPRINTF(f, " <span class=\"thread-closed\">");
                        CHECK_FPRINTF(f, "Closed</span>");
                }

                if (p->thread_stickied) {
                        CHECK_FPRINTF(f, " <span class=\"thread-stickied\">");
                        CHECK_FPRINTF(f, "Sticky</span>");
                }
        }

        if (is_summary &&
            is_op) {
                CHECK_FPRINTF(f, " [<a href=\"/%s/res/%ju\">View</a>]",
                              board_name, p->id);
        }

        CHECK_FPRINTF(f, "</span><br />");

        if (p->system_full_path_len) {
                CHECK_FPRINTF(f, "<span class=\"file-info\">");
                CHECK_FPRINTF(f, "<a class=\"filelink\" href=\"%.*s\">",
                              (int) p->system_full_path_len,
                              p->system_full_path);
                CHECK_FPRINTF(f, "%.*s", (int) p->file_name_len, p->file_name);
                CHECK_FPRINTF(f, "</a>");
                CHECK_FPRINTF(f, " (%.*s)</span><br />", (int) p->file_info_len,
                              p->file_info);
                CHECK_FPRINTF(f, "<div class=\"post-image\">");
                CHECK_FPRINTF(f, "<a class=\"filelink\" href=\"%.*s\">",
                              (int) p->system_full_path_len,
                              p->system_full_path);
                CHECK_FPRINTF(f, "<img src=\"%.*s\" alt=\"image\" ",
                              (int) p->system_thumb_path_len,
                              p->system_thumb_path);
                CHECK_FPRINTF(f, "class=\"thumb\"/>");
                CHECK_FPRINTF(f, "</a></div>");
        }

        CHECK_FPRINTF(f, "<div class=\"post-comment\">");

        if (p->comment_len) {
                CHECK_FPRINTF(f, "%.*s", (int) p->comment_len, p->comment);
        } else {
                CHECK_FPRINTF(f, "&nbsp;");
        }

        CHECK_FPRINTF(f, "</div>");
        CHECK_FPRINTF(f, "</div>");

        if (!is_op) {
                CHECK_FPRINTF(f, "</div>");
        }

        if (is_summary &&
            is_op) {
                CHECK_FPRINTF(f, "<div class=\"reply-count-info\">");
                CHECK_FPRINTF(f, "%ju post%s in thread. ",
                              total_posts_in_thread, (total_posts_in_thread ==
                                                      1) ? "" : "s");
                CHECK_FPRINTF(f, "<a href=\"/%s/res/%ju\">View thread</a>",
                              board_name, p->id);
                CHECK_FPRINTF(f, "</div>");
        }

        ret = 0;
done:
        free(human_readable_time);

        return ret;
}

/*
 * Write out the HTML for the /recent/ page
 *
 * Preconditions:
 *
 *  - setup_write_thread() has been called more recently than
 *    clean_write_thread().
 *
 *  - for the recent page, THE LOCK IS HELD.
 *
 * Postconditions (success):
 *
 *  - A file at ${static_www_folder}/recent/index.html has been
 *    written out, representing the last few posts on the site.
 */
int
wt_write_recent_page(void)
{
        int ret = -1;
        size_t idx = 0;
        size_t match_pos = 0;
        size_t after_match_pos = 0;
        pcre2_match_data *match_data = 0;
        int nret = 0;
        char *path = 0;
        size_t len = 0;
        FILE *f = 0;
        PCRE2_UCHAR *var_name = 0;
        PCRE2_SIZE var_name_len = 0;

        len = snprintf(0, 0, "%s/recent/index.html", conf->static_www_folder);

        if (len + 1 < len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (!(path = malloc(len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        sprintf(path, "%s/recent", conf->static_www_folder);

        /* Make the directory. We shouldn't need mkdir -p. */
        if (mkdir(path, 0755) < 0) {
                if (errno != EEXIST) {
                        PERROR_MESSAGE("mkdir");
                        ERROR_MESSAGE("mkdir(\"%s\") failed", path);
                        goto done;
                }
        }

        sprintf(path, "%s/recent/index.html", conf->static_www_folder);

        if (!(f = fopen(path, "w"))) {
                PERROR_MESSAGE("fopen");
                ERROR_MESSAGE("fopen(\"%s\", \"w\") failed", path);
                goto done;
        }

        /* 3 capture groups: more than sufficient for ${foo} */
        if (!(match_data = pcre2_match_data_create(3, 0))) {
                PERROR_MESSAGE("pcre2_match_data_create");
                goto done;
        }

find_next_bit:

        /* Are we done? */
        if (idx >= recent_page_len) {
                goto success;
        }

        /* Where does ${FOO} appear next? */
        nret = pcre2_match(variables, (PCRE2_SPTR) recent_page, recent_page_len,
                           idx, 0, match_data, 0);

        if (nret == PCRE2_ERROR_NOMATCH) {
                CHECK_FPRINTF(f, "%s", recent_page + idx);
                goto success;
        }

        if (nret < 0) {
                PCRE2_UCHAR8 err_buf[120];

                pcre2_get_error_message(nret, err_buf, 120);
                ERROR_MESSAGE("pcre2_match: error while matching \"%.*s\": %s"
                              " (PCRE2 %d)", (int) (recent_page_len - idx),
                              recent_page + idx,
                              err_buf, nret);
                goto done;
        }

        /* What is FOO? */
        pcre2_substring_free(var_name);
        var_name = 0;
        var_name_len = 0;

        if ((nret = pcre2_substring_get_byname(match_data, (PCRE2_SPTR) "var",
                                               &var_name, &var_name_len))) {
                PCRE2_UCHAR8 err_buf[120];

                pcre2_get_error_message(nret, err_buf, 120);
                ERROR_MESSAGE("pcre2_substring_get_byname: %s (PCRE2 %d)",
                              err_buf, nret);
                goto done;
        }

        match_pos = pcre2_get_ovector_pointer(match_data)[0];
        after_match_pos = pcre2_get_ovector_pointer(match_data)[1];
        CHECK_FPRINTF(f, "%.*s", (int) (match_pos - idx), recent_page + idx);
        idx = match_pos;

        if (!strncasecmp((const char *) var_name, "RANDOM_HEADER",
                         var_name_len)) {
                CHECK_FPRINTF(f, "%s", conf->headers[rand() %
                                                     conf->headers_num]);
        } else if (!strncasecmp((const char *) var_name, "RECENT_POSTS",
                                var_name_len)) {
                /*
                 * We have to transfer control to db-ZZZ for a bit
                 * - it comes back through wt_write_post().
                 */
                db_writeback_recent_posts(f, wt_write_post);
        } else {
                CHECK_FPRINTF(f, "${%.*s}", (int) var_name_len, var_name);
        }

        idx = after_match_pos;
        goto find_next_bit;
success:
        ret = 0;
done:
        pcre2_match_data_free(match_data);
        pcre2_substring_free(var_name);

        if (f) {
                if (fclose(f)) {
                        PERROR_MESSAGE("fclose");
                        ret = -1;
                }
        }

        free(path);

        return ret;
}

/*
 * Write out the HTML file for a thread
 *
 * Preconditions:
 *
 *  - setup_write_thread() has been called more recently than
 *    clean_write_thread().
 *
 *  - board_idx represents a board, AND THE LOCK IS HELD.
 *
 *  - thread is the post_id of a thread.
 *
 * Postconditions (success):
 *
 *  - A file at ${static_www_folder}/${board}/res/${thread}/index.html
 *    has been written out, representing the recent state of the
 *    thread.
 */
int
wt_write_thread(size_t board_idx, uintmax_t thread)
{
        int ret = -1;
        size_t idx = 0;
        size_t match_pos = 0;
        size_t after_match_pos = 0;
        pcre2_match_data *match_data = 0;
        int nret = 0;
        FILE *f = 0;
        size_t len = 0;
        char *index_path = 0;
        PCRE2_UCHAR *var_name = 0;
        PCRE2_SIZE var_name_len = 0;
        size_t challenge_id = rand() % conf->challenges_num;

        len = snprintf(0, 0, "%s/%s/res/%ju/index.html",
                       conf->static_www_folder, conf->boards[board_idx].name,
                       thread);

        if (len + 1 < len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (!(index_path = malloc(len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        sprintf(index_path, "%s/%s/res/%ju", conf->static_www_folder,
                conf->boards[board_idx].name, thread);
        errno = 0;

        /* Make the directory. We shouldn't need mkdir -p. */
        if (mkdir(index_path, 0755) < 0) {
                if (errno != EEXIST) {
                        PERROR_MESSAGE("mkdir");
                        ERROR_MESSAGE("mkdir(\"%s\") failed", index_path);
                        goto done;
                }
        }

        /* Now open the file */
        sprintf(index_path, "%s/%s/res/%ju/index.html", conf->static_www_folder,
                conf->boards[board_idx].name, thread);

        if (!(f = fopen(index_path, "w"))) {
                PERROR_MESSAGE("fopen");
                ERROR_MESSAGE("fopen(\"%s\", \"w\") failed", index_path);
                goto done;
        }

        /* 3 capture groups: more than sufficient for ${foo} */
        if (!(match_data = pcre2_match_data_create(3, 0))) {
                PERROR_MESSAGE("pcre2_match_data_create");
                goto done;
        }

find_next_bit:

        /* Are we done? */
        if (idx >= thread_page_len) {
                goto success;
        }

        /* Where does ${FOO} appear next? */
        nret = pcre2_match(variables, (PCRE2_SPTR) thread_page, thread_page_len,
                           idx, 0, match_data, 0);

        if (nret == PCRE2_ERROR_NOMATCH) {
                CHECK_FPRINTF(f, "%s", thread_page + idx);
                goto success;
        }

        if (nret < 0) {
                PCRE2_UCHAR8 err_buf[120];

                pcre2_get_error_message(nret, err_buf, 120);
                ERROR_MESSAGE("pcre2_match: error while matching \"%.*s\": %s"
                              " (PCRE2 %d)", (int) (thread_page_len - idx),
                              thread_page + idx,
                              err_buf, nret);
                goto done;
        }

        /* What is FOO? */
        pcre2_substring_free(var_name);
        var_name = 0;
        var_name_len = 0;

        if ((nret = pcre2_substring_get_byname(match_data, (PCRE2_SPTR) "var",
                                               &var_name, &var_name_len))) {
                PCRE2_UCHAR8 err_buf[120];

                pcre2_get_error_message(nret, err_buf, 120);
                ERROR_MESSAGE("pcre2_substring_get_byname: %s (PCRE2 %d)",
                              err_buf, nret);
                goto done;
        }

        match_pos = pcre2_get_ovector_pointer(match_data)[0];
        after_match_pos = pcre2_get_ovector_pointer(match_data)[1];
        CHECK_FPRINTF(f, "%.*s", (int) (match_pos - idx), thread_page + idx);
        idx = match_pos;

        if (!strncasecmp((const char *) var_name, "BOARD", var_name_len)) {
                CHECK_FPRINTF(f, "%s", conf->boards[board_idx].name);
        } else if (!strncasecmp((const char *) var_name, "BOARD_TITLE",
                                var_name_len)) {
                CHECK_FPRINTF(f, "%s", conf->boards[board_idx].title);
        } else if (!strncasecmp((const char *) var_name, "CHALLENGE",
                                var_name_len)) {
                CHECK_FPRINTF(f, "%s", conf->challenges[challenge_id].question);
        } else if (!strncasecmp((const char *) var_name, "CHALLENGE_ID",
                                var_name_len)) {
                CHECK_FPRINTF(f, "%zu", challenge_id);
        } else if (!strncasecmp((const char *) var_name, "OP_SUBJECT",
                                var_name_len)) {
                char *subject = 0;
                size_t subject_len = 0;

                if (db_extract_subject(board_idx, thread, &subject,
                                       &subject_len) < 0) {
                        goto done;
                }

                if (subject) {
                        CHECK_FPRINTF(f, "%.*s", (int) subject_len, subject);
                }

                free(subject);
        } else if (!strncasecmp((const char *) var_name, "RANDOM_HEADER",
                                var_name_len)) {
                CHECK_FPRINTF(f, "%s", conf->headers[rand() %
                                                     conf->headers_num]);
        } else if (!strncasecmp((const char *) var_name, "THREAD_NUMBER",
                                var_name_len)) {
                CHECK_FPRINTF(f, "%ju", thread);
        } else if (!strncasecmp((const char *) var_name, "POSTS",
                                var_name_len)) {
                /*
                 * We have to transfer control to db-ZZZ for a bit
                 * - it comes back through wt_write_post().
                 */
                db_writeback_posts_in_thread(board_idx, thread, f,
                                             wt_write_post);
        } else {
                CHECK_FPRINTF(f, "${%.*s}", (int) var_name_len, var_name);
        }

        idx = after_match_pos;
        goto find_next_bit;
success:
        ret = 0;
done:
        pcre2_match_data_free(match_data);
        pcre2_substring_free(var_name);

        if (f) {
                if (fclose(f)) {
                        PERROR_MESSAGE("fclose");
                        ret = -1;
                }
        }

        free(index_path);

        return ret;
}

/*
 * Clean any memory from this file
 *
 * Postconditions (success):
 *
 *  - Valgrind won't report any memory leaks from this file.
 *
 *  - setup_write_thread() can be safely called again.
 */
void
clean_write_thread(void)
{
        conf = 0;
        free(board_page);
        free(thread_page);
        board_page = 0;
        board_page_len = 0;
        thread_page = 0;
        thread_page_len = 0;
        pcre2_code_free(variables);
        variables = 0;
}
