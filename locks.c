/*
 * Copyright (c) 2017-2020, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include "macros.h"
#include "rb79.h"

static int *lock_table = 0;
static size_t lock_table_len = 0;
static int lock_recent;

/*
 * Set up the lock files and make sure we can connect to them.
 *
 * Preconditions:
 *
 *  - setup_locks() was not invoked more recently than clean_locks().
 *
 * Postconditions (success):
 *
 *  - Any other function in this file may be safely called.
 */
int
setup_locks(const struct configuration *conf)
{
        int ret = -1;
        char *path = 0;
        size_t len = 0;

        if (lock_table) {
                return 0;
        }

        if (!(lock_table = calloc(conf->boards_num, sizeof *lock_table))) {
                PERROR_MESSAGE("malloc");
                ERROR_MESSAGE("Cannot set up locks");
                goto done;
        }

        for (size_t j = 0; j < conf->boards_num; ++j) {
                lock_table[j] = -1;
        }

        /* Locks for each board */
        for (size_t j = 0; j < conf->boards_num; ++j) {
                len = snprintf(0, 0, "%s/lock_board_%s", conf->work_path,
                               conf->boards[j].name);

                if (len + 1 < len) {
                        ERROR_MESSAGE("overflow");
                        goto done;
                }

                if (!(path = malloc(len + 1))) {
                        PERROR_MESSAGE("malloc");
                        goto done;
                }

                sprintf(path, "%s/lock_board_%s", conf->work_path,
                        conf->boards[j].name);

                if (!(lock_table[j] = open(path, O_RDWR | O_CREAT, 0700))) {
                        PERROR_MESSAGE("open");
                        ERROR_MESSAGE("Cannot open or create lock file %s",
                                      path);
                        goto done;
                }

                free(path);
                path = 0;
        }

        lock_table_len = conf->boards_num;

        /* Lock for the recent page */
        len = snprintf(0, 0, "%s/lock_recent", conf->work_path);

        if (len + 1 < len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (!(path = malloc(len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        sprintf(path, "%s/lock_recent", conf->work_path);

        if (!(lock_recent = open(path, O_RDWR | O_CREAT, 0700))) {
                PERROR_MESSAGE("open");
                ERROR_MESSAGE("Cannot open or create lock file %s", path);
                goto done;
        }

        free(path);
        path = 0;

        /* Now we've got all the locks */
        ret = 0;
done:
        free(path);

        return ret;
}

/*
 * Get a lock for a board
 *
 * Preconditions:
 *
 *  - board_idx represents a board.
 *
 *  - The lock for board_idx is not held in this program.
 *
 * Postconditions (success):
 *
 *  - The lock for board_idx is now held by this program.
 */
int
lock_acquire(size_t board_idx)
{
        int ret = -1;

        if (!lock_table ||
            lock_table[board_idx] < 0) {
                ERROR_MESSAGE("lock_table isn't set up yet");
                goto done;
        }

        if (lockf(lock_table[board_idx], F_LOCK, 0) < 0) {
                PERROR_MESSAGE("lockf");
                ERROR_MESSAGE("Cannot lock board %zu", board_idx);
                goto done;
        }

        ret = 0;
done:

        return ret;
}

/*
 * Get a lock for the recent page
 *
 * Preconditions:
 *
 *  - The lock for the recent page is not held in this program.
 *
 * Postconditions (success):
 *
 *  - The lock for the recent page is now held by this program.
 */
int
lock_acquire_recent(void)
{
        int ret = -1;

        if (lock_recent < 0) {
                ERROR_MESSAGE("lock_recent isn't set up yet");
                goto done;
        }

        if (lockf(lock_recent, F_LOCK, 0) < 0) {
                PERROR_MESSAGE("lockf");
                ERROR_MESSAGE("Cannot lock recent page");
                goto done;
        }

        ret = 0;
done:

        return ret;
}

/*
 * Release a lock for a board
 *
 * Preconditions:
 *
 *  - board_idx represents a board.
 *
 *  - The lock for board_idx is held in this program.
 *
 * Postconditions (success):
 *
 *  - The lock for board_idx is now not held by this program.
 */
int
lock_release(size_t board_idx)
{
        if (!lock_table ||
            lock_table[board_idx] < 0) {
                return 0;
        }

        if (lockf(lock_table[board_idx], F_ULOCK, 0) < 0) {
                PERROR_MESSAGE("lockf");
                ERROR_MESSAGE("Cannot release lock for board %zu", board_idx);

                return -1;
        }

        return 0;
}

/*
 * Release a lock for a board
 *
 * Preconditions:
 *
 *  - The lock for the recent page is held in this program.
 *
 * Postconditions (success):
 *
 *  - The lock for the recent page is now not held by this program.
 */
int
lock_release_recent(void)
{
        if (lock_recent < 0) {
                return 0;
        }

        if (lockf(lock_recent, F_ULOCK, 0) < 0) {
                PERROR_MESSAGE("lockf");
                ERROR_MESSAGE("Cannot release lock for recent page");

                return -1;
        }

        return 0;
}

/*
 * Clean up any memory from this file
 *
 * Postconditions (success):
 *
 *  - Valgrind won't report any memory leaks from this file.
 *
 *  - setup_locks() can be safely called again.
 */
int
clean_locks(void)
{
        if (!lock_table) {
                return 0;
        }

        for (size_t j = 0; j < lock_table_len; ++j) {
                lock_release(j);
                close(lock_table[j]);
        }

        free(lock_table);
        lock_table = 0;
        lock_release_recent();
        close(lock_recent);

        return 0;
}
