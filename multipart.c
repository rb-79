/*
 * Copyright (c) 2017-2020, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <gmime/gmime.h>

#include "macros.h"
#include "rb79.h"

/* User data for callback */
struct gmime_cb_data {
        /* */
        struct post_cmd pc;
        int had_error;
};

/* Initialize GMime */
int
setup_multipart(void)
{
        g_mime_init();

        return 0;
}

/* Callback to add things - get stuff out of mime so we can strcmp() it */
static void
record_argument(GMimeObject *parent, GMimeObject *part, gpointer user_data)
{
        UNUSED(parent);
        struct gmime_cb_data *d = (struct gmime_cb_data *) user_data;

        if (!(GMIME_IS_PART(part))) {
                return;
        }

        if (GMIME_IS_MESSAGE_PART(part)) {
                GMimeMessage *m = g_mime_message_part_get_message(
                        (GMimeMessagePart *) part);

                g_mime_message_foreach(m, record_argument, user_data);
        }

        GMimePart *p = (GMimePart *) part;
        const char *name = g_mime_object_get_content_disposition_parameter(part,
                                                                           "name");

        if (!name) {
                return;
        }

        char **set_this = 0;
        size_t *set_this_len = 0;

        if (!strcmp(name, "action")) {
                set_this = &d->pc.raw.action;
                set_this_len = &d->pc.raw.action_len;
        } else if (!strcmp(name, "board")) {
                set_this = &d->pc.raw.board;
                set_this_len = &d->pc.raw.board_len;
        } else if (!strcmp(name, "thread")) {
                set_this = &d->pc.raw.thread;
                set_this_len = &d->pc.raw.thread_len;
        } else if (!strcmp(name, "post")) {
                set_this = &d->pc.raw.post;
                set_this_len = &d->pc.raw.post_len;
        } else if (!strcmp(name, "name")) {
                set_this = &d->pc.raw.name;
                set_this_len = &d->pc.raw.name_len;
        } else if (!strcmp(name, "email")) {
                set_this = &d->pc.raw.email;
                set_this_len = &d->pc.raw.email_len;
        } else if (!strcmp(name, "subject")) {
                set_this = &d->pc.raw.subject;
                set_this_len = &d->pc.raw.subject_len;
        } else if (!strcmp(name, "comment")) {
                set_this = &d->pc.raw.comment;
                set_this_len = &d->pc.raw.comment_len;
        } else if (!strcmp(name, "comment")) {
                set_this = &d->pc.raw.comment;
                set_this_len = &d->pc.raw.comment_len;
        } else if (!strcmp(name, "challengeid")) {
                set_this = &d->pc.raw.challenge_id;
                set_this_len = &d->pc.raw.challenge_id_len;
        } else if (!strcmp(name, "challengeresponse")) {
                set_this = &d->pc.raw.challenge_response;
                set_this_len = &d->pc.raw.challenge_response_len;
        } else if (!strcmp(name, "file")) {
                set_this = &d->pc.raw.file_contents;
                set_this_len = &d->pc.raw.file_contents_len;
                free(d->pc.raw.file_name);
                d->pc.raw.file_name = 0;
                d->pc.raw.file_name_len = 0;
                const char *file_name = g_mime_part_get_filename(p);

                if (!file_name) {
                        file_name = "[No filename provided]";
                }

                if (!(d->pc.raw.file_name = strdup(file_name))) {
                        ERROR_MESSAGE("strdup");
                        d->had_error = 1;

                        return;
                }

                d->pc.raw.file_name_len = strlen(d->pc.raw.file_name);
        }

        if (!set_this) {
                return;
        }

        GMimeDataWrapper *w = 0;
        GMimeStream *s = 0;
        size_t l = 0;
        char *buf = 0;

        if (!(w = g_mime_part_get_content(p))) {
                d->had_error = 1;

                return;
        }

        if (!(s = g_mime_data_wrapper_get_stream(w))) {
                d->had_error = 1;

                return;
        }

        l = g_mime_stream_length(s);

        if (l + 1 < l) {
                ERROR_MESSAGE("overflow");
                d->had_error = 1;

                return;
        }

        if (!(buf = malloc(l + 1))) {
                PERROR_MESSAGE("malloc");
                d->had_error = 1;

                return;
        }

        g_mime_stream_read(s, buf, l);
        buf[l] = '\0';
        free(*set_this);
        *set_this = buf;
        *set_this_len = l;

        /* Don't g_object_unref here.  I think. */
}

/*
 * This is segmented off because, one day, I want to go full NIH.
 * No pre/postconditions until then.
 */
int
multipart_decompose(const char *full_data, size_t full_data_len, struct
                    post_cmd *post_cmd)
{
        GMimeStream *s = 0;
        GMimeParser *p = 0;
        GMimeMessage *m = 0;
        int had_error = 0;
        int ret = -1;
        struct gmime_cb_data d = { 0 };

        memcpy(&d, post_cmd, sizeof *post_cmd);

        if (!(s = g_mime_stream_mem_new_with_buffer(full_data,
                                                    full_data_len))) {
                ERROR_MESSAGE("Some kind of GMime error");
                goto done;
        }

        if (!(p = g_mime_parser_new_with_stream(s))) {
                ERROR_MESSAGE("Some kind of GMime error");
                goto done;
        }

        if (!(m = g_mime_parser_construct_message(p, 0))) {
                ERROR_MESSAGE("Some kind of GMime error");
                goto done;
        }

        g_mime_message_foreach(m, record_argument, (gpointer) & d);

        if (had_error) {
                ERROR_MESSAGE("Some kind of GMime error");
                goto done;
        }

        memcpy(post_cmd, &d.pc, sizeof *post_cmd);
        ret = 0;
done:

        if (s) {
                g_object_unref(s);
        }

        if (p) {
                g_object_unref(p);
        }

        if (m) {
                g_object_unref(m);
        }

        return ret;
}

/* Tear down GMime */
int
clean_multipart(void)
{
        g_mime_shutdown();

        return 0;
}
