.SUFFIXES:
.SUFFIXES: .o .c

CFLAGS ?=
LDFLAGS ?=
PKG_CONFIG ?= pkg-config

PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
SHAREDIR ?= $(PREFIX)/share
MANDIR ?= $(SHAREDIR)/man

CFLAGS += -std=c99 -D_POSIX_C_SOURCE=200809L -D_DEFAULT_SOURCE

# Do you want logging?
CFLAGS += -DWRITE_LOGS=1

# fcgi - grumble grumble no pkg-config entry
LDFLAGS += -lfcgi

# libmagic - grumble grumble no $(PKG_CONFIG) entry
LDFLAGS += -lmagic

# sqlite3
CFLAGS += $(shell $(PKG_CONFIG) --cflags sqlite3)
LDFLAGS += $(shell $(PKG_CONFIG) --libs sqlite3)

# libpcre2
CFLAGS += $(shell $(PKG_CONFIG) --cflags libpcre2-8)
LDFLAGS += $(shell $(PKG_CONFIG) --libs libpcre2-8)

# libsodium
CFLAGS += $(shell $(PKG_CONFIG) --cflags libsodium)
LDFLAGS += $(shell $(PKG_CONFIG) --libs libsodium)

# gmime - one of these day's I'll do it without fscking glib
CFLAGS += $(shell $(PKG_CONFIG) --cflags gmime-3.0)
LDFLAGS += $(shell $(PKG_CONFIG) --libs gmime-3.0)

# Debug
# CFLAGS += -g -O0 -pedantic -Wall -Wextra -Werror

# The `-O3' makes it go three times faster
# CFLAGS += -O3 -pedantic -Wall -Wextra -Werror

# If this is `foodb', then you'd better have db-foodb.c ready
DB_TYPE = sqlite3

default: all
all: \
	rb79-ban-ip \
	rb79-delete-post \
	rb79-moderate-post \
	rb79-server \
	rb79-update-recent-page \
	rb79-view-thread

SERVER_OFILES = \
	db-$(DB_TYPE).o \
	locks.o \
	multipart.o \
	preconditions.o \
	rb79-server.o \
	sanitize-comment.o \
	sanitize-file.o \
	tripcodes.o \
	util.o \
	write-thread.o

config.h: config.def.h
	test -f config.h || cp config.def.h config.h
	touch config.h

%.o: %.c config.h rb79.h macros.h unicode-transforms.h
	$(CC) $(CFLAGS) -c -o $@ $<

rb79-server: $(SERVER_OFILES)
	$(CC) -o $@ $^ $(LDFLAGS)

rb79-ban-ip: db-$(DB_TYPE).o locks.o rb79-ban-ip.o util.o \
		 write-thread.o
	$(CC) -o $@ $^ $(LDFLAGS)

rb79-delete-post: db-$(DB_TYPE).o locks.o rb79-delete-post.o util.o \
		 write-thread.o
	$(CC) -o $@ $^ $(LDFLAGS)

rb79-moderate-post: db-$(DB_TYPE).o locks.o rb79-moderate-post.o \
		 util.o write-thread.o
	$(CC) -o $@ $^ $(LDFLAGS)

rb79-update-recent-page: db-$(DB_TYPE).o locks.o write-thread.o util.o \
		rb79-update-recent-page.o
	$(CC) -o $@ $^ $(LDFLAGS)

rb79-view-thread: db-$(DB_TYPE).o locks.o rb79-view-thread.o util.o \
		 write-thread.o
	$(CC) -o $@ $^ $(LDFLAGS)


.PHONY: clean
clean:
	find -name '*.o' -delete
	rm -f rb79-server
	rm -f rb79-ban-ip
	rm -f rb79-delete-post
	rm -f rb79-moderate-post
	rm -f rb79-update-recent-page
	rm -f rb79-view-thread

.PHONY: install
install: all
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -f rb79-server $(DESTDIR)$(BINDIR)/
	cp -f tools/rb79-describe-file.sh $(DESTDIR)$(BINDIR)/rb79-describe-file
	cp -f rb79-ban-ip $(DESTDIR)$(BINDIR)/
	cp -f rb79-delete-post $(DESTDIR)$(BINDIR)/
	cp -f rb79-moderate-post $(DESTDIR)$(BINDIR)/
	cp -f rb79-update-recent-page $(DESTDIR)$(BINDIR)/
	cp -f rb79-view-thread $(DESTDIR)$(BINDIR)/
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	cp -f rb79.1 $(DESTDIR)$(MANDIR)/man1/
	cp -f rb79-ban-ip.1 $(DESTDIR)$(MANDIR)/man1/
	cp -f rb79-delete-post.1 $(DESTDIR)$(MANDIR)/man1/
	cp -f rb79-moderate-post.1 $(DESTDIR)$(MANDIR)/man1/
	cp -f rb79-server.1 $(DESTDIR)$(MANDIR)/man1/
	cp -f rb79-update-recent-page.1 $(DESTDIR)$(MANDIR)/man1/
	cp -f rb79-view-thread.1 $(DESTDIR)$(MANDIR)/man1/

.PHONY: uninstall
uninstall:
	cd $(DESTDIR)$(BINDIR) && rm -f rb79-server
	cd $(DESTDIR)$(BINDIR) && rm -f rb79-describe-file
	cd $(DESTDIR)$(BINDIR) && rm -f rb79-ban-ip
	cd $(DESTDIR)$(BINDIR) && rm -f rb79-delete-post
	cd $(DESTDIR)$(BINDIR) && rm -f rb79-moderate-post
	cd $(DESTDIR)$(BINDIR) && rm -f rb79-update-recent-page
	cd $(DESTDIR)$(BINDIR) && rm -f rb79-view-thread
	cd $(DESTDIR)$(MANDIR) && rm -f man1/rb79.1
	cd $(DESTDIR)$(MANDIR) && rm -f man1/rb79-ban-ip.1
	cd $(DESTDIR)$(MANDIR) && rm -f man1/rb79-delete-post.1
	cd $(DESTDIR)$(MANDIR) && rm -f man1/rb79-moderate-post.1
	cd $(DESTDIR)$(MANDIR) && rm -f man1/rb79-server.1
	cd $(DESTDIR)$(MANDIR) && rm -f man1/rb79-update-recent-page.1
	cd $(DESTDIR)$(MANDIR) && rm -f man1/rb79-view-thread.1
