/*
 * Copyright (c) 2017-2020, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sodium.h>

#include "macros.h"
#include "rb79.h"

/* Global configuration */
static const struct configuration *conf;

/* Characters for Base64 */
static const char *base64_elts =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

/* This should be a multiple of 3, so that base64 gives no '=' */
#define OUT_HASH_LEN (3 * 3)

/*
 * Initialize any static elements needed for this file
 *
 * Preconditions:
 *
 *  - setup_tripcodes() was not invoked more recently than
 *    clean_tripcodes().
 *
 * Postconditions (success):
 *
 *  - Any other function in this file may be safely called.
 */
int
setup_tripcodes(const struct configuration *in_conf)
{
        conf = in_conf;

        return 0;
}

/*
 * Base64 encode in to *out.
 *
 * Preconditions:
 *
 *  - in is an array of memory of length in_len (it need not be a string).
 *
 *  - in_len is a multiple of 3.
 *
 *  - out and out_len are not 0.
 *
 *  - overwriting *out shall not cause a memory leak.
 *
 * Postconditions (success):
 *
 *  - *out is a string of length *out_len.
 *
 *  - The contents of *out are the base64 encoding of the first
 *    in_len bytes of in.
 */
static int
base64_encode(const unsigned char *in, size_t in_len, char **out,
              size_t *out_len)
{
        int ret = -1;
        size_t built_len = in_len * 4 / 3 + 3;
        size_t out_idx = 0;
        char *built = 0;

        if ((in_len * 4) / in_len != 4 ||
            (in_len * 4 / 3) + 3 < (in_len * 4 / 3)) {
                ERROR_MESSAGE("overflow (in_len = %zu)", in_len);
                goto done;
        }

        if (!(built = malloc(built_len))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        for (size_t in_idx = 0; in_idx + 2 < in_len; in_idx += 3) {
                unsigned char i1 = in[in_idx + 0];
                unsigned char i2 = in[in_idx + 1];
                unsigned char i3 = in[in_idx + 2];
                size_t j1 = i1 >> 2;
                size_t j2 = ((i1 & 0x03) << 4) | (i2 >> 4);
                size_t j3 = ((i2 & 0x0F) << 2) | ((i3 & 0xC0) >> 6);
                size_t j4 = (i3 & 0x3F);

                built[out_idx++] = base64_elts[j1];
                built[out_idx++] = base64_elts[j2];
                built[out_idx++] = base64_elts[j3];
                built[out_idx++] = base64_elts[j4];
        }

        built[out_idx] = '\0';
        *out = built;
        *out_len = out_idx;
        ret = 0;
done:

        return ret;
}

/*
 * Calculate (raw) tripcode from (raw) name, if necessary. Name is
 * truncated to hide the input. This assumes p->raw.tripcode is
 * empty.
 *
 * Preconditions:
 *
 *  - p is not 0.
 *
 *  - p->raw.name is either 0, or is a string of length p->raw.name_len.
 *
 *  - Overwriting p->raw.tripcode shall not cause a memory leak.
 *
 * Postconditions (success):
 *
 *  - If p->raw.name was of the form "foo#bar", it is now of the
 *    form "foo". p->raw.name_len has been adjusted accordingly.
 *
 *  - If p->raw.name was of the form "foo#bar", p->raw.tripcode is
 *    determined by the key "bar". p->raw.tripcode_len is appropriate.
 */
int
tripcodes_calculate(struct post_cmd *p)
{
        int ret = -1;
        char *trip_phrase_start = 0;
        unsigned char out_buf[OUT_HASH_LEN];

        /* do not initialize this - unknown if struct or scalar */
        crypto_generichash_state cg_state;

        if (!p->raw.name_len) {
                goto success;
        }

        /* XXX: The string length operations could be tightened */
        trip_phrase_start = strchr(p->raw.name, '#');

        if (!trip_phrase_start) {
                goto success;
        }

        *trip_phrase_start = '\0';
        p->raw.name_len = strlen(p->raw.name);
        trip_phrase_start++;

        if (!*trip_phrase_start) {
                goto success;
        }

        if (crypto_generichash_init(&cg_state, 0, 0, OUT_HASH_LEN) < 0) {
                PERROR_MESSAGE("crypto_generichash_init");
                goto done;
        }

        if (crypto_generichash_update(&cg_state, (const unsigned
                                                  char *) trip_phrase_start,
                                      strlen(trip_phrase_start)) < 0) {
                LOG("Failed to update hash with \"%s\"", trip_phrase_start);
                PERROR_MESSAGE("crypto_generichash_update");
                goto done;
        }

        if (crypto_generichash_update(&cg_state, (const unsigned
                                                  char *) conf->trip_salt,
                                      conf->trip_salt_len) < 0) {
                LOG("Failed to update hash with \"%s\"", conf->trip_salt);
                PERROR_MESSAGE("crypto_generichash_update");
                goto done;
        }

        if (crypto_generichash_final(&cg_state, out_buf, OUT_HASH_LEN) < 0) {
                PERROR_MESSAGE("crypto_generichash_final");
                goto done;
        }

        if (base64_encode(out_buf, OUT_HASH_LEN, &p->raw.tripcode,
                          &p->raw.tripcode_len) < 0) {
                goto done;
        }

success:
        ret = 0;
done:

        return ret;
}

/*
 * Clean any memory from this file
 *
 * Postconditions (success):
 *
 *  - Valgrind won't report any memory leaks from this file.
 *
 *  - setup_tripcodes() can be safely called again.
 */
int
clean_tripcodes(void)
{
        conf = 0;

        return 0;
}
